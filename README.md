# Hyperzod Service Functions Package

This package includes the following functionalities in the form of Traits, Functions, Classes.

- `ApiResponseTrait` - Generate Api Response
- `ServiceResponseTrait` - Generate Service Response and Then Process to Api Response
- `ServiceConsumerTrait` - Call service using service discovery

Add all the traits into Base Controller or Service files.

## Installation

You can install the package via composer:

```bash
composer require hyperzodtech/hyperzod-service-functions
```

Add in bootstrap/app.php

```php
$app->register(Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsServiceProvider::class);
```
In app/http/kernel.php, add to the 'api' array in $middlewareGroups

```php
\Hyperzod\HyperzodServiceFunctions\Http\Middleware\HasTenantId::class
```

## ServiceConsumerTrait

All the service names and hosts have been defined in `routes/services.json` of Service Discovery project

Set `SERVICE_DISCOVERY_HOST` in .env file

Add trait to base controller

``` php

namespace App\Http\Controllers;

use Hyperzod\HyperzodServiceFunctions\ServiceConsumerTrait;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use ServiceConsumerTrait;
}
```

Calling service by assigning `$SERVICE` vaiable and then calling `consumeService` method

``` php
class AuthServiceController extends Controller
{
    function __construct()
    {
        $this->SERVICE = 'auth';
    }

    public function login(Request $request)
    {
        return $this->consumeService($request, 'POST', '/login');
    }
}
```
