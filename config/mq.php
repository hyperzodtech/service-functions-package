<?php

return [
   'connection' => [
      'host' => env('AMQP_HOST', 'localhost'),
      'port' => env('AMQP_PORT', 5672),
      'user' => env('AMQP_USER', 'guest'),
      'password' => env('AMQP_PASSWORD', 'guest'),
      'vhost' => env('AMQP_VHOST', '/'),
      'ssl' => env('AMQP_SSL', false),
   ],
   'proxy_enabled' => env('AMQP_PROXY_ENABLED', false),
   'proxy_connection' => [
      'host' => env('AMQP_PROXY_HOST', 'localhost'),
      'port' => env('AMQP_PROXY_PORT', 5673),
      'user' => env('AMQP_PROXY_USER', 'guest'),
      'password' => env('AMQP_PROXY_PASSWORD', 'guest'),
      'vhost' => env('AMQP_PROXY_VHOST', '/'),
      'ssl' => env('AMQP_PROXY_SSL', false),
   ],
   'queue_config' => [
      [
         'exchange' => "Exchange name",
         'queues' => [
            [
               "name" => "Unique queue name",
               "binding_key" => "Binding key",
               "callback" => "Path to callback",
            ],

         ]
      ]
   ]
];
