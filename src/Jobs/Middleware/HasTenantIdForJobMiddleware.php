<?php
 
namespace Hyperzod\HyperzodServiceFunctions\Jobs\Middleware;

use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

class HasTenantIdForJobMiddleware
{
    /**
     * Process the queued job.
     *
     * @param  mixed  $job
     * @param  callable  $next
     * @return mixed
     */
    public function handle($job, $next)
    {
        if (! isset($job->tenantId)) throw new \Exception('Tenant ID not set for job.');
        HyperzodServiceFunctionsFacade::setTenantId($job->tenantId);
        return $next($job);

    }
}