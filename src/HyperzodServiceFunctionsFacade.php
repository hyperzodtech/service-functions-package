<?php

namespace Hyperzod\HyperzodServiceFunctions;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Hyperzod\HyperzodServiceFunctions\Skeleton\SkeletonClass
 */
class HyperzodServiceFunctionsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'HyperzodServiceFunctions';
    }
}
