<?php

namespace Hyperzod\HyperzodServiceFunctions\Mq\Commands;

use Exception;
use ErrorException;
use RuntimeException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Exception\AMQPIOException;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Hyperzod\HyperzodServiceFunctions\Enums\Mq\MQEnum;
use PhpAmqpLib\Exception\AMQPConnectionClosedException;
use PhpAmqpLib\Connection\Heartbeat\PCNTLHeartbeatSender;

class ConsumeMQ extends Command
{
    protected $signature = 'mq:consume';

    protected $description = 'Consume MQ messages';

    protected $connection = null;

    protected $queueConfig = null;

    protected $amqpConnection = null;

    protected $amqpChannel = null;

    protected  $waitBeforeReconnect = 1000000 * 3;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->setConnection();
        $this->setQueueConfig();

        while (true) {
            try {
                //Connect to RabbitMQ
                $this->connect();

                //Consume messages
                $this->consume();
            } catch (AMQPRuntimeException $e) {
                $this->reconnectionRoutine($e, 'AMQPRuntimeException: ' . $e->getMessage() . "\n");
            } catch (AMQPIOException $e) {
                $this->reconnectionRoutine($e, 'AMQPIOException: ' . $e->getMessage() . "\n");
            } catch (AMQPConnectionClosedException $e) {
                $this->reconnectionRoutine($e, 'AMQPConnectionClosedException: ' . $e->getMessage() . "\n");
            } catch (RuntimeException $e) {
                $this->reconnectionRoutine($e, 'Runtime exception: ' . $e->getMessage() . "\n");
            } catch (ErrorException $e) {
                $this->reconnectionRoutine($e, 'Error exception: ' . $e->getMessage() . "\n");
            } catch (Exception $e) {
                $this->reconnectionRoutine($e, 'Exception: ' . $e->getMessage() . "\n");
            }
            echo "-------------------------------------------------------\n";
            echo "-------------------------------------------------------\n";
            $this->echoMessage("Trying to reconnect\n");
            echo "-------------------------------------------------------\n";
            echo "-------------------------------------------------------\n";
        }
    }

    protected function setConnection()
    {
        $this->connection = config('mq.connection');
        if (!$this->connection) {
            $this->reportError('MQ connection not configured');
            exit;
        }
        return;
    }

    protected function setQueueConfig()
    {
        $this->queueConfig = config('mq.queue_config');
        if (!$this->queueConfig) {
            $this->error("MQ queue config not configured");
            exit;
        }
        return;
    }

    protected function connect()
    {
        $this->echoMessage("Connecting to " . $this->connection['host'] . ":" . $this->connection['port'] . "\n");

        $this->amqpConnection = AMQPStreamConnection::create_connection(
            [
                [
                    "host" => $this->connection['host'],
                    "port" => $this->connection['port'],
                    "user" => $this->connection['user'],
                    "password" => $this->connection['password'],
                    "vhost" => $this->connection['vhost']
                ]
            ],
            [
                'heartbeat' => 10
            ]
        );

        $sender = new PCNTLHeartbeatSender($this->amqpConnection);
        $sender->register();

        $this->amqpChannel = $this->amqpConnection->channel();

        if ($this->amqpConnection && $this->amqpChannel) {
            $this->echoMessage("Connected to " . $this->connection['host'] . ":" . $this->connection['port'] . "\n");
        }

        return;
    }

    protected function consume()
    {
        foreach ($this->queueConfig as $exch) {
            $exchange = $exch["exchange"];
            /*
                name: $exchange
                type: direct
                passive: false
                durable: true // the exchange will survive server restarts
                auto_delete: false //the exchange won't be deleted once the channel is closed.
            */
            $this->amqpChannel->exchange_declare($exchange, AMQPExchangeType::TOPIC, false, true, false);
            $queue_bindings = $exch["queues"];
            foreach ($queue_bindings as $queue) {
                /*
                        name: $queue
                        passive: false
                        durable: true // the queue will survive server restarts
                        exclusive: false // the queue can be accessed in other channels
                        auto_delete: false //the queue won't be deleted once the channel is closed.
                    */
                $this->amqpChannel->queue_declare($queue["name"], false, true, false, false);
            }
            foreach ($queue_bindings as $queue) {
                $this->amqpChannel->queue_bind($queue["name"], $exchange, $queue["binding_key"]);
            }

            foreach ($queue_bindings as $queue) {
                $call = $queue["callback"];
                $callback = function ($data) use ($call, $queue) {
                    $data = json_decode($data->body, true);

                    if (isset($queue["meta"])) {
                        $data["mq_meta"] = $queue["meta"];
                    }
                    $data["mq_queue"] = $queue["name"];
                    $data["binding_key"] = $queue["binding_key"];
                    // echo "Dispatching job for queue: " . $queue["name"] . "binding_key: " . $queue["binding_key"] . "\n";
                    dispatch(function () use ($call, $data) {
                        $callback = new $call;
                        $callback->handle($data);
                    })->onQueue('mq_queue');
                };
                $this->amqpChannel->basic_consume($queue["name"], '', false, true, false, false, $callback);
            }
        }


        $this->echoMessage("Consumption started with pid: " . getmypid() . "\n");

        // Save PID to cache
        cache([MQEnum::CACHE_KEY => getmypid()]);

        // enable PCNTL signal handling
        pcntl_async_signals(true);

        // Handle SIGINT
        pcntl_signal(SIGINT, function () {
            $this->handlePCNTLSignal("SIGINT");
        });

        // Handle SIGTERM
        pcntl_signal(SIGTERM, function () {
            $this->handlePCNTLSignal("SIGTERM");
        });

        $this->echoMessage("Waiting for messages. To exit press CTRL+C\n");
        $this->amqpChannel->consume();
    }

    protected function cleanup_connection()
    {
        // Close the channel
        try {
            if ($this->amqpChannel !== null) {
                $this->echoMessage("Closing channel...\n");
                $this->amqpChannel->close();
            }
        } catch (Exception $e) {
            $this->reportError("Error while cleaning channel: " . $e->getMessage() . "\n");
        }

        // Close the connection
        try {
            if ($this->amqpConnection !== null) {
                $this->echoMessage("Closing connection...\n");
                $this->amqpConnection->close();
            }
        } catch (Exception $e) {
            $this->reportError("Error while cleaning connection: " . $e->getMessage() . "\n");
        }
        return;
    }

    protected function echoMessage($message)
    {
        echo "[" . now("UTC")->toDateTimeString() . " UTC]: " . $message;
    }

    protected function reportError($message)
    {
        $this->echoMessage($message);
        Log::alert("Service: " . config('app.name') . "\nEnvironment: " . config('app.env') . "\nMessage: " . $message);
    }

    protected function reconnectionRoutine($e, $message)
    {
        report($e);
        $this->reportError($message);
        $this->cleanup_connection();
        usleep($this->waitBeforeReconnect);
    }

    protected function handlePCNTLSignal($signal)
    {
        $this->echoMessage("$signal received\n");
        $this->echoMessage("Shutting down MQ...\n");
        $this->cleanup_connection();
        exit;
    }
}
