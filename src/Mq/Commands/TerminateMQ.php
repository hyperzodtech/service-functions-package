<?php

namespace Hyperzod\HyperzodServiceFunctions\Mq\Commands;

use Hyperzod\HyperzodServiceFunctions\Enums\Mq\MQEnum;
use Illuminate\Console\Command;

class TerminateMQ extends Command
{
    protected $signature = 'mq:terminate';

    protected $description = 'Terminate MQ process';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $pid = cache(MQEnum::CACHE_KEY); // get pid from cache
            if ($pid) {
                $this->info('Terminating MQ process with pid: ' . $pid);
                posix_kill($pid, SIGTERM);
                cache()->forget(MQEnum::CACHE_KEY); // remove pid from cache
                $this->info('OK!');
            } else {
                $this->error('MQ process not found');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
