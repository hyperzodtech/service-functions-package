<?php

namespace Hyperzod\HyperzodServiceFunctions\Mq\Abstract;

use Hyperzod\HyperzodServiceFunctions\Mq\MQHelper;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

abstract class AbstractPublishEventToMQ implements ShouldQueue
{
	public int $tries = 5;
	public int $backoff = 5;
	public string $exchange;
	public string $routingKey;
	public bool $publishToMQ;

	abstract function setExchange(): string;
	abstract function setRoutingKey(): string;

	function setPublishToMQ(): bool
	{
		return true;
	}

	public function handle($event)
	{
		$this->exchange = $this->setExchange();
		$this->routingKey = $this->setRoutingKey();
		$this->publishToMQ = $this->setPublishToMQ();

		if ($this->publishToMQ === true) {
			MQHelper::publishToMQ($this->exchange, $this->routingKey, $event->data);
		}
	}
}
