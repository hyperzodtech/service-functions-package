<?php

namespace Hyperzod\HyperzodServiceFunctions\Mq;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class MQHelper
{
    public static function publishToMQ($exchange, $routing_key, $data)
    {
        $queue_connection = config('mq.connection');

        $proxy_enabled = config('mq.proxy_enabled', false);
        if ($proxy_enabled) {
            $queue_connection = config('mq.proxy_connection');
        }

        if (!$queue_connection) {
            throw new \Exception('Queue connection data not found');
        }

        $connection = new AMQPStreamConnection(
            $queue_connection['host'],
            $queue_connection['port'],
            $queue_connection['user'],
            $queue_connection['password'],
            $queue_connection['vhost']
        );

        $channel = $connection->channel();
        /*
            name: $exchange
            type: direct
            passive: false
            durable: true // the exchange will survive server restarts
            auto_delete: false //the exchange won't be deleted once the channel is closed.
        */
        $channel->exchange_declare($exchange, AMQPExchangeType::TOPIC, false, true, false);
        $data = new AMQPMessage(json_encode($data), [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
        ]);

        $channel->basic_publish($data, $exchange, $routing_key);

        if ($channel) {
            $channel->close();
        }

        if ($connection) {
            $connection->close();
        }
    }
}
