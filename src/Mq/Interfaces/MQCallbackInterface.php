<?php

namespace Hyperzod\HyperzodServiceFunctions\Mq\Interfaces;

interface MQCallbackInterface
{
	public function handle(array $data);
}
