<?php

namespace Hyperzod\HyperzodServiceFunctions\ValueObjects;

use Spatie\LaravelData\Data;

class LatLonVO extends Data
{
   function __construct(
      public float $lat,
      public float $lon,
   ) {
   }
}
