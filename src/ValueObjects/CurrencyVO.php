<?php

namespace Hyperzod\HyperzodServiceFunctions\ValueObjects;

use Spatie\LaravelData\Data;

class CurrencyVO extends Data
{
   function __construct(
      public string $code,
      public string $symbol,
      public string $symbol_native,
      public string $symbol_direction,
      public string $symbol_type,
   ) {
   }
}
