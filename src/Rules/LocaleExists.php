<?php

namespace Hyperzod\HyperzodServiceFunctions\Rules;

use Illuminate\Contracts\Validation\Rule;
use Hyperzod\HyperzodServiceFunctions\Traits\HelpersServiceTrait;
use Illuminate\Support\Facades\Cache;

class LocaleExists implements Rule
{
    use HelpersServiceTrait;

    protected $locale;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  string  $locale
     * @return bool
     */
    public function passes($attribute, $locale)
    {
        $this->locale = $locale;

        $locales = Cache::rememberForever('locales', function () {
            return $this->fetchLocales();
        });
        
        $locales = array_column($locales, 'locale');

        // localeValidity
        return in_array($locale, $locales);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid locale: ' . $this->locale;
    }
}
