<?php

namespace Hyperzod\HyperzodServiceFunctions\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateLatLonLocationArray implements Rule
{
    public $message = null;
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_array($value)) {
            $this->message = "The $attribute must be an array.";
            return false;
        }
        if (count($value) != 2) {
            $this->message = "The $attribute must be an array with 2 elements.";
            return false;
        }
        if (!is_numeric($value[0]) || !is_numeric($value[1])) {
            $this->message = "The $attribute must be an array with 2 numeric elements.";
            return false;
        }
        if ($value[0] < -90 || $value[0] > 90) {
            $this->message = "The latitude must be between between -90 and 90.";
            return false;
        }
        if ($value[1] < -180 || $value[1] > 180) {
            $this->message = "The longitude must be between between -180 and 180.";
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
