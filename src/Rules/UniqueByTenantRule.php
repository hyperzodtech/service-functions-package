<?php

namespace Hyperzod\HyperzodServiceFunctions\Rules;

use Illuminate\Contracts\Validation\Rule;

class UniqueByTenantRule implements Rule
{
    var $model;
    var $except_column;
    var $except_value;

    function __construct($model, $except_column = null, $except_value = null)
    {
        $this->model = $model;
        $this->except_column = $except_column;
        $this->except_value = $except_value;
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $query = $this->model::query();
        $query->where($attribute, $value);

        if (!is_null($this->except_column))
            $query->where($this->except_column, "!=", $this->except_value);

        return !$query->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be unique.';
    }
}
