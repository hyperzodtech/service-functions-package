<?php

namespace Hyperzod\HyperzodServiceFunctions\Rules;

use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Contracts\Validation\Rule;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;
use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

class ValidateActiveOrderTypeTenant implements Rule
{
   use ServiceConsumerTrait;

   public $message = null;
   /**
    * Determine if the validation rule passes.
    *
    * @param  string  $attribute
    * @param  mixed  $value
    * @return bool
    */
   public function passes($attribute, $value)
   {
      $order_types = collect($this->getActiveOrderTypesTenant());
      $allowed_order_types = $order_types->pluck('order_type')->toArray();
      if (!is_array($value)) {
         if (!in_array($value, $allowed_order_types)) {
            $this->message = "The $attribute is invalid.";
            return false;
         }
         $order_type = $order_types->where('order_type', $value)->first();
         Config::set('is_address_required', boolval($order_type['requires_address']));
      } else {
         foreach ($value as $order_type) {
            if (!in_array($order_type, $allowed_order_types)) {
               $this->message = "Some of the $attribute are invalid.";
               return false;
            }
         }
      }
      return true;
   }

   /**
    * Get the validation error message.
    *
    * @return string
    */
   public function message()
   {
      return $this->message;
   }

   public function getActiveOrderTypesTenant()
   {
      $request = new \Illuminate\Http\Request;

      $request->query->add([
         'tenant_id' => HyperzodServiceFunctionsFacade::getTenantId(),
      ]);

      $response = $this->consumeService(ServiceEnum::ORDER, $request, 'GET', '/orderTypes/tenant/active');

      // responseReceivedSuccessfully()
      if (isset($response['success']) && $response['success'] == true) {
         return $response['data'];
      }

      if (isset($response['message'])) {
         throw new Exception($response['message']);
      }

      throw new Exception("Error fetching tenant order types");
   }
}
