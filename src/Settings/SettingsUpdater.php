<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings;

use Faker\Extension\Helper;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractUpdaterLayer;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingCollectionDto;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;

class SettingsUpdater
{
    protected SettingCollectionDto $settings;

    private $config;
    protected $keyModel;
    protected array $blueprint;
    protected array $response;
    protected array $layers;

    public function setConfig(string $config_key)
    {
        $this->config = config($config_key);

        if (is_null($this->config)) throw new ServiceClientException("Config not found.");

        $this->keyModel = $this->config['models']['key_model'];
        $this->layers = $this->config['updater']['layers'];

        return $this;
    }

    public function handle(SettingCollectionDto $settings)
    {
        $this->settings = $settings;
        $this->validateSettingKeys();
        $this->setBlueprint();
        $this->validateSettingKeysTypes();

        foreach ($this->settings->collection as $setting)
            $this->passThroughLayers($setting);

        return $this;
    }

    public function validateSettingKeys()
    {
        $valid_keys = $this->keyModel::pluck('setting_key')->toArray();

        foreach ($this->settings->collection as $setting) {
            if (!in_array($setting->setting_key, $valid_keys)) {
                if (isset($this->config['config']['silently_ignore_invalid_keys']) && $this->config['config']['silently_ignore_invalid_keys'] == true) {
                    unset($this->settings->collection[array_search($setting, $this->settings->collection)]);
                } else {
                    throw new ServiceClientException("Invalid setting key: {$setting->setting_key}");
                }
            }
        }
    }

    protected function setBlueprint()
    {
        $setting_keys = collect($this->settings->toArray()['collection'])->pluck('setting_key')->toArray();

        $this->blueprint = $this->keyModel::whereIn('setting_key', $setting_keys)->get()->toArray();
    }

    public function validateSettingKeysTypes()
    {
        foreach ($this->settings->collection as $setting) {

            $blueprint = collect($this->blueprint)->where('setting_key', $setting->setting_key)->first();

            if ($blueprint['types'] ?? false) {

                $required = collect($blueprint['types'])->pluck('type')->toArray();
                $given = array_keys($setting->types ?? []);
                $diff = array_diff($required, $given);

                if (!empty($diff)) {
                    throw new ServiceClientException("Given types not given for setting_key: $setting->setting_key", array_values($diff));
                }

                if ($setting->types) {
                    foreach ($setting->types as $type_key => $type_value) {
                        $this->checkValidValuesForType($setting, $type_key, $type_value);
                    }
                }
            }
        }
    }

    public function checkValidValuesForType($setting, $type_key, $type_value)
    {
        $blueprint = collect($this->blueprint)->where('setting_key', $setting->setting_key)->first();

        if (isset($blueprint['types'])) {
            $type_blueprint = collect($blueprint['types'])->where('type', $type_key)->first();

            if (is_null($type_blueprint)) {
                throw new ServiceClientException("Type '$type_key' is not supported for $setting->setting_key");
            }

            if (!in_array($type_value, $type_blueprint['values'])) {
                throw new ServiceClientException("$setting->setting_key: Invalid type value given for type '$type_key': $type_value");
            }
        } else {
            throw new ServiceClientException("No types are defined in blueprint for $setting->setting_key");
        }
    }

    public function passThroughLayers(SettingDto $setting)
    {
        $setting = $this->checkLocaleIsGiven($setting);

        $setting = $this->appendBlueprintFields($setting);
        $setting_blueprint = HelperMethods::getBlueprint($this->blueprint, $setting->setting_key);

        foreach ($this->layers as $layer_name => $layer_config) {

            $layer_config         = HelperMethods::resolveFullLayerConfig($layer_config, $setting_blueprint);
            $class_path           = HelperMethods::resolveClassPath($layer_config, $setting->setting_key);

            if (!class_exists($class_path))
                $class_path = HelperMethods::tryToGetDefaultClass($layer_config);

            if (class_exists($class_path)) {
                HelperMethods::checkClassInstance($class_path, AbstractUpdaterLayer::class);

                $instance = new $class_path;
                $instance->setSetting($settingReturnedFromLayer ?? $setting);
                $instance->setConfig($this->config);
                $instance->setBlueprint($setting_blueprint);

                $settingReturnedFromLayer = $instance->handle($settingReturnedFromLayer ?? $setting)->get();
            }

            HelperMethods::checkIfLayerIsRequired($layer_config, $class_path, $setting, $setting_blueprint);
        }

        $this->response[] = $settingReturnedFromLayer->toArray();
    }

    public function checkLocaleIsGiven(SettingDto $setting): SettingDto
    {
        $locale_is_required = collect($this->blueprint)->where('setting_key', $setting->setting_key)->first();

        if (isset($locale_is_required['has_locale']) && $locale_is_required['has_locale'] == false) {
            $setting->locale = null;
            return $setting;
        }

        if ($locale_is_required['has_locale'] ?? false) {
            if ($locale_is_required && is_null($setting->locale))
                throw new ServiceClientException("Locale is required for: " . $setting->setting_key);
        }

        return $setting;
    }

    public function appendBlueprintFields(SettingDto $setting): SettingDto
    {
        $group = collect($this->blueprint)->where('setting_key', $setting->setting_key)->first()['group'];
        $tags  = collect($this->blueprint)->where('tags')->first()['tags'] ?? [];

        $setting->group = $group;
        $setting->tags = $tags;
        return $setting;
    }

    public function getBlueprints()
    {
        return $this->blueprint;
    }

    public function get()
    {
        return $this->response;
    }
}
