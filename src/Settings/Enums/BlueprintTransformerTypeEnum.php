<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\Enums;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractTransformer;
use Hyperzod\HyperzodServiceFunctions\Settings\Seeders\GroupBlueprintTransformer;
use Hyperzod\HyperzodServiceFunctions\Settings\Seeders\KeyBlueprintTransformer;

enum BlueprintTransformerTypeEnum: string {
    case KEY =  KeyBlueprintTransformer::class;
    case GROUP = GroupBlueprintTransformer::class;
}