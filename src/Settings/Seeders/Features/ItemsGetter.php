<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\Seeders\Features;

class ItemsGetter
{
    protected array $events;
    protected array $currentItem;
    protected array $items = [];
    protected string $itemType;

    public function setFolderPath(string $folderPath)
    {
        $this->folderPath = $folderPath;
        return $this;
    }

    public function setItemType(string $item_type)
    {
        $this->itemType = $item_type;
        return $this;
    }

    public function process()
    {
        // Get all the json files
        $files = glob($this->folderPath . "/*.json");

        // Validate each file and set the array
        foreach ($files as $file) {

            // Abstractable.
            $file_contents = json_decode(file_get_contents($file), true);
            $array_of_items = $file_contents[$this->itemType];
            
            $this->items = array_merge($this->items, $array_of_items);
        }
        return $this;
    }

    public function get()
    {
        return $this->items;
    }
}
