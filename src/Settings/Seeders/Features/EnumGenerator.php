<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Seeders\Features;

class EnumGenerator
{
    protected array $list;

    protected $namespace = "App\Enums";
    protected $path = "app/Enums";
    protected $class_name = "TestEnum";

    protected $isKeyValueList = false;

    public function setList($list)
    {
        $this->list = array_unique($list);
        return $this;
    }

    public function asKeyValueList()
    {
        $this->isKeyValueList = true;
        return $this;
    }

    public function setNamespace(string $namespace)
    {
        $this->namespace = $namespace;
        return $this;
    }

    public function setPath(string $path)
    {
        $this->path = $path;
        return $this;
    }

    public function setClassName(string $class_name)
    {
        $this->class_name = $class_name;
        return $this;
    }

    public function generate()
    {
        // ✅ Generate Enum only locally.
        if (env('APP_ENV') == 'local') {
            $class_name = $this->class_name;
            $namespace = $this->namespace;
            $path = $this->path;

            $php_content = "<?php\nnamespace $namespace;\n\nclass $class_name\n{\n";

            foreach ($this->list as $key => $value) {

                if ($this->isKeyValueList) {
                    $enum_key = $key;
                } else {
                    $enum_key = $value;
                }

                // 👉 const EVENT_NAME = 'event_name';
                $php_content .= "\tconst " . strtoupper($enum_key) . " = '$value';" . "\n";
            }

            $php_content .= "\n}";

            file_put_contents(base_path("$path/$class_name.php"), $php_content);
        }
    }
}
