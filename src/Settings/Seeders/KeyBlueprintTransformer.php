<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Seeders;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractTransformer;
use Hyperzod\HyperzodServiceFunctions\Settings\Seeders\Features\EnumGenerator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class KeyBlueprintTransformer extends AbstractTransformer
{
    protected function formatAndSet(array $validated_file_contents)
    {
        $group = $validated_file_contents['group'];
        $unique_key_plural = Str::plural($this->config['unique_key']);

        $items_array = $validated_file_contents[$unique_key_plural] ?? $validated_file_contents['setting_keys'];

        // 📝 Run each items through the transformer.
        foreach ($items_array as $item) {

            // ✅ Prepend group to item object.
            $item = Arr::prepend($item, $group, 'group');

            // ✅ Add to items array.
            $this->response[] = $item;
        }
    }

    protected function passThroughPreValidationLayers($items_with_group): array
    {
        if ($this->config['layers']['pre-validation'] ?? false) {

            $layers = $this->config['layers']['pre-validation'];
            foreach ($layers as $layer) {
                $class = $layer['class_path'];

                $unique_key_plural = Str::plural($this->config['unique_key']);
                $items_with_group[$unique_key_plural] = (new $class)
                    ->setArrayOfBlueprints($items_with_group[$unique_key_plural])
                    ->setGroup($items_with_group['group'])
                    ->setConfig($this->config)
                    ->process()
                    ->get();
            }
        }

        // else
        return $items_with_group;
    }

}
