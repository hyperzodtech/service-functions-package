<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Seeders\Validators;

class ValidateDuplicates
{
    public function validate($list, string $key)
    {
        $keys = collect($list)->pluck($key)->toArray();
        $key_are_unique = count($keys) === count(array_flip($keys));

        if (!$key_are_unique) {
            throw new \Exception('Duplicate unique_keys found for: ' . $key);
        }
    }
}
