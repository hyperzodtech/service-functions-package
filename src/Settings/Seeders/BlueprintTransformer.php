<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\Seeders;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractTransformer;
use Hyperzod\HyperzodServiceFunctions\Settings\Enums\BlueprintTransformerTypeEnum;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\BlueprintInterface;

class BlueprintTransformer implements BlueprintInterface
{
    protected AbstractTransformer $transformerType;

    public function setType(BlueprintTransformerTypeEnum $type)
    {
        $this->transformerType = new $type->value;
        return $this;
    }

    public function setConfig(string $config)
    {
        $this->transformerType->setConfig($config);
        return $this;
    }

    public function overrideFolderPath(string $path)
    {
        $this->transformerType->overrideFolderPath($path);
        return $this;
    }

    public function overridePreValidationLayers(array $layer)
    {
        $this->transformerType->overridePreValidationLayers($layer);
        return $this;
    }

    public function appendPreValidationLayer($layer)
    {
        $this->transformerType->appendPreValidationLayer($layer);
        return $this;
    }

    public function process()
    {
        $this->transformerType->process();
        return $this;
    }

    public function get(): array
    {
        return $this->transformerType->get();
    }

}
