<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Seeders;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractTransformer;
use Illuminate\Support\Facades\Validator;

class GroupBlueprintTransformer extends AbstractTransformer
{
    protected function formatAndSet(array $validated_file_contents)
    {
        foreach ($validated_file_contents as $group) {
            $this->response[] = $group;
        }
    }

}
