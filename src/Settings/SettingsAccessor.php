<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingAccessorDto;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;

class SettingsAccessor
{
    protected SettingAccessorDto $settingsAccessorData;
    protected $groupModel;
    protected $keyModel;
    protected $userSettingModel;
    protected $classPathForDefault;
    protected $classPathForAccessor;

    protected $blueprintForGroup;
    protected array $blueprintForSettingKeys;
    protected $blueprintForTags;
    protected $settingKeyMetas = [];

    protected $childrenGroups = false;

    protected $requestedSettingKeys;
    protected $fetchedSettingKeys;
    protected array $missingSettingKeys = [];

    protected $layers;
    protected $config;

    protected $locale = 'en';
    protected array $localeScope;

    protected array $processedSettings;

    protected $response = [];

    public function setConfig($config)
    {
        $this->groupModel       =   config($config)['models']['group_model'];
        $this->keyModel         =   config($config)['models']['key_model'];
        $this->userSettingModel =   config($config)['models']['user_setting_model'];
        $this->layers           =   config($config)['accessor']['layers'];
        $this->config           =   config($config);

        return $this;
    }

    public function setProcessedSettings(array $processedSettings)
    {
        $this->processedSettings = $processedSettings;
        return $this;
    }

    public function handle(SettingAccessorDto $settings_accessor_data)
    {
        $this->settingsAccessorData = $settings_accessor_data;
        $this->init();
        $this->resolveSettingRequest();
        $this->setBlueprint();
        $this->setMetaRecords();
        $this->resolve();
        $this->finish();

        return $this;
    }

    private function init()
    {
        $this->locale = $this->settingsAccessorData->locale ?? $this->locale;

        if (!empty($this->settingsAccessorData->locales)) {
            $this->localeScope = [...$this->settingsAccessorData->locales, null];
        } else {
            $this->localeScope = [$this->locale, null];
        }
    }

    private function resolve()
    {
        if ($this->settingsAccessorData->tags) {
            $this->resolveSettingsByTags();
            return;
        }

        if ($this->settingsAccessorData->group_key) {
            $this->resolveSettingsByGroup();
            return;
        }

        if ($this->settingsAccessorData->keys) {
            $this->resolveSettingsByKeys();
            return;
        }

        if ($this->settingsAccessorData->custom_query) {
            $this->resolveSettingsByCustomQuery();
            return;
        }

        $this->resolveAllSettings();
    }

    private function resolveSettingRequest()
    {
        $this->resolveKeys();
        $this->checkGroupOrKeyGiven();
        $this->validateKeysOrGroup();
    }

    private function resolveKeys()
    {
        // Single setting key is given.
        if (getType($this->settingsAccessorData->keys) == 'string') {
            // Put in array.
            $this->settingsAccessorData->keys = [$this->settingsAccessorData->keys];
        }
    }

    private function checkGroupOrKeyGiven()
    {
        $group_key = $this->settingsAccessorData->group_key ?? false;
        $keys = $this->settingsAccessorData->keys ?? false;
        $tags = $this->settingsAccessorData->tags ?? false;

        // if (!$group_key && !$keys && !$tags)
        // throw new ServiceClientException('Either value is required: group_key, keys or tags');
    }

    private function validateKeysOrGroup()
    {
        if ($this->settingsAccessorData->group_key) {
            $this->validateGroupKey();
            return;
        }

        if ($this->settingsAccessorData->keys) {
            $this->validateKeys();
        }
    }

    private function validateGroupKey()
    {
        if (!$this->groupModel::pluck('group_key')->contains($this->settingsAccessorData->group_key)) {
            throw new ServiceClientException('Invalid group key.');
        }
    }

    private function validateKeys()
    {
        $valid_keys = $this->keyModel::pluck('setting_key')->toArray();

        foreach ($this->settingsAccessorData->keys as $setting_key) {
            if (!in_array($setting_key, $valid_keys)) {
                unset($this->settingsAccessorData->keys[array_search($setting_key, $this->settingsAccessorData->keys)]);
            }
        }
    }

    public function setBlueprint()
    {
        // Settings are requested by group
        if ($this->settingsAccessorData->group_key) {
            $this->setBlueprintForGroup();
            return $this;
        }

        // Settings are requested by keys
        if ($this->settingsAccessorData->keys) {
            $this->setBlueprintForKeys();
            return $this;
        }

        // Settings are requested by tags
        if ($this->settingsAccessorData->tags) {
            $this->setBlueprintForTags();
            return $this;
        }

        if ($this->settingsAccessorData->custom_query) {
            return $this;
        }

        $this->setBlueprintForAllKeys();
    }

    public function setMetaRecords()
    {
        $model = $this->config['models']['meta_model'] ?? null;

        if ($model) {
            $plucked = collect($this->blueprintForSettingKeys)->pluck('setting_key')->toArray();
            $this->settingKeyMetas = $model::whereIn('setting_key', $plucked)->get()->toArray();
        }
    }

    // Set blueprints...
    private function setBlueprintForGroup()
    {
        $this->blueprintForGroup = $this->groupModel::where('group_key', $this->settingsAccessorData->group_key)->first();
        $this->resolveChildrenGroups();
        $this->resolveBlueprintForSettingKeys();
    }

    private function resolveChildrenRecursively($group_blueprint)
    {
        if ($group_blueprint->hasChildren->isEmpty())
            return;

        foreach ($group_blueprint->hasChildren as $group) {
            $this->childrenGroups[] = $group->toArray();
        }
        $this->resolveChildrenRecursively($group);
    }

    private function resolveChildrenGroups()
    {
        $this->resolveChildrenRecursively($this->blueprintForGroup);
        if (!empty($this->childrenGroups)) {
            $this->childrenGroups = collect($this->childrenGroups)->pluck('group_key')->toArray();
            $this->childrenGroups[] = $this->blueprintForGroup->group_key;
        }
    }

    public function resolveBlueprintForSettingKeys()
    {
        if ($this->childrenGroups) {
            $this->blueprintForSettingKeys = $this->keyModel::whereIn('group', $this->childrenGroups)->get()->toArray();
            return;
        }

        $this->blueprintForSettingKeys = $this->keyModel::where('group', $this->settingsAccessorData->group_key)->get()->toArray();
    }

    private function setBlueprintForKeys()
    {
        if ($this->settingsAccessorData->keys) {
            $this->blueprintForSettingKeys = $this->keyModel::whereIn('setting_key', $this->settingsAccessorData->keys)->get()->toArray();
        }
    }

    public function setBlueprintForTags()
    {
        if ($this->settingsAccessorData->tags) {
            $this->blueprintForSettingKeys = $this->keyModel::whereIn('tags', $this->settingsAccessorData->tags)->get()->toArray();
        }
    }

    public function setBlueprintForCustomQuery()
    {
        $setting_keys = collect($this->response)->pluck('setting_key')->toArray();

        if ($this->settingsAccessorData->custom_query) {
            $this->blueprintForSettingKeys = $this->keyModel::whereIn('setting_key', $setting_keys)->get()->toArray();
        }
    }

    public function setBlueprintForAllKeys()
    {
        $this->blueprintForSettingKeys = $this->keyModel::all()->toArray();
    }

    public function resolveSettingsByTags()
    {
        $requested_keys = collect($this->blueprintForSettingKeys)->pluck('setting_key')->toArray();
        $this->response = $this->userSettingModel::whereIn('setting_key', $requested_keys)
            ->whereIn('locale', $this->localeScope)
            ->where($this->settingsAccessorData->types)
            ->get()
            ->toArray();
    }

    private function resolveSettingsByGroup()
    {
        $requested_keys = collect($this->blueprintForSettingKeys)->pluck('setting_key')->toArray();
        $this->response = $this->userSettingModel::whereIn('setting_key', $requested_keys)
            ->whereIn('locale', $this->localeScope)
            ->where($this->settingsAccessorData->types)
            ->get()
            ->toArray();
    }

    private function resolveSettingsByKeys()
    {
        if ($this->settingsAccessorData->keys) {
            $this->response = $this->userSettingModel::whereIn('setting_key', $this->settingsAccessorData->keys)
                ->whereIn('locale', $this->localeScope)
                ->where($this->settingsAccessorData->types)
                ->get()
                ->toArray();
        }
    }

    public function resolveSettingsByCustomQuery()
    {
        if ($this->settingsAccessorData->custom_query) {

            $closure = $this->settingsAccessorData->custom_query;
            $this->response = $closure($this->userSettingModel);

            $this->setBlueprintForCustomQuery();
        }
    }

    public function resolveSettingsForUpdatedKeys()
    {
        $this->setBlueprint();
        $this->setMetaRecords();
        foreach ($this->processedSettings as $setting) {

            $locale = $setting['locale'] ?? null;
            $where_condition = array_merge(
                [
                    'setting_key' => $setting['setting_key']
                ],
                [
                    'locale' => $locale
                ],
                $setting['types'] ?? []
            );

            $this->response[] = $this->userSettingModel::where($where_condition)
                ->first()
                ->toArray();
        }

        return $this;
    }

    public function resolveAllSettings()
    {
        $this->response = $this->userSettingModel::whereIn('locale', $this->localeScope)->get()->toArray();
    }

    private function passThroughLayers()
    {
        if ($this->layers['default'])
            $this->passThroughDefaultLayer();

        if ($this->layers['decryption'] ?? false)
            $this->passThroughDecryption();

        if ($this->layers['accessor'] ?? false)
            $this->passThroughAccessorLayer();

        $this->passThroughValidationLayer();
        $this->passThroughFormatterLayer();
        $this->checkEmptyResponse();
    }

    private function setRequestedAndFetchedSettingKeys()
    {
        $this->fetchedSettingKeys = collect($this->response)->pluck('setting_key');
        $this->requestedSettingKeys = collect($this->blueprintForSettingKeys)->pluck('setting_key');
    }

    public function setMissingKeys()
    {
        $this->missingSettingKeys = collect($this->requestedSettingKeys)
            ->diff($this->fetchedSettingKeys)
            ->toArray();
    }

    public function processResponse()
    {
        // Ignore setting which no longer exists in the db
        foreach ($this->response as $response) {
            if ($response['setting_key']) {
                if (!in_array($response['setting_key'], collect($this->blueprintForSettingKeys)->pluck('setting_key')->toArray())) {
                    $this->response = collect($this->response)->reject(function ($value, $key) use ($response) {
                        return $value['setting_key'] == $response['setting_key'];
                    })->toArray();
                }
            }
        }
    }

    public function passThroughDefaultLayer()
    {
        if (!empty($this->missingSettingKeys)) {
            foreach ($this->missingSettingKeys as $missing_key) {

                $layer_config = $this->layers['default'];

                $class_path = HelperMethods::resolveClassPath($layer_config, $missing_key);
                $blueprint   = HelperMethods::getBlueprint($this->blueprintForSettingKeys, $missing_key);

                if (!class_exists($class_path)) {
                    $class_path = HelperMethods::tryToGetDefaultClass($layer_config);
                }

                if (class_exists($class_path)) {

                    // 👉 new DefaultApiKeys;
                    $instance = new $class_path;
                    $instance->group = $this->keyModel::where('setting_key', $missing_key)->first()->group;

                    $instance->setting_key = $missing_key;
                    $instance->config = $this->config;
                    $instance->blueprint = $blueprint;
                    $instance->settingsAccessorData = $this->settingsAccessorData;

                    $instance->init();

                    $defaults = $instance->handle()->get();

                    // 💡 Skip the setting for a 'false' response.
                    if ($defaults) {
                        $this->response[] = $defaults;
                    }
                }
            }
        }
    }

    public function passThroughAccessorLayer()
    {
        // ✅ Settings were fetched.
        if (!empty($this->response)) {

            // 📝 Foreach each setting
            foreach ($this->response as $index => $setting) {

                // 📝 Compose accessor class.
                // 👉 api_keys -> AccessorApiKeys
                $setting_key = $setting['setting_key'];
                $class_path  = HelperMethods::resolveClassPath($this->layers['accessor'], $setting_key);
                $blueprint   = HelperMethods::getBlueprint($this->blueprintForSettingKeys, $setting_key);

                if (!class_exists($class_path)) {
                    $class_path  = HelperMethods::tryToGetDefaultClass($this->layers['accessor']);
                }

                // ✅ Pass through accessor layer.
                if (class_exists($class_path)) {

                    // 📝 Unset node if accessor returns false.
                    $instance = new $class_path;

                    $instance->config = $this->config;
                    $instance->setting = $setting;
                    $instance->blueprint = $blueprint;

                    $instance->init();

                    $accessor_resp = $instance->handle()->get();

                    if ($accessor_resp) {
                        $this->response[$index] = $accessor_resp;
                    }

                    if ($accessor_resp == false) {
                        unset($this->response[$index]);
                    }
                }
            }
            return $this;
        }
    }

    public function passThroughDecryption()
    {
        // ✅ Settings were fetched.
        if (!empty($this->response)) {

            // 📝 Foreach each setting
            foreach ($this->response as $index => $setting) {

                $setting_key    = $setting['setting_key'];
                $blueprint      = HelperMethods::getBlueprint($this->blueprintForSettingKeys, $setting_key);

                $updater_layer  = $this->layers['decryption']['updater']['name'];

                $this->layers['decryption']['accessor_layer_name'] = $this->layers['decryption']['name'];
                $this->layers['decryption']['name'] = $updater_layer;

                $layer_config   = HelperMethods::resolveFullLayerConfig($this->layers['decryption'], $blueprint);
                // 📝 Compose decryption class.
                // 👉 api_keys -> DecryptApiKeys
                $class_path = HelperMethods::resolveClassPath($layer_config, $setting_key);

                if (!class_exists($class_path)) {
                    $class_path = HelperMethods::tryToGetDefaultClass($layer_config);
                }

                // ✅ Pass through accessor layer.
                if (class_exists($class_path)) {

                    // 📝 Unset node if accessor returns false.
                    $instance = new $class_path;

                    $instance->config = $this->config;
                    $instance->setting = $setting;
                    $instance->blueprint = $blueprint;

                    $accessor_resp = $instance->handle()->get();

                    if ($accessor_resp) {
                        $this->response[$index] = $accessor_resp;
                    }

                    if ($accessor_resp == false) {
                        unset($this->response[$index]);
                    }
                }
            }
            return $this;
        }
    }

    public function passThroughValidationLayer()
    {
        // ✅ Validation on access is enabled.
        if ($enabled = $this->config['accessor']['validate']['enabled'] ?? false) {

            if ($enabled) {

                $layer_config = $this->config['updater']['layers']['validation'];
                $this->response = LayerHelpers::processEachSetting(function ($setting) use ($layer_config) {
                    $class_path = HelperMethods::resolveClassPath($layer_config, $setting['setting_key']);
                    $for_validation = new SettingDto($setting);
                    $setting['setting_value'] = (new $class_path)->handle($for_validation)->get()->setting_value;
                    return $setting;
                }, $this->response);
            }
        }
    }

    public function passThroughFormatterLayer()
    {
        foreach ($this->response as $index => $setting) {

            $group = collect($this->blueprintForSettingKeys)
                ->where('setting_key', $setting['setting_key'])
                ->first()['group'];

            $setting['group'] = $group;
            $setting = $this->resolveSettingMeta($setting);
            $this->response[$index] = $setting;
        }

        $this->response = array_values($this->response);

        if (isset($this->settingsAccessorData->request_config['verbose']) ?? false) {
            if (($this->settingsAccessorData->request_config['verbose'] == false) ?? false) {
                $this->response = HelperMethods::transformAsKeyToValue($this->response);
            }
        }
    }

    public function resolveSettingMeta($setting)
    {
        $meta = collect($this->settingKeyMetas)->where('setting_key', $setting['setting_key'])->first();

        if ($meta) {
            $setting['meta'] = $meta['meta'];
            return $setting;
        }

        return $setting;
    }

    public function setSettingsAccessorDto(SettingAccessorDto $settingsAccessorData)
    {
        $this->settingsAccessorData = $settingsAccessorData;
        return $this;
    }

    public function setResponse(array $response)
    {
        $this->response = $response;
        return $this;
    }

    private function checkEmptyResponse()
    {
        if (empty($this->response)) throw new ServiceClientException('No settings found.');
    }

    private function finish()
    {
        $this->setRequestedAndFetchedSettingKeys();
        $this->setMissingKeys();
        $this->processResponse();
    }

    public function get()
    {
        $this->passThroughLayers();
        return $this->response;
    }
}
