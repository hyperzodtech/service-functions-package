<?php

namespace App\Actions\Tenant;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\Actions\AbstractSettingUpdater;

class UpdateTenantSettings extends AbstractSettingUpdater
{
    public string $config = 'tenant_settings';
    public array $validationRules = [
        'tenant_id' => 'required',
        'settings' => 'required|array',
        'settings.*.locale' => 'nullable',
        'settings.*.setting_key' => 'required',
        'settings.*.setting_value' => 'nullable',
    ];

    protected function setUpdateIdentifiers(): array
    {
        // Set tenant_id here
        return [];
    }
}
