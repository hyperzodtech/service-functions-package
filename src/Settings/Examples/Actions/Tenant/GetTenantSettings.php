<?php

namespace App\Actions\Tenant;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\Actions\AbstractSettingGetter;

class GetTenantSettings extends AbstractSettingGetter
{
    public string $config = 'tenant_settings';
}
