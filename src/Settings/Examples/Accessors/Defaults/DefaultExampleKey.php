<?php

namespace App\Settings\Accessors\Defaults;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractDefaultSettings;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;

class DefaultExampleKey extends AbstractDefaultSettings
{
    public function handle(): self
    {
        $this->additionalNodes = ['tenant_id' => 1];

        $this->setting_value = [
            'example_field' => 'default Value',
            'password' => HelperMethods::encrypt('default Value')
        ];

        return $this;
    }
}
