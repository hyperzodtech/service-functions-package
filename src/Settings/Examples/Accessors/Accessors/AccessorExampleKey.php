<?php
namespace App\Settings\Accessors\Accessors;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractAccessor;

class AccessorExampleKey extends AbstractAccessor {

    public function handle(): self
    {
        // Modify $this->setting_value here
        return $this;
    }
}