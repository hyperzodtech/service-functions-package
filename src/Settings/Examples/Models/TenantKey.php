<?php
namespace App\Models\Settings\Tenant;

use Hyperzod\HyperzodServiceFunctions\Settings\ConcreteClasses\SettingKeyBlueprint;

class TenantKey extends SettingKeyBlueprint {
    
    protected $groupModel = TenantGroup::class;

}