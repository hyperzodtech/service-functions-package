<?php

return [

    'user_type' => 'tenant',
    'models' => [
        'group_model' => \App\Models\Settings\Tenant\TenantGroup::class,
        'key_model' => \App\Models\Settings\Tenant\TenantKey::class,
        'user_setting_model' => \App\Models\Settings\Tenant\TenantSetting::class
    ],

    # Updater Config
    'updater' => [

        'layers' => [

            // ✅ More layers can be added here.
            'validation' => [
                'name' => 'validation',
                'namespace' => '\App\Settings\Updaters\Validators',
                'required' => true,
                'prefix' => 'Validate'
            ],

            'casting' => [
                'name' => 'casting',
                'namespace' => '\App\Settings\Updaters\Casters',
                'required' => false,
                'prefix' => 'Cast'
            ],

            'encryption' => [
                'name' => 'encryption',
                'namespace' => '\App\Updaters\Encrypters',
                'default_layer' => '\App\Settings\Updaters\Encrypters\DefaultEncrypter',
                'required' => false,
                'prefix' => 'Encrypt',
                'accessor' => [
                    'name' => 'decryption'
                ]
            ]
        ]
    ],

    # Accessor Config
    'accessor' => [

        // Validate setting value while accessing setting❓
        'validate' => [
            'enabled' => true
        ],

        // ❌ More layers cannot be added here.
        'layers' => [

            'default' => [
                'namespace' => 'App\Settings\Accessors\Defaults',
                'prefix' => 'Default',
                'required' => true
            ],

            'accessor' => [
                'namespace' => 'App\Settings\Accessors\Accessors',
                'prefix' => 'Accessor',
                'required' => true
            ],

            'decryption' => [
                'name' => 'decryption',
                'namespace' => 'App\Accessors\Decrypters',
                'prefix' => 'Decrypt',
                'default_layer' => '\App\Settings\Accessors\Decrypters\DefaultDecrypter',
                'required' => true,
                'updater' => [
                    'name' => 'encryption'
                ]
            ]
        ]
    ]
];
