<?php

return [

    // Items
    'setting_keys_blueprint' => [
        'unique_key' => 'setting_key',
        'folder_path' => base_path() . "/database/json/tenant/blueprints/keys_by_group",
        'validation' => [
            'group' => 'required|string',
            'setting_keys' => 'required|array',

            'setting_keys.*.setting_name' => 'required|string|max:100',
            'setting_keys.*.types.*.type' => 'nullable|string',
            'setting_keys.*.types.*.values' => 'nullable|array',
            'setting_keys.*.types.*.values.*' => 'nullable|string',
            'setting_keys.*.has_locale' => 'required|boolean',
            'setting_keys.*.setting_key' => ['required', 'max:100', 'regex:/^(?:\p{Ll}+_)*\p{Ll}+$/u'], // Snakecase regex
            'setting_keys.*.description' => 'nullable|string|max:100',
            'setting_keys.*.tags' => 'nullable|array|max:100',
            'setting_keys.*.tags.*' => 'string|max:50',
            'setting_keys.*.layers' => 'array',
            'setting_keys.*.layers.*' => 'array',
            'setting_keys.*.layers.*.name' => 'required|string',
            'setting_keys.*.layers.*.enabled' => 'required|boolean',
            'setting_keys.*.layers.*.config' => 'array'
        ],
        'layers' => [
            // 'pre-validation' => [
            //     [
            //         // 'class_path' => ''
            //     ],
            // ],
        ]
    ],

    'setting_groups_blueprint' => [
        'unique_key' => 'group_key',
        'folder_path' => base_path() . "/database/json/tenant/blueprints/groups",
        'validation' => [
            '*.group_name' => 'required|string|max:100',
            '*.group_key' => ['required', 'regex:/^(?:\p{Ll}+_)*\p{Ll}+$/u'], // Snakecase regex
            '*.description' => 'nullable|string|max:100',
            '*.parent_group_key' => ['nullable', 'max:100', 'regex:/^(?:\p{Ll}+_)*\p{Ll}+$/u'],
        ]
    ]
];
