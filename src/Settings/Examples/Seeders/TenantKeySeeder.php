<?php
namespace Database\Seeders;

use App\Models\Settings\Tenant\TenantKey;
use Hyperzod\HyperzodServiceFunctions\Settings\Enums\BlueprintTransformerTypeEnum;
use Hyperzod\HyperzodServiceFunctions\Settings\Seeders\BlueprintTransformer;
use Illuminate\Database\Seeder;

class TenantKeySeeder extends Seeder
{
    public function run()
    {
        $blueprint = (new BlueprintTransformer)
                       ->setType(BlueprintTransformerTypeEnum::KEY)
                       ->setConfig('setting_blueprint.setting_keys_blueprint')
                       ->process()
                       ->get();

        TenantKey::truncate();
        TenantKey::insert($blueprint);
    }
}
