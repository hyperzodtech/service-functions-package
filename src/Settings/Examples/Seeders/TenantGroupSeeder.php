<?php
namespace Database\Seeders;

use App\Models\Settings\Tenant\TenantGroup;
use Hyperzod\HyperzodServiceFunctions\Settings\Enums\BlueprintTransformerTypeEnum;
use Hyperzod\HyperzodServiceFunctions\Settings\Seeders\BlueprintTransformer;
use Illuminate\Database\Seeder;

class TenantGroupSeeder extends Seeder
{
    protected array $groupArray;

    public function run()
    {
        $groups = (new BlueprintTransformer)
                       ->setType(BlueprintTransformerTypeEnum::GROUP)
                       ->setConfig('setting_blueprint.setting_groups_blueprint')
                       ->process()
                       ->get();

        TenantGroup::truncate();
        TenantGroup::insert($groups);
    }

}
