<?php
namespace App\Settings\Updaters\Casters;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractCaster;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Traits\HelpersServiceTrait;

class CastExampleKey extends AbstractCaster
{
    use HelpersServiceTrait;

    public function handle(SettingDto $setting): self
    {
        $this->response = $setting;
        return $this;
    }
}
