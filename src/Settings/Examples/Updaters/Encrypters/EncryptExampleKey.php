<?php

namespace App\Settings\Updaters\Encrypters;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractEncryptor;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;

class EncryptExampleKey extends AbstractEncryptor
{
    public function handle(SettingDto $setting): self
    {
        // Encrypt and set $this->response manually here.
        $this->response = $setting;
        return $this;
    }
}