<?php
namespace App\Settings\Updaters\Validators;

use Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\AbstractValidator;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Illuminate\Support\Facades\Validator;

class ValidateExampleKey extends AbstractValidator
{
    public array $rules = [
        'example_field' => 'required|string',
        'password' => 'required|string'
    ];
}
