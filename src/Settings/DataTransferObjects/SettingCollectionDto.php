<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects;

use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\Attributes\MapFrom;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\Casters\ArrayCaster;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class SettingCollectionDto extends DataTransferObject {

    #[CastWith(ArrayCaster::class, itemType: SettingDto::class)]
    public array $collection;

}