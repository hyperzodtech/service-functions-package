<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects;

use Closure;
use Spatie\DataTransferObject\Attributes\Strict;
use Spatie\DataTransferObject\DataTransferObject;

#[Strict]
class SettingAccessorDto extends DataTransferObject {
    
    public ?string $locale = null;
    public array $locales = [];
    public ?array $types;
    public mixed $keys = null;
    public ?string $group_key = null;
    public ?array $tags = null;
    public ?Closure $custom_query = null;
    public array $request_config = [];

}