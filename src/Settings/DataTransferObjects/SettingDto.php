<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class SettingDto extends DataTransferObject {
    
    public ?string $locale = null;
    
    public string $setting_key;
    public ?string $group = null;
    public array $tags = [];
    public mixed $setting_value;
    public ?array $types;

}