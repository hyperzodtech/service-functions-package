<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class ResponseDto extends DataTransferObject {

    public bool $status;
    public string $message;
    public mixed $data;
}