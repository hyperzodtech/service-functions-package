<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects;

use Spatie\DataTransferObject\DataTransferObject;

class DecryptedDto extends DataTransferObject {
    public string $type;
    public mixed $value;
}