<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\ConcreteClasses;

class SettingGroupBlueprint extends BaseModel
{
    public $timestamps = false;

    protected $guarded = [];

    public function hasChildren()
    {
        return $this->hasMany(get_called_class(), 'parent_group_key', 'group_key');
    }

}
