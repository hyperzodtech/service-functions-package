<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\ConcreteClasses;

use Hyperzod\HyperzodServiceFunctions\Scope\TenantScope;
use Hyperzod\HyperzodServiceFunctions\Settings\ConcreteClasses\BaseModel;

class SettingBlueprint extends BaseModel
{
    protected static function booted()
    {
        // static::addGlobalScope(new TenantScope());
    }
}
