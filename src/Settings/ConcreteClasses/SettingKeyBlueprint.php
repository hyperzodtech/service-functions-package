<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\ConcreteClasses;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;

class SettingKeyBlueprint extends BaseModel
{
    public $timestamps = false;

    protected $guarded = [];

    public function tenantGroup()
    {
        if (!$this->groupModel) {
            throw new ServiceClientException('groupModel property not set in ' . get_class($this));
        }

        return $this->belongsTo($this->groupModel, 'group', 'group_key');
    }
}
