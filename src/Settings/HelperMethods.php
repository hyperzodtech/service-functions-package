<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\DecryptedDto;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Illuminate\Support\Str;

class HelperMethods
{
    public static function resolveClassPath($layer_config, $setting_key): string
    {
        $prefix_or_suffix = (new self)->getPrefixOrSuffix($layer_config);

        if ($prefix_or_suffix['suffix'] ?? false) {
            $class_path = $layer_config['namespace'] . "\\" . Str::of($setting_key)->studly() . $prefix_or_suffix['suffix'];
        }

        if ($prefix_or_suffix['prefix'] ?? false) {
            $class_path = $layer_config['namespace'] . "\\" . $prefix_or_suffix['prefix'] . Str::of($setting_key)->studly();
        }

        if (!class_exists($class_path))
            $class_path = self::tryToGetDefaultClass($layer_config);

        return $class_path;
    }

    public static function checkIfLayerIsRequired($layer_config, $class_path)
    {
        $is_required = $layer_config['required'] ?? false;
        if (!class_exists($class_path) && $is_required) {
            throw new ServiceClientException("Class for " . $layer_config['name'] . " is required and does not exist.");
            return true;
        }
    }

    public static function checkIfLayerIsRequiredForGivenKey($layer_config, $class_path, SettingDto $setting, $setting_blueprint)
    {
        $layer_name = $layer_config['name'];

        if (!empty($setting_blueprint['layers'])) {

            foreach ($setting_blueprint['layers'] as $layer) {

                if ($layer['name'] == $layer_name && $layer['enabled'] == true) {
                    return true;
                    // throw new ServiceClientException(ucfirst($layer['name']) . " layer: '" . $class_path . "' is requied for: " . $setting->setting_key);
                }

                if ($layer['name'] == $layer_name && $layer['enabled'] == false) {
                    return false;
                }
            }
        }

        return false;
    }

    public static function checkClassInstance(string $class_path, string $instance)
    {
        if (class_exists($class_path) && new $class_path instanceof $instance) {
            return;
        }

        throw new ServiceClientException("Invalid class: $class_path");
    }

    private function getPrefixOrSuffix(array $layer_config): array
    {
        if (isset($layer_config['suffix'])) {
            $prefix_or_suffix['suffix'] = $layer_config['suffix'];
            $prefix_or_suffix['prefix'] = false;
        }

        if (isset($layer_config['prefix'])) {
            $prefix_or_suffix['prefix'] = $layer_config['prefix'];
            $prefix_or_suffix['suffix'] = false;
        }

        if (isset($layer_config['suffix']) && isset($layer_config['prefix']))
            throw new ServiceClientException("Either a suffix or prefix can be set.");

        if (!isset($layer_config['suffix']) && !isset($layer_config['prefix']))
            throw new ServiceClientException("Either a suffix or prefix must be set.");

        return $prefix_or_suffix;
    }

    public static function encrypt(string|int $value): string
    {
        return encrypt($value);
    }

    public static function decrypt(string|int $value): DecryptedDto
    {
        return new DecryptedDto(type: gettype($value), value: decrypt($value));
    }

    public static function getKeysToDecrypt($blueprint)
    {
        $layers = $blueprint['layers'] ?? false;

        if ($layers) {
            $encryption_layer = collect($blueprint['layers'])
                ->where('name', 'encryption')
                ->first() ?? false;

            if ($encryption_layer) {
                // Keys for decryption are given
                if ($keys_to_decrypt = $encryption_layer['config']['keys'] ?? false)
                    return $keys_to_decrypt;

                return 'setting_value';
            }
        }

        return false;
    }

    public static function encryptionIsEnabled($setting_key, $setting_blueprint)
    {
        $key_with_layers = collect($setting_blueprint)->where('layers')->first();
        if ($key_with_layers) {
            $encryption_layer = collect($key_with_layers['layers'])->where('name', 'encryption')->first();
            $enabled = $encryption_layer['enabled'] ?? false;
            return (bool) $enabled;
        }

        return false;
    }

    public static function resolveFullLayerConfig($layer_config, $setting_blueprint)
    {
        if ($layers = $setting_blueprint['layers'] ?? false) {

            foreach ($layers as $layer) {
                if ($layer['name'] == $layer_config['name']) {
                    // Return full config.
                    return array_merge($layer, $layer_config);
                }
            }
        }

        return $layer_config;
    }

    public static function tryToGetDefaultClass($layer_config)
    {
        $enabled = $layer_config['enabled'] ?? false;
        $default_class = $layer_config['default_layer'] ?? false;

        if ($enabled && !$default_class)
            throw new ServiceClientException("Default class is required for " . $layer_config['name'] . " layer.");

        if ($default_class)
            return $default_class;

        return false;
    }

    public static function getBlueprint(array $blueprints, $setting_key): array
    {
        return collect($blueprints)->where('setting_key', $setting_key)->first();
    }

    public static function processEachSetting(Closure $closure, array $settings): array
    {
        foreach ($settings as $setting) {
            $array[] = $closure($setting);
        }

        return $array;
    }

    public static function transformAsKeyToValue(array $settings)
    {
        $formatted_settings = collect($settings)->map(function ($setting) {

            $formatted[$setting['setting_key']] = $setting['setting_value'];
            return $formatted;
        });

        $formatted_settings = $formatted_settings->flatMap(function ($values) {
            return $values;
        });

        return $formatted_settings->toArray();
    }

    public static function getSettingGroup($setting_key, $blueprints): string
    {
        $setting_blueprint = collect($blueprints)->where('setting_key', $setting_key)->first();
        return $setting_blueprint['group'];
    }

    public static function getApiKey()
    {
        return config('SettingServiceGlobal.ApiKey');
    }
}
