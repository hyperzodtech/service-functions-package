<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractActionV2;

use Exception;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

abstract class AbstractAction
{
    protected $data = null;
    protected $rules = false;
    protected $failed = false;
    protected $message = null;

    public static function setRequest($request)
    {
        $class = get_called_class();
        $instance = new $class;

        $validated = $instance->validate($request);
        $validated = $instance->afterValidation($validated);

        $instance->message = 'Request successful';
        $instance->data = $instance->handle($validated);

        return $instance;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function data()
    {
        return $this->data;
    }

    public function afterValidation($validated)
    {
        return $validated;
    }

    abstract protected function rules($unvalidated);

    abstract protected function handle($request): mixed;

    protected function validate($unvalidated)
    {
        $this->rules = $this->rules($unvalidated);

        if ($this->rules === false) {
            return $unvalidated;
        }

        if ($this->rules === null) {
            return null;
        }

        if ($unvalidated instanceof Request) {
            $unvalidated = $unvalidated->all();
        }

        if (empty($this->rules)) {
            throw new ServiceClientException('Validation rules are empty');
        }

        // When rules has a single value.
        if (count($this->rules) == 1 && !$this->isAssocArray($unvalidated)) {
            return Validator::make([$unvalidated], $this->rules)->validate()[0];
        }

        if (count($this->rules) == 1 && $this->isAssocArray($unvalidated)) {
            $key = array_key_first($this->rules);
            $validated = Validator::make($unvalidated, $this->rules)->validate();

            if (isset($validated[$key])) {
                return $validated[$key];
            }
            return null;
        }

        // When rules is an associative array.
        if (count($this->rules) > 1) {
            return Validator::make($unvalidated, $this->rules)->validate();
        }
    }

    public function failed()
    {
        return $this->failed;
    }

    public function fails()
    {
        return $this->failed();
    }

    public function message()
    {
        return $this->message;
    }

    private function isAssocArray($value)
    {
        if ($value[0] ?? false) {
            return false;
        }

        return true;
    }
}
