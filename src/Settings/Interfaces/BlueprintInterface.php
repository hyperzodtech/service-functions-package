<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

interface BlueprintInterface {
    public function setConfig(string $config);
    public function overrideFolderPath(string $path);
    public function overridePreValidationLayers(array $layer);
    public function appendPreValidationLayer(array $layer);
    public function process();
    public function get(): array;
}