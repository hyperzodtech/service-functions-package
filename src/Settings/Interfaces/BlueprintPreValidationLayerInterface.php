<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

interface BlueprintPreValidationLayerInterface {
    public function setArrayOfBlueprints(array $blueprints): self;
    public function setGroup(string $group): self;
    public function setConfig(array $config): self;
    public function process();
    public function get(): array;
}