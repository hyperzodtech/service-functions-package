<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

use App\DataTransferObjects\SettingDto;

interface IsSettingAccessorLayer {

    public function handle(SettingDto $setting): self;
    public function get(): SettingDto;
}