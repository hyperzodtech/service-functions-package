<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;

interface ValidatorInterface {
    public function handle(SettingDto $setting_for_validation): self;
}