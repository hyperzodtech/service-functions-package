<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

interface CasterInterface {
    public function cast($value);
}