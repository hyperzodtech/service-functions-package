<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;

interface IsSettingUpdaterLayer {

    public function handle(SettingDto $setting): self;
    public function get(): SettingDto;
}