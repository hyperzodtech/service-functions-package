<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

interface AccessorInterface {
    public function handle(): self;
    public function get();
}