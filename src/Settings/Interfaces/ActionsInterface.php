<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Interfaces;

use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\ResponseDto;

interface ActionsInterface {
    public function handle($data): self;
    public function getResponse(): ResponseDto;
}