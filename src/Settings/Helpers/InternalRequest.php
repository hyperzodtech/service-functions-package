<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\Helpers;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;

class InternalRequest
{
    const CONFIG = 'internal_request';

    public function __construct()
    {
        $this->checkConfig();
    }

    public static function setRequestAsInternal($type)
    {
        self::setType($type);

        config([self::CONFIG . '.enabled' => true]);

        return new self;
    }

    private static function setType($type): void
    {
        config()->set(self::CONFIG . '.type', $type);
    }

    public static function getType(): string
    {
        if (is_null(config(self::CONFIG . '.type'))) err("Internal Request: 'type' is not set");
        return config(self::CONFIG . '.type');
    }

    public static function setConfig(mixed $config)
    {
        config([self::CONFIG . '.config' => $config]);
    }

    public static function getConfig()
    {
        if (is_null(config(self::CONFIG))){
            throw new \Exception('Internal operations not configured');
        }

        return config(self::CONFIG . '.config');
    }

    public static function isInternal(): bool
    {
        if (config(self::CONFIG . '.enabled') == true) {
            return true;
        }

        return false;

    }

    ######
    private function checkConfig()
    {
        if (is_null(config(self::CONFIG))) {
            throw new ServiceClientException('Internal operations not set in the config folder.');
        }

        if (is_null(config(self::CONFIG . '.enabled'))) {
            throw new ServiceClientException('Internal operations: enabled field not found.');
        }

        if (is_null(config(self::CONFIG . '.config'))) {
            throw new ServiceClientException('Internal operations: config field not set.');
        }
    }

}