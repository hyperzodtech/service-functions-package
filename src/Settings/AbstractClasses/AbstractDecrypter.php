<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;
use Illuminate\Support\Arr;

abstract class AbstractDecrypter extends AbstractAccessor
{
    public function handle(): self
    {
        $setting = $this->setting;
        $keys_to_decrypt = HelperMethods::getKeysToDecrypt($this->blueprint);

        if ($keys_to_decrypt == 'setting_value') {

            if ($setting['setting_value'])
                $setting['setting_value'] = $this->decrypt($setting['setting_value']);
        }

        if (is_array($keys_to_decrypt)) {

            foreach ($keys_to_decrypt as $decryptable_key) {

                $key_exists = Arr::get($setting['setting_value'], $decryptable_key, false);

                if (is_array($setting['setting_value']) && $key_exists) {
                    $encrypted = Arr::get($setting['setting_value'], $decryptable_key);
                    $decrypted = $this->decrypt($encrypted);
                    Arr::set($setting['setting_value'], $decryptable_key, $decrypted);
                }
            }
        }

        $this->response = $setting;
        return $this;
    }

    public function get()
    {
        return $this->response;
    }
}
