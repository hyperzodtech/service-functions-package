<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\IsSettingUpdaterLayer;

abstract class AbstractUpdaterLayer implements IsSettingUpdaterLayer
{
    protected SettingDto $setting;
    protected SettingDto $response;
    protected array $config;
    protected array $blueprint;

    public function setSetting(SettingDto $setting)
    {
        $this->setting = $setting;
        return $this;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
        return $this;
    }

    public function setBlueprint(array $blueprint)
    {
        $this->blueprint = $blueprint;
    }

    public function get(): SettingDto
    {
        return $this->response;
    }

    public function encrypt(string|int $value): string
    {
        $encrypted = HelperMethods::encrypt($value);
        return $encrypted;
    }
}