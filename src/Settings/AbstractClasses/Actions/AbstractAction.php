<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\Actions;

use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\ResponseDto;
use Illuminate\Http\Request;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\ActionsInterface;

abstract class AbstractAction implements ActionsInterface {
    
    protected Request $request;
    protected $data;
    protected ResponseDto $response;

    public function handle($data): self
    {
        $this->response = $this->performAction($data);
        return $this;
    }

    public function performAction($data): ResponseDto
    {
        return new ResponseDto(
            status: false,
            message: 'No response was set.',
            data: null
        );
    }

    public function getResponse(): ResponseDto
    {
        return $this->response;
    }

}