<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\Actions;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\ResponseDto;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingAccessorDto;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingCollectionDto;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\ActionsInterface;
use Hyperzod\HyperzodServiceFunctions\Settings\LayerHelpers;
use Hyperzod\HyperzodServiceFunctions\Settings\SettingsAccessor;
use Hyperzod\HyperzodServiceFunctions\Settings\SettingsUpdater;

abstract class AbstractSettingUpdater implements ActionsInterface
{
    public string $config;
    public array $validationRules;

    protected $requestData;
    protected array $validatedRequest;

    public array $blueprints;
    protected array $processedSettings;
    public ?Closure $beforeUpdateClosure = null;
    protected array $settingsUpdated;
    protected array $updatedKeys;

    public array $updateIdentifiers = [];
    protected ResponseDto $response;

    public function handle($request_data): self
    {
        $this->requestData = $request_data;
        $this->setGlobalValues();

        $this->beforeHandle();
        $this->validateRequest();
        $this->processSettingsArray();
        $this->updateOrCreateDbEntries();
        $this->resolveResponse();

        return $this;
    }

    public function setGlobalValues()
    {
        if (isset($this->requestData['api_key'])) {
            config()->set('SettingServiceGlobal.ApiKey', $this->requestData['api_key']);
        }
    }

    public function beforeHandle()
    {
    }

    protected function rules(): array
    {
        return $this->validationRules;
    }

    protected function validateRequest()
    {
        $rules = $this->rules();

        $this->validatedRequest = validator()->make(
            $this->requestData,
            $rules
        )->validate();
    }
    
    protected function beforeProcessSettingsArray($request)
    {
        return $request;
    }

    protected function processSettingsArray()
    {
        $this->validatedRequest = $this->beforeProcessSettingsArray($this->validatedRequest);
        $updater = new SettingsUpdater;
        $updater->setConfig($this->config);
        $updater->handle(new SettingCollectionDto(collection: $this->validatedRequest['settings']));
        $this->blueprints = $updater->getBlueprints();
        $this->processedSettings = $updater->get();
    }

    protected function updateOrCreateDbEntries()
    {
        $model = config($this->config)['models']['user_setting_model'];

        foreach ($this->processedSettings as $setting) {

            if ($closure = $this->beforeUpdateClosure)
                $closure($model, $setting);

            $this->settingsUpdated[] = $model::updateOrCreate(

                array_merge(
                    $this->setUpdateIdentifiers(),
                    [
                        'setting_key' => $setting['setting_key']
                    ],
                    $this->resolveLocale($setting),
                    $this->resolveTypes($setting)
                ),
                [
                    'setting_value' => $setting['setting_value']
                ]
            )->toArray();
        }

        $this->afterUpdate($this);
        $this->runThroughAfterUpdateLayers($this->settingsUpdated);

        $this->updatedKeys = collect($this->settingsUpdated)->pluck('setting_key')->toArray();
    }

    public function resolveLocale(array $setting): array
    {
        if ($setting['locale'] ?? false) {
            return ['locale' => $setting['locale']];
        }
        return [];
    }

    public function resolveTypes(array $setting): array
    {
        if (isset($setting['types'])) {
            return $setting['types'];
        }

        return [];
    }

    protected function setUpdateIdentifiers(): array
    {
        return $this->updateIdentifiers;
    }

    public function afterUpdate($obj)
    {
    }

    protected function runThroughAfterUpdateLayers($settingsUpdated)
    {
        try {
            if (
                config($this->config . ".updater.config.enable_after_update_layers") === false ||
                !config($this->config . ".updater.after_update_layers")
            ) {
                return;
            }

            LayerHelpers::processEachSetting(function ($setting) {

                $layers = config($this->config . ".updater.after_update_layers");

                foreach ($layers as $layer) {
                    $class = LayerHelpers::resolveClassPath($layer, $setting['setting_key']);
                    if (class_exists($class)) (new $class)->handle($setting);
                }
            }, $settingsUpdated);
        } catch (\Throwable $th) {
            return false;
        }
    }

    protected function resolveResponse()
    {
        try {

            $accessedSettings = (new SettingsAccessor)
                ->setConfig($this->config)
                ->setProcessedSettings($this->processedSettings)
                ->setSettingsAccessorDto(new SettingAccessorDto(
                    keys: $this->updatedKeys,
                    locale: app()->getLocale(),
                    request_config: $this->requestData['config'] ?? []
                ))
                ->resolveSettingsForUpdatedKeys()
                ->get();

            $this->response = new ResponseDto(
                status: true,
                message: "Settings updated successfully.",
                data: $accessedSettings
            );
        } catch (\Throwable $th) {
            $this->response = new ResponseDto(
                status: false,
                message: $th->getMessage(),
                data: $th->getMessage()
            );
        }
    }

    public function getBlueprints(): array
    {
        return $this->blueprints;
    }

    public function getResponse(): ResponseDto
    {
        return $this->response;
    }
}
