<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\Actions;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\ResponseDto;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingAccessorDto;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\ActionsInterface;
use Hyperzod\HyperzodServiceFunctions\Settings\SettingsAccessor;
abstract class AbstractSettingGetter implements ActionsInterface
{
    public string $config;
    protected $requestData;
    protected SettingAccessorDto $settingAccessorDto;
    protected $requestedSettings;

    protected ResponseDto $response;

    public ?Closure $queryClosure = null;

    public function beforeHandle(){}

    public function handle($request_data): self
    {
        $this->requestData = $request_data;
        
        $this->beforeHandle();
        $this->validateSettings();

        $this->setSettingAccessorDto();
        $this->resolveRequestedSettings();

        return $this;
    }

    private function validateSettings()
    {
        $this->requestData = validator()->make($this->requestData, [
            'keys' => 'nullable',
            'group_key' => 'nullable',
            'types' => 'nullable|array',
            'tags' => 'nullable',
            'config' => 'nullable',
            'locales' => 'nullable|array',
        ])->validate();

    }

    public function setSettingAccessorDto()
    {
        $this->settingAccessorDto = new SettingAccessorDto(
            locale: app()->getLocale(),
            keys: $this->requestData['keys'] ?? null,
            types: $this->requestData['types'] ?? null,
            group_key: $this->requestData['group_key'] ?? null,
            tags: $this->requestData['tags'] ?? [],
            custom_query: $this->queryClosure,
            request_config: $this->requestData['config'] ?? [],
            locales: $this->requestData['locales'] ?? [],
        );
    }

    public function resolveRequestedSettings()
    {
        try {
            $this->requestedSettings = (new SettingsAccessor)
            ->setConfig($this->config)
            ->handle($this->settingAccessorDto)
            ->get();
        
            $this->response = new ResponseDto(
            status: true,
            message: "Settings fetched successfully.",
            data: $this->requestedSettings,
        );
        } catch (\Throwable $th) {
            $this->response = new ResponseDto(
                status: false,
                message: $th->getMessage(),
                data: null
            );
        }
    }

    public function getResponse(): ResponseDto
    {
        return $this->response;
    }
}
