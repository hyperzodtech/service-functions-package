<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Illuminate\Support\Facades\Validator;
use Spatie\DataTransferObject\DataTransferObject;

abstract class AbstractValidator extends AbstractUpdaterLayer
{
    public array $rules = [];
    
    public function setDto(): string|null
    {
        return null;
    }

    public function conformToDto($settingValue)
    {
        $dtoClass = $this->setDto();

        if ($dtoClass) {
            $settingValue = (new $dtoClass($settingValue))->toArray();
        }

        return $settingValue;
    }

    public function handle(SettingDto $setting): self
    {
        $setting->setting_value = $this->conformToDto($setting->setting_value);
        
        $this->rules = $this->rules();

        // When rules has a single value.
        // if (!empty($this->rules) && count($this->rules) == 1) {
        //     $setting->setting_value = Validator::make([$setting->setting_value], $this->rules)->validate();
        // }

        // When rules is an associative array.
        if (!empty($this->rules) && count($this->rules) > 1) {
            $setting->setting_value = Validator::make($setting->setting_value, $this->rules)->validate();
        }

        if (empty($this->rules)) {
            throw new ServiceClientException('Empty validation rules for: ' . $setting->setting_key);
        }
        
        $this->response = $setting;
        return $this;
    }

    protected function rules(): array
    {
        return $this->rules;
    }

    public function get(): SettingDto
    {
        return $this->response;
    }

}