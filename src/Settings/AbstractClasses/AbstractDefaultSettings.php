<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\DefaultSettingInterface;

abstract class AbstractDefaultSettings extends AbstractAccessorLayer
{
    public $setting_key;
    public $group;
    public $skipSetting = false;
    public array $additionalNodes = [];
    public array $defaultSetting;
    public array $config;
    public $response;
    public $settingsAccessorData;

    public $setting_value;

    public function init()
    {
        $this->defaultSetting = [
             "setting_key" => $this->setting_key,
             "group" => $this->group,
             "setting_value" => null
        ];
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    public function handle(): self
    {
        return $this;
    }

    public function get()
    {
        if ($this->skipSetting === true) {
            return false;
        }

        if (isset($this->setting_value) || is_null($this->setting_value)) {

            $this->defaultSetting['setting_value'] = $this->setting_value;

            $this->validateSetting();

            return $this->response;
        }

        return false;

    }

    private function validateSetting()
    {
        $layer_config    = $this->config['updater']['layers']['validation'];
        $class_path      = HelperMethods::resolveClassPath($layer_config, $this->setting_key);

        $setting_dto     = new SettingDto($this->defaultSetting);
        $instance        = new $class_path;
        $instance->setSetting($setting_dto);
        $validated_value = $instance->handle($setting_dto)->get()->setting_value;

        $this->defaultSetting['setting_value'] = $validated_value;
        $this->defaultSetting = array_merge($this->additionalNodes, $this->defaultSetting);

        $add_null = false;
        if ($this->defaultSetting['setting_value'] === null) {
            $add_null = true;
        }

        $response = collect($this->defaultSetting)->filter()->toArray();

        if ($add_null) {
            $response['setting_value'] = null;
        }

        $this->response = $response;

    }
}
