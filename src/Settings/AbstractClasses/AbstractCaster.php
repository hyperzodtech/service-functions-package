<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\IsCastingLayer;

abstract class AbstractCaster extends AbstractUpdaterLayer implements IsCastingLayer
{
    public function get(): SettingDto
    {
        if (!isset($this->response)) throw new ServiceClientException("Casting response not set.");

        $layer_config = $this->config['updater']['layers']['validation'];
        $class_path = HelperMethods::resolveClassPath($layer_config, $this->response->setting_key);

        $for_validation = new SettingDto($this->response->toArray());

        $instance = new $class_path;
        $this->response->setting_value = $instance->setSetting($for_validation)
                                                  ->handle($for_validation)
                                                  ->get()
                                                  ->setting_value;

        return $this->response;
    }
}