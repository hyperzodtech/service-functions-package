<?php
namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\AccessorInterface;

abstract class AbstractAccessorLayer implements AccessorInterface
{
    public $skipSetting = false;
    public $setting;
    public array $config;
    public array $blueprint;

    // Modify this property
    public $setting_value;

    public function init()
    {
        $this->setting_value = $this->setting['setting_value'];
    }

    public function handle(): self
    {
        // Do something with $this->setting here.
        return $this;
    }

    public function get()
    {
        if ($this->skipSetting) {
            return false;
        }

        $layer_config = $this->config['updater']['layers']['validation'];
        $class_path = HelperMethods::resolveClassPath($layer_config, $this->setting['setting_key']);

        $this->setting['setting_value'] = $this->setting_value;
        
        $for_validation = new SettingDto($this->setting);
        
        $instance = new $class_path;
        $this->setting['setting_value'] = $instance->setSetting($for_validation)
                                                  ->handle($for_validation)
                                                  ->get()
                                                  ->setting_value;

        return $this->setting;
    }

    public function decrypt(string $value)
    {
        return HelperMethods::decrypt($value)->value;
    }

}