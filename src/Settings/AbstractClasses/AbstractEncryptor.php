<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\IsEncryptorLayer;
use Illuminate\Support\Arr;

abstract class AbstractEncryptor extends AbstractUpdaterLayer implements IsEncryptorLayer
{
    public function handle(SettingDto $setting): self
    {
        $config_for_setting_key = $this->getLayerConfigForSettingKey();
        $is_enabled = $config_for_setting_key['enabled'] ?? false;
        $keys_to_encrypt = $config_for_setting_key['config']['keys'] ?? false;
        
        $setting_value = $setting->setting_value;

        if ($keys_to_encrypt && $is_enabled) {
            
            foreach ($keys_to_encrypt as $encrytable) {
                $key_exists = Arr::get($setting_value, $encrytable, false);
                if ($key_exists) {
                    $encrypted = $this->encrypt(Arr::get($setting_value, $encrytable));
                    Arr::set($setting_value, $encrytable, $encrypted);
                    $setting->setting_value = $setting_value;
                }
            }
        }

        if (!$keys_to_encrypt && $is_enabled) {
            if (!is_array($setting_value))
            $setting->setting_value = $this->encrypt($setting_value);
        }

        $this->response = $setting;
        return $this;
    }

    public function get(): SettingDto
    {
        return $this->response;
    }

    private function getLayerConfigForSettingKey()
    {
        if (!empty($this->blueprint['layers'])) {
            return collect($this->blueprint['layers'])->where('name', 'encryption')->first();
        }
    }
}