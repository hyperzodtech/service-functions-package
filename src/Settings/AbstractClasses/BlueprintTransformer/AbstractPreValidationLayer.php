<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses\BlueprintTransformer;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\BlueprintPreValidationLayerInterface;
use Illuminate\Support\Arr;

abstract class AbstractPreValidationLayer implements BlueprintPreValidationLayerInterface
{
    protected array $currentItem;
    protected array $arrayOfSettingKeyBlueprints;
    protected array $response;
    protected string $group;
    protected array $config;

    public function setArrayOfBlueprints(array $arrayOfSettingKeyBlueprints): self
    {
        $this->arrayOfSettingKeyBlueprints = $arrayOfSettingKeyBlueprints;
        return $this;
    }

    public function setGroup($group): self
    {
        $this->group = $group;
        return $this;
    }

    public function setConfig(array $config): self
    {
        $this->config = $config;
        return $this;
    }

    public function get(): array
    {
        return $this->response;
    }

}
