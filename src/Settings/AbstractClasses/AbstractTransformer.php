<?php

namespace Hyperzod\HyperzodServiceFunctions\Settings\AbstractClasses;

use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Settings\DataTransferObjects\SettingDto;
use Hyperzod\HyperzodServiceFunctions\Settings\HelperMethods;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\AccessorInterface;
use Hyperzod\HyperzodServiceFunctions\Settings\Interfaces\BlueprintInterface;
use Hyperzod\HyperzodServiceFunctions\Settings\Seeders\Validators\ValidateDuplicates;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Arr;

abstract class AbstractTransformer implements BlueprintInterface
{
    protected array $config;
    protected string $folderPath;
    protected array $validationRules;
    protected array $jsonPaths;
    protected array $response;

    public function setConfig(String $config)
    {
        try {
            
            $this->config = config($config);
            $this->folderPath = $this->config['folder_path'];
            $this->validationRules = $this->config['validation'];


        } catch (\Throwable $th) {
            
            throw new ServiceClientException("Error setting config", [$th->getMessage()]);

        } finally {
            return $this;
        }
    }

    public function overrideFolderPath($path)
    {
        $this->folderPath = $path;
        return $this;
    }

    public function overridePreValidationLayers(array $layers)
    {
        $this->config['layers'] = ['pre-validation' => [$layers]];
    }

    public function appendPreValidationLayer(array $layer)
    {
        if ($this->config['layers']['pre-validation'] ?? false) {
            $this->config['layers']['pre-validation'][] = $layer;
        }
    }

    private function init()
    {
        // Set Json paths
        if (!is_dir($this->folderPath)) {
            throw new ServiceClientException("Directory [".$this->folderPath."] does not exist");
        }

        // Get all the json files
        $this->jsonPaths = glob($this->folderPath . "/*.json");
    }

    public function process()
    {
        $this->init();
        $this->checkJsonPaths();

        // Validate each file and set the array
        foreach ($this->jsonPaths as $path) {

            $json_file_contents      = json_decode(file_get_contents($path), true);
            $json_file_contents      = $this->passThroughPreValidationLayers($json_file_contents);
            $validated_file_contents = $this->getValidatedFileContents($json_file_contents);

            $this->formatAndSet($validated_file_contents);
        }

        return $this;
    }

    public function checkJsonPaths()
    {
        if (!isset($this->jsonPaths)) throw new ServiceClientException("No json paths set");
    }

    protected function passThroughPreValidationLayers($json_file_contents): array
    {
        return $json_file_contents;
    }

    private function getValidatedFileContents(array $json_file_contents): array
    {
        $validator = Validator::make($json_file_contents, $this->validationRules);
        
        if ($validator->fails()) {
            print "Error in:"  . json_encode($validator->errors(), JSON_PRETTY_PRINT) . "\n";
            throw new ServiceClientException(json_encode($validator->errors(), JSON_PRETTY_PRINT));
        }

        return $validator->validated();
    }

    abstract protected function formatAndSet(array $validated_file_contents);

    public function passThroughPreResponseLayers()
    {
        (new ValidateDuplicates)->validate($this->response, $this->config['unique_key']);
    }

    public function get(): array
    {
        $this->passThroughPreResponseLayers();
        return $this->response;
    }
}
