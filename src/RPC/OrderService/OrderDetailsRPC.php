<?php

namespace Hyperzod\HyperzodServiceFunctions\RPC\OrderService;

use Hyperzod\HyperzodServiceFunctions\Dto\Auth\UserDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Order\OrderDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\RPC\AbstractRPC;
use Hyperzod\HyperzodServiceFunctions\ValueObjects\CurrencyVO;

class OrderDetailsRPC extends AbstractRPC
{
   protected $service = ServiceEnum::ORDER;
   protected $service_method = 'GET';
   protected $service_path = '/fetch';

   public function handle(int $order_id): OrderDTO
   {
      $this->request_params = [
         TerminologyEnum::TENANT_ID => HyperzodServiceFunctions::getTenantId(),
         TerminologyEnum::ORDER_ID => intval($order_id),
         "select_columns" => [
            "tenant_id",
            "merchant_id",
            "user_id",
            "order_id",
            "user",
            "cart",
            "merchant",
            "payment_mode",
            "order_type",
            "order_status",
            "order_amount",
            "currency",
            "delivery_address",
            "commission",
            "created_at"
         ]
      ];

      $data = $this->doRPC();

      $data['currency'] = CurrencyVO::from($data['currency']);

      return new OrderDTO($data);
   }
}
