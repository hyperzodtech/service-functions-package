<?php

namespace Hyperzod\HyperzodServiceFunctions\RPC;

use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;

abstract class AbstractRPC
{
   use ServiceConsumerTrait;

   protected $service;
   protected $service_method = 'GET';
   protected $service_path = '/';
   protected $request_params = [];

   protected function doRPC(): array
   {
      $request = new \Illuminate\Http\Request();
      $request->query->add($this->request_params);

      $resp = $this->consumeService($this->service, $request, $this->service_method, $this->service_path);

      if (isset($resp['success']) && $resp['success'] == true) {
         return $resp['data'];
      }

      throw new ServiceClientException("RPC error for " . get_class($this));
   }
}
