<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Helper;

use Spatie\DataTransferObject\DataTransferObject;

class CurrencyDTO extends DataTransferObject
{
    public string $code;
    public string $name;
    public string $name_plural;
    public string $symbol;
    public string $symbol_native;
    public int $decimal_digits;
    public int $rounding;
}
