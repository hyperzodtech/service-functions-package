<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Order;

use Hyperzod\HyperzodServiceFunctions\Dto\Address\AddressDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Cart\CartDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Auth\UserDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Merchant\MerchantDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Payment\PaymentModeDTO;
use Hyperzod\HyperzodServiceFunctions\ValueObjects\CurrencyVO;
use Spatie\DataTransferObject\DataTransferObject;

class OrderDTO extends DataTransferObject
{
    public int $tenant_id;
    public string $merchant_id;
    public int $user_id;
    public int $order_id;
    public UserDTO $user;
    public ?CartDTO $cart;
    public ?MerchantDTO $merchant;
    public PaymentModeDTO $payment_mode;
    public string $order_type;
    public int $order_status;
    public float $order_amount;
    public CurrencyVO $currency;
    public ?AddressDTO $delivery_address;
    public ?CommissionDTO $commission;
    public string $created_at;
}
