<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Order;

use Spatie\DataTransferObject\DataTransferObject;

class CommissionDTO extends DataTransferObject
{
    public string $type;
    public float $amount;
    public array $meta;
}
