<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Catalog;

use Spatie\DataTransferObject\DataTransferObject;

class ProductDTO extends DataTransferObject
{
   public int $tenant_id;
   public string $merchant_id;
   public array $language_translation;
   public array|null $product_category;
   public array $product_pricing;
   public int $sort_order;
   public bool $is_featured;
   public bool $has_product_options;
   public array|null $product_options;
   public bool $status;
   public bool $is_quantity_enabled;
   public array|null $product_quantity;
   public bool $is_inventory_enabled;
   public int|null $product_inventory;
   public array|null $product_labels;
   public array|null $product_tags;
   public array|null $product_images;
   public string|null $sku;
   public bool|null $is_dummy;
}
