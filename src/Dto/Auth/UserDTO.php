<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Auth;

use Spatie\DataTransferObject\DataTransferObject;

class UserDTO extends DataTransferObject
{
    public int $id;
    public ?int $tenant_id;
    public string $full_name;
    public string $first_name;
    public string|null $last_name;
    public string|null $email;
    public string|null $mobile;
    public int $status;
    public array $roles;
    public array|null $merchant_users;
    public array $permissions;
}
