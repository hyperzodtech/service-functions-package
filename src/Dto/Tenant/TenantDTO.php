<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Tenant;

use Spatie\DataTransferObject\DataTransferObject;

class TenantDTO extends DataTransferObject
{
    public int $id;
    public string $name;
    public string $slug;
    public string $domain;
    public string $native_domain_ordering;
    public string $native_domain_admin;
    public string|null $admin_domain;
    public int $status;
    public bool $is_open;
    public array $saas_modules;
    public array|null $business_type;
}
