<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Log;

use Spatie\DataTransferObject\DataTransferObject;

class HttpLogDTO extends DataTransferObject
{
    public int|null $tenant_id;
    public string $host;
    public string $path;
    public array $headers;
    public string $method;
}
