<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Log;

use Spatie\DataTransferObject\DataTransferObject;

class TenantHttpRequestLogDTO extends DataTransferObject
{
    public int $tenant_id;
    public string $tenant_name;
    public string $host;
    public string $path;
    public array $headers;
    public string $method;
}
