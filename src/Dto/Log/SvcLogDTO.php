<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Log;

use Spatie\DataTransferObject\DataTransferObject;

class SvcLogDTO extends DataTransferObject
{
    public string $service;
    public string $env;
    public string $timestamp;
    public string $log_level;
    public string $logger;
    public string $message;
    public ?array $meta;
}
