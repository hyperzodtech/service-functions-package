<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Merchant;

use Spatie\DataTransferObject\DataTransferObject;

class MerchantDTO extends DataTransferObject
{
    public int $tenant_id;
    public string $merchant_id;
    public string $name;
    public string|null $phone;
    public string|null $email;
    public string|null $address;
    public string $delivery_by;
    public string $slug;
    public string|null $post_code;
    public string|null $city;
    public string|null $state;
    public string|null $country;
    public string|null $country_code;
    public string|null $owner_name;
    public string|null $owner_phone;
    public array $accepted_order_types;
    public string|null $merchant_package_id;
    public array|null $group_ids;
    public array|null $merchant_category_ids;
    public bool $status;
    public float $delivery_amount;
    public float $min_order_amount;
    public float $average_rating;
    public bool $is_accepting_orders;
    public bool $is_open;
    public array $commission;
}
