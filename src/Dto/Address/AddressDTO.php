<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Address;

use Hyperzod\HyperzodServiceFunctions\ValueObjects\LatLonVO;
use Spatie\DataTransferObject\DataTransferObject;

class AddressDTO extends DataTransferObject
{
    public int $tenant_id;
    public int $user_id;
    public string $address_type;
    public array $location_lat_lon;
    public string $address;
    public string|null $building;
    public string|null $area;
    public string|null $landmark;
    public string|null $city;
    public string|null $region;
    public string|null $zip_code;
    public string $country;
    public string $country_code;
}
