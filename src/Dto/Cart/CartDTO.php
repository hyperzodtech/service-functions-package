<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Cart;

use Spatie\DataTransferObject\DataTransferObject;

class CartDTO extends DataTransferObject
{
    public float $sub_total_amount;
    public float $tax;
    public bool $is_delivery_fee_applied;
    public float $delivery_fee;
    public bool $is_delivery_tax_applied;
    public float $delivery_tax;
    public string $tax_method;
    public bool $is_tip_applied;
    public float $tip_amount;
    public array $custom_charges;
    public ?array $coupon;
    public ?float $discount_amount;
}
