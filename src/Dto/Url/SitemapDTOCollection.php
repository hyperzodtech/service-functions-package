<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Url;

use Spatie\DataTransferObject\DataTransferObject;

class SitemapDTOCollection extends DataTransferObject
{
   /** @var \Hyperzod\HyperzodServiceFunctions\Dto\Url\SitemapDTO[] */
   public array $collection;
}
