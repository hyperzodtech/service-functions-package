<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Url;

use Illuminate\Support\Carbon;
use Spatie\DataTransferObject\DataTransferObject;

class SitemapDTO extends DataTransferObject
{
   public string $uri;
   public Carbon $last_modified;
}
