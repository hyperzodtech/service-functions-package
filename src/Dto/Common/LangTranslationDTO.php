<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Common;

use Spatie\DataTransferObject\DataTransferObject;

class LangTranslationDTO extends DataTransferObject
{
   public string $locale;
   public string $key;
   public string $value;
}
