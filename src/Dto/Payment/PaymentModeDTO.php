<?php

namespace Hyperzod\HyperzodServiceFunctions\Dto\Payment;

use Spatie\DataTransferObject\DataTransferObject;

class PaymentModeDTO extends DataTransferObject
{
    public int $payment_mode_id;
    public string $alias;
}
