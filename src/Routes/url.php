<?php

use Hyperzod\HyperzodServiceFunctions\Actions\Url\GenerateSitemapAction;
use Illuminate\Support\Facades\Route;

Route::get('/sitemap', GenerateSitemapAction::class)->middleware('api');
