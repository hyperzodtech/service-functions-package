<?php

use Illuminate\Support\Facades\Route;
use Hyperzod\HyperzodServiceFunctions\Common\FlushModels;

Route::middleware('api')->any('/flush', [FlushModels::class, 'flushTenant']);
Route::middleware('api')->any('/flush/merchant', [FlushModels::class, 'flushMerchant']);

Route::withoutMiddleware([HasTenantId::class])->get('/', function () {
    return response()->json([
        'service' => config('app.name'),
        'version' => app()->version(),
    ]);
});
