<?php
// laravel command boilerplate
namespace Hyperzod\HyperzodServiceFunctions\Console\Commands;

use Hyperzod\HyperzodServiceFunctions\Cache\TenantCache;
use Illuminate\Console\Command;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;

class TenantCacheFlushCommand extends Command
{

    protected $signature = 'tenant:cache {tenant_id}';

    protected $description = 'Flush cache for tenant';

    public function handle()
    {
        $tenant_id = $this->argument(TerminologyEnum::TENANT_ID);
        $this->info('Flushing cache for tenant: ' . $tenant_id);

        $cache_tag = TenantCache::TENANT_CACHE_KEY_PREFIX . $tenant_id;

        $this->call("cache:clear", [
            '--tags' => $cache_tag
        ]);
    }
}
