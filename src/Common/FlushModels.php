<?php

namespace Hyperzod\HyperzodServiceFunctions\Common;

use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;

class FlushModels
{
    use ApiResponseTrait;

    public function flushTenant(\Illuminate\Http\Request $request)
    {
        HyperzodServiceFunctionsFacade::setGlobal('flush_flag', true);

        if ($this->tenantModelsCanBeFlushed($request)) {
            foreach (config('flush.tenant_models') as $model) {
                $data[] = $model::withoutGlobalScopes()->where(TerminologyEnum::TENANT_ID, (int) $request->tenant_id)->delete();
            }
        }

        if (!$this->tenantModelsCanBeFlushed($request))
            throw new ServiceClientException('Error flushing tenant models.');

        return ['message' => 'flushed successfully!', 'data' => $data];
    }

    public function flushMerchant(\Illuminate\Http\Request $request)
    {
        HyperzodServiceFunctionsFacade::setGlobal('flush_flag', true);

        if ($this->merchantModelsCanBeFlushed($request)) {
            foreach (config('flush.merchant_models') as $model) {
                $data[] = $model::where([
                    TerminologyEnum::TENANT_ID => (int) $request->tenant_id,
                    TerminologyEnum::MERCHANT_ID => $request->merchant_id,
                ])->delete();
            }
        }

        if (!$this->merchantModelsCanBeFlushed($request))
            throw new ServiceClientException('Error flushing merchant models.');

        return ['message' => 'flushed successfully!', 'data' => $data];
    }

    public function merchantModelsCanBeFlushed(\Illuminate\Http\Request $request)
    {
        $validated = $request->validate(
            [
                TerminologyEnum::TENANT_ID => 'required|integer',
                'merchant_id' => 'required'
            ]
        );

        return (bool) config('flush.merchant_models') && (bool) $validated;
    }

    public function tenantModelsCanBeFlushed(\Illuminate\Http\Request $request)
    {
        $validated = $request->validate([TerminologyEnum::TENANT_ID => 'required|integer']);

        return (bool) config('flush.tenant_models') && (bool) $validated;
    }
}
