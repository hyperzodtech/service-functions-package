<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ApmSetUserContextMiddleware
{
    public function handle($request, Closure $next)
    {
        \Sentry\configureScope(function (\Sentry\State\Scope $scope): void {
            # Set User Context
            if ($user = HyperzodServiceFunctions::getUser(false)) {
                $scope->setUser([
                    'id' => $user->id,
                    'name' => $user->full_name,
                    'email' => $user->email,
                    'roles' => $user->roles,
                ]);
            }
        });

        return $next($request);
    }
}
