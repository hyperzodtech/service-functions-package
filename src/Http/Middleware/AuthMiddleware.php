<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Exceptions\Auth\UnauthorizedTokenException;
use Hyperzod\HyperzodServiceFunctions\Helpers\AuthHelper;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Illuminate\Http\Request;

class AuthMiddleware
{
    use ApiResponseTrait;

    public function handle(Request $request, Closure $next)
    {
        if (HyperzodServiceFunctions::getUser(false) === false) {
            return $this->errorResponse(
                new UnauthorizedTokenException('Auth token missing or invalid.'),
                null,
                401,
                true
            );
        }
        return $next($request);
    }
}
