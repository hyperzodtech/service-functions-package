<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = [
            'Access-Control-Allow-Origin'      => '*',
            'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => 'Content-Type, Authorization, sentry-trace, ' . HttpHeaderKeyEnum::CLIENT_MEDIUM . ', ' . HttpHeaderKeyEnum::TENANT . ', ' . HttpHeaderKeyEnum::APM_TRANSACTION_ID . ', ' . HttpHeaderKeyEnum::PAGE_BUILDER_MODE_ENABLED . ', ' . HttpHeaderKeyEnum::X_API_KEY,
        ];

        if ($request->isMethod('OPTIONS')) {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->header($key, $value);
        }

        return $response;
    }
}
