<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Dto\Tenant\TenantDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ResolveTenantHeaderDtoMiddleware
{
    public function handle($request, Closure $next)
    {
        $header = $request->header(HttpHeaderKeyEnum::TENANT);
        if ($header) {
            $header = json_decode($header, true);
            HyperzodServiceFunctions::setTenant(new TenantDTO($header));
        }

        return $next($request);
    }
}
