<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Exceptions\Auth\UnauthorizedTokenException;
use Hyperzod\HyperzodServiceFunctions\Helpers\AuthHelper;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Illuminate\Http\Request;

class AuthenticateUserMiddleware
{
    use ApiResponseTrait;

    public function handle(Request $request, Closure $next)
    {
        $bearer_token = $request->bearerToken();
        if (is_string($bearer_token)) {
            AuthHelper::validateBearerToken($bearer_token);
        }

        return $next($request);
    }
}
