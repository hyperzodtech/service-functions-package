<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Dto\Tenant\TenantDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\EnvEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;
use Illuminate\Http\Request;

class ResolveTenantMiddleware
{
    use ApiResponseTrait, ServiceConsumerTrait;

    private const CACHE_PREFIX = "validated_tenant_";

    private string $header_x_tenant_value;

    public function handle(Request $request, Closure $next, $strict_mode = 1)
    {
        $strict_mode = boolval($strict_mode);

        $header_x_tenant_value = $request->header(HttpHeaderKeyEnum::TENANT);

        # Block request if tenant header is not set and strict_mode is true
        if (!$header_x_tenant_value && $strict_mode == true) {
            return $this->errorResponse(HttpHeaderKeyEnum::TENANT . ' header is missing');
        }
        # Continue request if strict_mode is false and tenant header is missing
        if (!$header_x_tenant_value && $strict_mode == false) {
            return $next($request);
        }

        $this->header_x_tenant_value = $header_x_tenant_value;

        # Check cache
        $validated_tenant_data = $this->getValidatedTenantFromCache();

        # if not cached then call services
        if (!$validated_tenant_data) {
            # prepare new request object 
            $svc_request = new Request();
            $svc_request->query->add([HttpHeaderKeyEnum::TENANT => $this->header_x_tenant_value]);
            # consume tenant service
            $resp = $this->consumeService(ServiceEnum::TENANT, $svc_request, 'GET', '/tenant/validate');

            if (!isset($resp['success']) || !$resp['success']) {
                return $this->errorResponse("Unable to resolve tenant", $resp, 404, true);
            }
            if ($resp['status_code'] == 301) {
                return $this->successResponse($resp['message'], $resp['data'], $resp['status_code'], true);
            }
            $validated_tenant_data = $resp['data'];
            $this->cacheValidatedTenant($validated_tenant_data);
        }

        $tenantDto = new TenantDTO($validated_tenant_data);
        HyperzodServiceFunctions::setTenant($tenantDto);
        HyperzodServiceFunctions::setTenantId($tenantDto->id);

        $request->query->add([TerminologyEnum::TENANT_ID => $tenantDto->id]);

        return $next($request);
    }

    private function getCacheKey()
    {
        return self::CACHE_PREFIX . $this->header_x_tenant_value;
    }

    private function cacheValidatedTenant(array $tenantResponseData): void
    {
        // set ttl as per environment
        $ttl = app()->environment() == EnvEnum::PRODUCTION ? now()->addMinutes(2) : now()->addSeconds(15);
        // cache validated tenant
        cache([self::getCacheKey() => $tenantResponseData], $ttl);
    }

    private function getValidatedTenantFromCache()
    {
        return cache(self::getCacheKey($this->header_x_tenant_value));
    }
}
