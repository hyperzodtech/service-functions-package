<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Validator;

class HasTenantId
{
    use ApiResponseTrait;

    public function handle($request, Closure $next)
    {
        if ($request->route('tenant_id')) {
            $request->query->add([TerminologyEnum::TENANT_ID => (int) $request->route('tenant_id')]);
        }

        $validator = Validator::make($request->all(), [TerminologyEnum::TENANT_ID => 'required']);
        if ($validator->fails()) {
            return $this->errorResponse(
                'Tenant id is required to access '.config('app.name').' service',
                null,
                400,
                true
            );
        }

        HyperzodServiceFunctionsFacade::setTenantId($request->input(TerminologyEnum::TENANT_ID));

        return $next($request);
    }
}
