<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Hyperzod\HyperzodServiceFunctions\Rules\LocaleExists;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

class HasLocale
{
    use ApiResponseTrait;

    public function handle($request, Closure $next)
    {
        $validator = Validator::make($request->all(), ['locale' => ['nullable', new LocaleExists]]);

        if ($validator->fails()) {
            return $next($request);
        }

        if ($validator->passes() && $request->input('locale')) {
            if (!HyperzodServiceFunctionsFacade::hasGlobal('locale')) {
                HyperzodServiceFunctionsFacade::setGlobal('locale', $request->input('locale'));
            }
            App::setLocale($request->input('locale'));
        }

        return $next($request);
    }
}
