<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Hyperzod\HyperzodServiceFunctions\Rules\LocaleExists;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

class HasLocaleStrict
{
    use ApiResponseTrait;

    public function handle($request, Closure $next)
    {
        if ($request->input('locale') == null) {
            throw new ServiceClientException('Locale is required');
        }

        $validator = Validator::make($request->all(), ['locale' => ['required', new LocaleExists]]);

        if ($validator->fails()) {
            throw new ServiceClientException('Invalid locale: ' . $request->input('locale'));
        }

        if ($validator->passes() && $request->input('locale')) {
            
            if (!HyperzodServiceFunctionsFacade::hasGlobal('locale')) {
                HyperzodServiceFunctionsFacade::setGlobal('locale', $request->input('locale'));
            }

        }

        return $next($request);
    }
}
