<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Dto\Log\TenantHttpRequestLogDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Tenant\TenantDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Log\LogApi;

class LogTenantHttpRequestMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!HyperzodServiceFunctions::getTenant() instanceof TenantDTO) {
            return $next($request);
        }

        $whitelistedHeaders = [HttpHeaderKeyEnum::CLIENT_DEVICE, 'user-agent'];
        // filter out whitelisted headers
        $headers = collect($request->headers->all())->filter(function ($value, $key) use ($whitelistedHeaders) {
            return in_array($key, $whitelistedHeaders);
        })->toArray();

        $httpLog = new TenantHttpRequestLogDTO([
            TerminologyEnum::TENANT_ID => HyperzodServiceFunctions::getTenantId(),
            'tenant_name' => HyperzodServiceFunctions::getTenant()->name,
            'host' => $request->getHost(),
            'path' => $request->getPathInfo(),
            'headers' => $headers,
            'method' => $request->getMethod(),
        ]);


        LogApi::submitTenantHttpLog($httpLog);

        return $next($request);
    }
}
