<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use AG\ElasticApmLaravel\Facades\ApmAgent;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ApmElkMiddleware
{
    public function handle($request, Closure $next)
    {
        if (app()->runningInConsole()) {
            return $next($request);
        }

        $response = $next($request);
        $response_elements = $this->extractResponseElements($response->content());

        try {
            $tags = [];
            if ($request->input(TerminologyEnum::TENANT_ID)) {
                $tags[TerminologyEnum::TENANT_ID] = $request->input(TerminologyEnum::TENANT_ID);
            }
            if (isset($response_elements['status_code'])) {
                $tags['status_code'] = $response_elements['status_code'];
            }

            ApmAgent::getCurrentTransaction()->setTags($tags);

            ApmAgent::getCurrentTransaction()->setCustomContext([
                'request.body' => json_encode($request->all()),
                'response.body' => $response->content(),
            ]);

            if (sizeof(HyperzodServiceFunctions::getUserData()) > 0) {
                ApmAgent::getCurrentTransaction()->setUserContext([
                    'id' => HyperzodServiceFunctions::getUserData()['id'] ?? null,
                    'name' => HyperzodServiceFunctions::getUserData()['first_name'] . " " . HyperzodServiceFunctions::getUserData()['last_name'] ?? null,
                    'email' => HyperzodServiceFunctions::getUserData()['email'] ?? null,
                ]);
            }
        } catch (\Exception $e) {
            // The connection with Elastic might fail, better be safe than sorry
        }

        return $response;
    }

    private function extractResponseElements($responseContent): array|bool
    {
        $response = json_decode($responseContent, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }

        return [
            'status_code' => $response['status_code'] ?? false,
        ];
    }
}
