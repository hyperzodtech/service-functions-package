<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\GoogleDistanceTrait;

class SetGoogleDistanceMiddleware
{
    use GoogleDistanceTrait, ApiResponseTrait;

    public function handle($request, Closure $next)
    {
        $tenant_id = HyperzodServiceFunctions::getTenantId();
        if (!$tenant_id) {
            return $this->errorResponse("Tenant ID not set.", null);
        }

        if ($request->has('pickup_location') && $request->has('delivery_location')) {
            if (is_string($request->pickup_location)) {
                $request->pickup_location = explode(',', $request->pickup_location);
            }
            if (is_string($request->delivery_location)) {
                $request->delivery_location = explode(',', $request->delivery_location);
            }
            $distance = $this->calculateGoogleDistance($request->pickup_location, $request->delivery_location);
            HyperzodServiceFunctions::setGoogleDistanceMeter($distance);
        }

        return $next($request);
    }
}
