<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Dto\Auth\UserDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ResolveAuthUserHeaderMiddleware
{
    public function handle($request, Closure $next)
    {
        # Check if client medium header is set 
        $auth_user_header = $request->header(HttpHeaderKeyEnum::AUTH_USER);
        if ($auth_user_header) {
            $auth_user = json_decode($auth_user_header, true);
            HyperzodServiceFunctions::setUser(new UserDTO($auth_user));
        }

        return $next($request);
    }
}
