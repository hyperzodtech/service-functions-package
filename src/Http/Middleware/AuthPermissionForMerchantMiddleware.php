<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Helpers\AuthPermissionHelper;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;

class AuthPermissionForMerchantMiddleware
{
    use ApiResponseTrait;

    public function handle($request, Closure $next, $permissions)
    {
        if (config('app.permissions') == false) {

            return $next($request);
        }

        $permissions = explode(AuthPermissionHelper::PERMISSION_DELIMETER, $permissions);
        $user = HyperzodServiceFunctions::getUser();
        $merchant_id = HyperzodServiceFunctions::getMerchantId();

        // Check for global permission
        if (AuthPermissionHelper::ifUserHasGlobalAccessPermission($user)) {
            return $next($request);
        }

        // Check for global permission
        if (AuthPermissionHelper::ifUserHasTenantGlobalAccessPermission($user)) {
            return $next($request);
        }

        // Check for tenant merchant access permission
        if (AuthPermissionHelper::ifUserHasTenantMerchantAccessPermission($user)) {
            return $next($request);
        }

        // check if any of the permission exist in the user permissions
        if (AuthPermissionHelper::ifUserHasPermission($user, $permissions)) {
            if (AuthPermissionHelper::ifUserBelongsToMerchantId($user, $merchant_id)) {
                return $next($request);
            }
        }

        return $this->errorResponse(
            'User does not have proper permissions to access this resource',
            [
                'user_permission' => $user->permissions,
                'required_permission' => $permissions,
                'merchant_id' => $merchant_id
            ],
            403,
            true
        );
    }
}
