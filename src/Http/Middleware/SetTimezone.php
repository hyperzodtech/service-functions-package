<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\SettingsServiceTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

class SetTimezone
{
    use SettingsServiceTrait, ApiResponseTrait;

    public function handle($request, Closure $next)
    {
        $tenant_id = HyperzodServiceFunctions::getTenantId();
        if (!$tenant_id) {
            return $this->errorResponse("Tenant ID not set.", null);
        }

        $timezone = Config::get('app.timezone', 'UTC');
        $tenant_timezone = $this->settingsByKeys('tenant', ['timezone'], null, null, $tenant_id, null);

        if ($tenant_timezone && isset($tenant_timezone["timezone"]) && !empty($tenant_timezone["timezone"])) {
            $timezone = $tenant_timezone["timezone"];
        }

        HyperzodServiceFunctionsFacade::setTenantTimezone($timezone);

        if ($timezone == "UTC") {
            // Log::alert("Service: " . config('app.name'));
            // Log::alert("Environment: " . config('app.env'));
            // Log::alert("Inside SetTimezone Middleware");
            // Log::alert(HyperzodServiceFunctionsFacade::getTenantId());
            // Log::alert("Timezone is UTC, no conversion needed");
            // is_array($tenant_timezone) ? Log::alert("Setting service response", $tenant_timezone) : Log::alert("Setting service response: " . strval($tenant_timezone));
        }
        return $next($request);
    }
}
