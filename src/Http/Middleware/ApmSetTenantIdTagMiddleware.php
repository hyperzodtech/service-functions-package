<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ApmSetTenantIdTagMiddleware
{
    public function handle($request, Closure $next)
    {
        \Sentry\configureScope(function (\Sentry\State\Scope $scope): void {
            $scope->setTag(TerminologyEnum::TENANT_ID, HyperzodServiceFunctions::getTenantId());
        });

        return $next($request);
    }
}
