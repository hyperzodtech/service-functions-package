<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Dto\Log\HttpLogDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Log\LogApi;

class LogHttpRequestMiddleware
{
    public function handle($request, Closure $next)
    {
        $whitelistedHeaders = [HttpHeaderKeyEnum::TENANT, HttpHeaderKeyEnum::CLIENT_DEVICE];
        // filter out whitelisted headers
        $headers = collect($request->headers->all())->filter(function ($value, $key) use ($whitelistedHeaders) {
            return in_array($key, $whitelistedHeaders);
        })->toArray();

        $httpLog = new HttpLogDTO([
            TerminologyEnum::TENANT_ID => (int) $request->input(TerminologyEnum::TENANT_ID),
            'host' => $request->getHost(),
            'path' => $request->getPathInfo(),
            'headers' => $headers,
            'method' => $request->getMethod(),
        ]);


        LogApi::submitHttpLog($httpLog);

        return $next($request);
    }
}
