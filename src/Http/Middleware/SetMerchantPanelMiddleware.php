<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\Auth\AuthEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderValueEnum;

class SetMerchantPanelMiddleware
{
    public function handle($request, Closure $next)
    {
        $client_medium = HttpHeaderValueEnum::CLIENT_MEDIUM_MERCHANT_ADMIN_PANEL;
        $api_key = $request->input(AuthEnum::APIKEY);
        if (!empty($api_key)) {
            $client_medium = HttpHeaderValueEnum::CLIENT_MEDIUM_API_KEY;
        }
        $request->headers->set(HttpHeaderKeyEnum::CLIENT_MEDIUM, $client_medium);
        return $next($request);
    }
}
