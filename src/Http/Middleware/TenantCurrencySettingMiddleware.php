<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;
use Hyperzod\HyperzodServiceFunctions\Traits\SettingsServiceTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

class TenantCurrencySettingMiddleware
{
    use SettingsServiceTrait;

    public function handle($request, Closure $next)
    {
        $tenant_id = HyperzodServiceFunctionsFacade::getTenantId();
        $cache_key = "currency_setting_$tenant_id";
        if (Cache::has($cache_key)) {
            HyperzodServiceFunctionsFacade::setTenantCurrencySetting(Cache::get($cache_key));
            return $next($request);
        }

        $currency_setting = $this->settingsByKeys('tenant', ['currency'], null, null, $tenant_id, null);

        if ($currency_setting && isset($currency_setting['currency'])) {
            HyperzodServiceFunctionsFacade::setTenantCurrencySetting($currency_setting['currency']);
            Cache::put($cache_key, $currency_setting['currency'], 20);
            return $next($request);
        }
        HyperzodServiceFunctionsFacade::setTenantCurrencySetting(null);
        return $next($request);
    }
}
