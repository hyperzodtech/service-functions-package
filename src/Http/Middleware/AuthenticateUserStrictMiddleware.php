<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\Auth\AuthEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\Auth\UnauthorizedTokenException;
use Hyperzod\HyperzodServiceFunctions\Helpers\AuthHelper;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Illuminate\Http\Request;

class AuthenticateUserStrictMiddleware
{
    use ApiResponseTrait;

    public function handle(Request $request, Closure $next)
    {
        // Validate X-API-KEY HEADER
        $x_api_key = $request->header(HttpHeaderKeyEnum::X_API_KEY);
        if (!empty($x_api_key)) {
            AuthHelper::validateBearerToken(false, $x_api_key);
            return $next($request);
        }

        // Validate API Key
        $api_key = $request->input(AuthEnum::APIKEY);
        if (!empty($api_key)) {
            AuthHelper::validateBearerToken(false, $api_key);
            return $next($request);
        }

        // Validate Bearer Token
        $bearer_token = $request->bearerToken();
        if (!is_string($bearer_token) || empty($bearer_token)) {
            throw new UnauthorizedTokenException();
        }

        AuthHelper::validateBearerToken($bearer_token);

        return $next($request);
    }
}
