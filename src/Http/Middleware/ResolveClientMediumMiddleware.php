<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ResolveClientMediumMiddleware
{
    public function handle($request, Closure $next)
    {
        $client_medium = $request->header(HttpHeaderKeyEnum::CLIENT_MEDIUM);

        // Set client medium header if valid
        if ($client_medium && HyperzodServiceFunctions::validateClientMedium($client_medium)) {
            HyperzodServiceFunctions::setClientMedium($client_medium);
        }

        return $next($request);
    }
}
