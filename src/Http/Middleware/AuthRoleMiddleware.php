<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;

class AuthRoleMiddleware
{
    use ApiResponseTrait;

    public function handle($request, Closure $next, $roles)
    {
        $roles = explode('|', $roles);
        $user = HyperzodServiceFunctions::getUser();

        // check if any of the roles exist in the user roles
        if (sizeof(array_intersect($roles, $user->roles)) > 0) {
            return $next($request);
        }

        return $this->errorResponse('User does not have proper role to access this resource', ['user_roles' => $user->roles, 'required_role' => $roles], 403);
    }
}
