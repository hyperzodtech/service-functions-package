<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ResolveClientMediumStrictMiddleware
{
    public function handle($request, Closure $next)
    {
        // Check if client medium header is set 
        $client_medium = $request->header(HttpHeaderKeyEnum::CLIENT_MEDIUM);
        if (!$client_medium) {
            throw new ServiceClientException('Required header missing: ' . HttpHeaderKeyEnum::CLIENT_MEDIUM);
        }

        // Validate client medium header
        if (!HyperzodServiceFunctions::validateClientMedium($client_medium)) {
            throw new ServiceClientException('Invalid value for ' .
                HttpHeaderKeyEnum::CLIENT_MEDIUM . ' header: ' . $client_medium);
        }

        HyperzodServiceFunctions::setClientMedium($client_medium);

        return $next($request);
    }
}
