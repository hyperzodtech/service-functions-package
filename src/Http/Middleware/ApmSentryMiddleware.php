<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class ApmSentryMiddleware
{
    public function handle($request, Closure $next)
    {
        $tags = [];

        if ($request->input(TerminologyEnum::TENANT_ID)) {
            $tags[TerminologyEnum::TENANT_ID] = $request->input(TerminologyEnum::TENANT_ID);
        }

        $tags['transaction_id'] = $request->header(HttpHeaderKeyEnum::APM_TRANSACTION_ID);
        if (empty($tags['transaction_id'])) {
            $tags['transaction_id'] = HyperzodServiceFunctions::getApmTransactionId();
        }

        \Sentry\configureScope(function (\Sentry\State\Scope $scope) use ($tags): void {
            foreach ($tags as $key => $value) {
                $scope->setTag($key, $value);
            }
        });

        return $next($request);
    }
}
