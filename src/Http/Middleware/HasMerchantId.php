<?php

namespace Hyperzod\HyperzodServiceFunctions\Http\Middleware;

use Closure;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Validator;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;

class HasMerchantId
{
    use ApiResponseTrait;

    public function handle($request, Closure $next)
    {
        $merchant_id = $request->route(TerminologyEnum::MERCHANT_ID);

        if (empty($merchant_id)) {
            $validator = Validator::make($request->all(), [TerminologyEnum::MERCHANT_ID => 'required']);
            if ($validator->fails()) {
                return $this->errorResponse("Merchant id is required to access this endpoint on " . config('app.name') . " service", null, 422, true);
            }
        }

        HyperzodServiceFunctionsFacade::setMerchantId($request->input(TerminologyEnum::MERCHANT_ID));

        return $next($request);
    }
}
