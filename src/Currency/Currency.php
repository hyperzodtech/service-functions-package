<?php

namespace Hyperzod\HyperzodServiceFunctions\Currency;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class Currency
{
    static public function convert(float $amount, string $from, string $to): float
    {
        $from = strtoupper($from);
        $to = strtoupper($to);
        $amount = floatval($amount);
        # Return amount if same currency
        if ($from === $to) {
            return round($amount, 2);
        }
        # Do exchange rate conversion
        $exchange_rate = self::getExchangeRate($from, $to);
        $converted_amount = $amount * $exchange_rate;

        return round($converted_amount, 2);
    }

    static public function fetchExchangeRateFromApi(string $from, string $to): float
    {
        $from = strtoupper($from);
        $to = strtoupper($to);
        $url = "http://api.exchangerate.host/live?source={$from}&currencies={$to}&access_key=6cd72502d44a1032ae8a59327831ba59";

        $response = Http::retry(3, 1000)->timeout(5)->get($url)->throw()->json();

        if (!isset($response['success']) || !$response['success']) {
            throw new \Exception("Error fetching exchange rate from api. " . json_encode($response), 1);
        }

        $exchange_rate = $response['quotes'][$from . $to];

        return $exchange_rate;
    }

    static public function getExchangeRate(string $from, string $to): float
    {
        $rates = false;
        $cache_key = "exchange_rate_" . strtolower($from) . "_" . strtolower($to);

        return Cache::remember($cache_key, now()->addDay(), function () use ($from, $to) {
            return (float) self::fetchExchangeRateFromApi($from, $to);
        });
    }
}
