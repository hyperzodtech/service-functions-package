<?php

namespace Hyperzod\HyperzodServiceFunctions\Log;

use Hyperzod\HyperzodServiceFunctions\Log\LogApi;
use Hyperzod\HyperzodServiceFunctions\Enums\EnvEnum;
use Hyperzod\HyperzodServiceFunctions\Dto\Log\SvcLogDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\LogLevelEnum;
use Illuminate\Support\Facades\Log;

class LogHelper
{
    public static function svclog(string $message, array $meta = [], string $log_level = LogLevelEnum::DEBUG)
    {
        if (app()->environment() == EnvEnum::LOCAL) {
            return \Log::debug($message, $meta);
        }

        $dto = new SvcLogDTO([
            'service' => config('app.name'),
            'env' => app()->environment(),
            'timestamp' => now()->toIso8601ZuluString(),
            'log_level' => $log_level,
            'logger' => 'clog',
            'message' => $message,
            'meta' => $meta,
        ]);
        LogApi::submitSvcLog($dto);
    }

    public static function debugToSlack(string $message, array $context = [], bool $production_only = true)
    {
        if ($production_only && app()->environment() != EnvEnum::PRODUCTION) {
            return;
        }
        $context['env'] = app()->environment();
        $context['service'] = config('app.name');
        Log::channel('slack')->emergency($message, $context);
    }
}
