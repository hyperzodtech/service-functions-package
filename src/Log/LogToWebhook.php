<?php

namespace Hyperzod\HyperzodServiceFunctions\Log;

use Illuminate\Support\Facades\Http;

class LogToWebhook
{
    public static $webhookUrl = null;
    public static $requestIsSync = true;

    public static function setRequestAsSync(bool $bool)
    {
        self::$requestIsSync = $bool;
    }

    public static function setWebhookUrl($url)
    {
        static::$webhookUrl = $url;
    }

    public static function log(...$data)
    {

        if (self::$requestIsSync) {
            try {
                // print "Sending webhook to: " . static::getWebhookUrl() . PHP_EOL;
                Http::post(static::getWebhookUrl(), $data);
                return;
                
            } catch (\Throwable $th) {
                return self::tryToSendErrorMessage($th->getMessage());
            }
        }

        dispatch(function () use ($data)
        {   
            try {
                // echo "Sending webhook to: " . static::getWebhookUrl() . PHP_EOL;
                Http::post(static::getWebhookUrl(), $data);
            } catch (\Throwable $th) {
                return self::tryToSendErrorMessage($th->getMessage());
            }
        });
    }

    public static function getWebhookUrl()
    {
        if (static::$webhookUrl) {
            return static::$webhookUrl;
        }
        
        return env('WEBHOOK_URL');
    }

    protected static function tryToSendErrorMessage($message)
    {
        try {
            Http::post(static::getWebhookUrl(), $message);
        } catch (\Throwable $th) {
            return false;
        }
    }
}
