<?php

namespace Hyperzod\HyperzodServiceFunctions\Log;

use Hyperzod\HyperzodServiceFunctions\Dto\Log\HttpLogDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Log\SvcLogDTO;
use Hyperzod\HyperzodServiceFunctions\Dto\Log\TenantHttpRequestLogDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\EnvEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Illuminate\Support\Facades\Http;

class LogApi
{
    public static function getHost()
    {
        $appDomain = HyperzodServiceFunctions::getServiceDomain();
        $subdomain = match (app()->environment()) {
            EnvEnum::PRODUCTION => 'log',
            EnvEnum::DEV => 'sys-log',
            EnvEnum::LOCAL => 'sys-log',
        };
        return "http://$subdomain.$appDomain";
    }

    public static function submitHttpLog(HttpLogDTO $httpLog)
    {
        self::sendHttpRequest('/log/http', $httpLog);
    }

    public static function submitTenantHttpLog(TenantHttpRequestLogDTO $httpLog)
    {
        self::sendHttpRequest('/log/http/tenant', $httpLog);
    }

    public static function submitSvcLog(SvcLogDTO $svcLog)
    {
        self::sendHttpRequest('/log/svc', $svcLog);
    }

    public static function submitTenantAdminUserLog(array $data)
    {
        try {
            $url = self::getHost() . '/log/user/tenant-admin';
            Http::timeout(1)->post($url, $data);
        } catch (\Exception $e) {
        }
    }

    private static function sendHttpRequest($url, $httpLog)
    {
        $url = self::getHost() . $url;
        $post_data = json_encode($httpLog->toArray());
        @shell_exec("curl --max-time 3 -d {$post_data} -X POST -H 'Content-Type: application/json' - '{$url}' > /dev/null 2>&1 &");
    }
}
