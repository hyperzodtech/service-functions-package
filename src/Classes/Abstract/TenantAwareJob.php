<?php

namespace Hyperzod\HyperzodServiceFunctions\Classes\Abstract;

use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

abstract class TenantAwareJob
{
	var int $tenant_id;

	abstract public function setTenantId(): int;

	public static function getTenantId(): int
	{
		return HyperzodServiceFunctions::getTenantId();
	}

	public function handle()
	{
		$this->tenant_id = $this->setTenantId();
		HyperzodServiceFunctions::setTenantId($this->tenant_id);
	}
}
