<?php

namespace Hyperzod\HyperzodServiceFunctions;

use Hyperzod\HyperzodServiceFunctions\Http\Middleware\HasLocale;
use Hyperzod\HyperzodServiceFunctions\Http\Middleware\HasTenantId;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Hyperzod\HyperzodServiceFunctions\Console\Commands\TenantCacheFlushCommand;
use Hyperzod\HyperzodServiceFunctions\Http\Middleware\ApmSentryMiddleware;
use Hyperzod\HyperzodServiceFunctions\Http\Middleware\AuthMiddleware;
use Hyperzod\HyperzodServiceFunctions\Http\Middleware\ResolveTenantMiddleware;
use Hyperzod\HyperzodServiceFunctions\Mq\Commands\ConsumeMQ;
use Hyperzod\HyperzodServiceFunctions\Mq\Commands\TerminateMQ;

class HyperzodServiceFunctionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('requiresAuth', AuthMiddleware::class);
        $router->aliasMiddleware('has-tenant-id', HasTenantId::class);
        $router->aliasMiddleware('resolveTenant', ResolveTenantMiddleware::class);

        $kernel = $this->app->make(\Illuminate\Contracts\Http\Kernel::class);
        $kernel->pushMiddleware(ApmSentryMiddleware::class);
        $kernel->pushMiddleware(HasLocale::class);

        $this->loadRoutesFrom(__DIR__ . '/Routes/Common.php');
        $this->loadRoutesFrom(__DIR__ . '/Routes/url.php');
        $this->publishes([
            __DIR__ . '/../config/flush.php' => config_path('flush.php'),
        ]);

        // Settings Files
        $this->publishes([
            __DIR__ . '/../src/Settings/Examples/Models' => base_path('/app/Models/Settings/Tenant'),
            __DIR__ . '/../src/Settings/Examples/Configs' => config_path(),
            __DIR__ . '/../src/Settings/Examples/JsonBlueprints' => base_path('/database/json/tenant/blueprints'),
            __DIR__ . '/../src/Settings/Examples/Seeders' => base_path('/database/seeders'),
            __DIR__ . '/../src/Settings/Examples/Updaters/Validators' => base_path('/app/Settings/Updaters/Validators'),
            __DIR__ . '/../src/Settings/Examples/Updaters/Casters' => base_path('/app/Settings/Updaters/Casters'),
            __DIR__ . '/../src/Settings/Examples/Updaters/Encrypters' => base_path('/app/Settings/Updaters/Encrypters'),
            __DIR__ . '/../src/Settings/Examples/Accessors/Defaults' => base_path('/app/Settings/Accessors/Defaults'),
            __DIR__ . '/../src/Settings/Examples/Accessors/Accessors' => base_path('/app/Settings/Accessors/Accessors'),
            __DIR__ . '/../src/Settings/Examples/Accessors/Decrypters' => base_path('/app/Settings/Accessors/Decrypters'),
            __DIR__ . '/../src/Settings/Examples/Actions' => base_path('/app/Actions')
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                TenantCacheFlushCommand::class,
                ConsumeMQ::class,
                TerminateMQ::class,
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Register the main class to use with the facade
        $this->app->singleton('HyperzodServiceFunctions', function () {
            return new HyperzodServiceFunctions;
        });
    }
}
