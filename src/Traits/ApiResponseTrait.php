<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

trait ApiResponseTrait
{
    public function processServiceResponse($response, $message = null)
    {
        if (!$response['success']) {
            return $this->errorResponse($message ?? @$response['message'], $response['data']);
        }

        if ($response['success']) {
            return $this->successResponse($message ?? @$response['message'], $response['data']);
        }

        return $this->errorResponse("An unknown error occurred.", $response);
    }
    public function successResponse($message = null, $data = null, $status_code = 200, bool $force_http_status = false, array $headers = [])
    {
        $resp['success'] = true;

        if (!is_null($message)) {
            $resp['message'] = $message;
        }

        if (!is_null($data)) {
            $resp['data'] = $data;
        }

        $resp['service'] = strtolower(config('app.name'));
        $resp['status_code'] = $status_code;

        return response()->json($resp, ($force_http_status ? $status_code : 200), $headers);
    }

    public function errorResponse($message = null, $data = null, $status_code = 200, bool $force_http_status = false)
    {
        $resp['success'] = false;

        if (!is_null($message)) {
            $resp['message'] = $message;
        }

        if (!is_null($data)) {
            $resp['data'] = $data;
        }

        $resp['service'] = strtolower(config('app.name'));
        $resp['status_code'] = $status_code;

        return response()->json($resp, ($force_http_status ? $status_code : 200));
    }
}
