<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\RequestApiGroupEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\Helpers\HttpClientHelper;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;
use Hyperzod\HyperzodServiceFunctions\ServiceDiscovery\ServiceDiscovery;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

trait ServiceConsumerTrait
{
    use ApiResponseTrait;

    public $PRODUCE_HTTP_RESPONSE = false;

    public int $requestTimeout = 10;

    public int $requestRetries = 0;
    public int $requestRetriesSleepMilliseconds = 250;

    public function consumeService($service_tag, Request $request, $method, $endpoint, $files = false, $https_enabled = false, array $options = [
        'skip_referrer' => false,
        'request_timeout' => 10,
    ])
    {

        $svc_host = ServiceDiscovery::discoverService($service_tag, $https_enabled);

        $url = $svc_host . $endpoint;
        $params = $request->all();
        $headers = [];
        $headers['Content-Type'] = $request->header('Content-Type') ?? 'application/json';
        $headers['Accept'] = 'application/json';

        $headers[HttpHeaderKeyEnum::REQUEST_FROM] = (!empty($request->getHost()) ? $request->getHost() : config('app.url'));
        $headers[HttpHeaderKeyEnum::REQUEST_FROM] = str_replace("https://", "", $headers[HttpHeaderKeyEnum::REQUEST_FROM]);
        $headers[HttpHeaderKeyEnum::REQUEST_FROM] = str_replace("http://", "", $headers[HttpHeaderKeyEnum::REQUEST_FROM]);

        if (!$options['skip_referrer']) {
            $headers[HttpHeaderKeyEnum::REQUEST_REFERRER] = $request->header('origin');
        }

        if (isset($options['request_timeout'])) {
            $this->requestTimeout = $options['request_timeout'];
        }

        $headers[HttpHeaderKeyEnum::CLIENT_IP] = $request->ip() ?? "0.0.0.0";

        $headers['authorization'] = HyperzodServiceFunctions::getAuthBearerToken();
        if ($request->hasHeader('authorization')) {
            $headers['authorization'] = $request->header('authorization');
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::SETTING_FALLBACK_ENABLED)) {
            $headers[HttpHeaderKeyEnum::SETTING_FALLBACK_ENABLED] = $request->header(HttpHeaderKeyEnum::SETTING_FALLBACK_ENABLED);
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::AUTH_IS_ADMIN_USER)) {
            $headers[HttpHeaderKeyEnum::AUTH_IS_ADMIN_USER] = intval($request->header(HttpHeaderKeyEnum::AUTH_IS_ADMIN_USER));
        } else {
            $headers[HttpHeaderKeyEnum::AUTH_IS_ADMIN_USER] = 1;
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::PAGE_BUILDER_MODE_ENABLED)) {
            $headers[HttpHeaderKeyEnum::PAGE_BUILDER_MODE_ENABLED] = intval($request->header(HttpHeaderKeyEnum::PAGE_BUILDER_MODE_ENABLED));
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::QUERY_SCOPE)) {
            $headers[HttpHeaderKeyEnum::QUERY_SCOPE] = $request->header(HttpHeaderKeyEnum::QUERY_SCOPE);
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::AUTH_USER_ID)) {
            $headers[HttpHeaderKeyEnum::AUTH_USER_ID] = $request->header(HttpHeaderKeyEnum::AUTH_USER_ID);
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::CLIENT_DEVICE)) {
            $headers[HttpHeaderKeyEnum::CLIENT_DEVICE] = $request->header(HttpHeaderKeyEnum::CLIENT_DEVICE);
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::CLIENT_MEDIUM)) {
            $headers[HttpHeaderKeyEnum::CLIENT_MEDIUM] = $request->header(HttpHeaderKeyEnum::CLIENT_MEDIUM);
        }

        if ($request->hasHeader(HttpHeaderKeyEnum::X_API_KEY)) {
            $headers[HttpHeaderKeyEnum::X_API_KEY] = $request->header(HttpHeaderKeyEnum::X_API_KEY);
        }

        # Get middleware group for the current request and set header if not set already
        $route_group = $request->route()?->getAction()['middleware'][0] ?? RequestApiGroupEnum::SERVICE->value;
        if ($route_group == 'api') {
            $route_group = RequestApiGroupEnum::SERVICE->value;
        }
        $headers[HttpHeaderKeyEnum::REQUEST_API_GROUP] = $route_group;

        $user = HyperzodServiceFunctionsFacade::getUser(false);
        if ($user) {
            $headers[HttpHeaderKeyEnum::AUTH_USER] = json_encode($user->toArray());
        }

        $tenant = HyperzodServiceFunctions::getTenant();
        if ($tenant) {
            $headers[HttpHeaderKeyEnum::TENANT] = json_encode($tenant->toArray());
        }

        $headers = $this->addApmTransactionHeader($request, $headers);

        $response = $this->makeHttpRequest($method, $url, $params, $request, $headers, $files);

        // For producing HTTP response with status code
        if ($this->PRODUCE_HTTP_RESPONSE) {
            if (json_last_error() == JSON_ERROR_NONE) {
                return response()->json($response->json(), $response->status());
            }
            return response(
                json_decode($response->body(), true),
                $response->status(),
                $response->headers()
            );
        }

        // Returns Array or String
        return (json_last_error() == JSON_ERROR_NONE) ? $response->json() : json_decode($response->body(), true);
    }

    private function makeHttpRequest($method, $url, $params, Request $request, $headers, $files)
    {
        $response = false;

        if ($files) {
            unset($headers['Content-Type']);
            if (!$request->file) {
                throw new ServiceClientException("File is required");
            }
        }

        $http_request = Http::withOptions([
            // 'debug' => true,
        ])->setClient(HttpClientHelper::getClient())
        ->timeout($this->requestTimeout)
        ->withHeaders($headers);

        // retry logic
        if ($this->requestRetries > 0) {
            $http_request->retry($this->requestRetries, $this->requestRetriesSleepMilliseconds);
        }

        // GET Request
        if ($method == 'GET') {
            $response = $http_request->get($url, $params);
        }
        // POST Request
        if ($method == 'POST') {
            $response = !$files ?
                $http_request->post($url, $params)
                :
                $http_request->attach('file', fopen($request->file, "r"))
                ->post($url, $params);
        }
        // PUT Request
        if ($method == 'PUT') {
            $response = !$files ?
                $http_request->put($url, $params)
                :
                $http_request->attach('file', fopen($request->file, "r"))->put($url, $params);
        }

        // DELETE Request
        if ($method == 'DELETE') {
            $response = $http_request->delete($url, $params);
        }

        return $response;
    }

    private function isServiceResponseValid($response)
    {
        $allowed_response_codes = [401, 422];
        $response_code = $response->status();
        if (($response->successful()) || (in_array($response_code, $allowed_response_codes))) {
            return true;
        }
        return false;
    }

    public function isAggregatedRequest()
    {
        $this->PRODUCE_HTTP_RESPONSE = false;
    }

    public function getServiceUrl($service_tag)
    {
        return ServiceDiscovery::discoverService($service_tag);
    }

    public function addApmTransactionHeader(Request $request, array $headers): array
    {
        try {
            $headers[HttpHeaderKeyEnum::APM_TRANSACTION_ID] = HyperzodServiceFunctions::getApmTransactionId();
        } finally {
            return $headers;
        }
    }
}
