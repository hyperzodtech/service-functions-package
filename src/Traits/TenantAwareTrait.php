<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

use Illuminate\Support\Facades\Config;
use Hyperzod\HyperzodServiceFunctions\Traits\SettingsServiceTrait;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

trait TenantAwareTrait
{
    use SettingsServiceTrait;

    public function setTimeZone($tenant_id)
    {
        $timezone = Config::get('app.timezone', 'UTC');
        $tenant_timezone = $this->settingsByKeys('tenant', ['timezone'], null, null, $tenant_id, null);

        if ($tenant_timezone && isset($tenant_timezone["timezone"]) && !empty($tenant_timezone["timezone"])) {
            $timezone = $tenant_timezone["timezone"];
        }
        HyperzodServiceFunctionsFacade::setTenantTimezone($timezone);
        return true;
    }
}
