<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;
use Illuminate\Support\Facades\Cache;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;

trait HelpersServiceTrait
{
    use ServiceConsumerTrait;

    public function fetchLocales()
    {
        $request = new \Illuminate\Http\Request();
        $response = $this->consumeService(ServiceEnum::HELPER, $request, 'GET', '/locales');
        if ($response['success'] == true) {
            return $response['data'];
        }

    }

    public function fetchOrderTypes()
    {
        $request = new \Illuminate\Http\Request();
        $response = $this->consumeService(ServiceEnum::HELPER, $request, 'GET', '/order-types');
        if ($response['success'] == true) {
            return $response['data'];
        }

    }

    public function fetchBusinessTypes()
    {
        $request = new \Illuminate\Http\Request();
        $response = $this->consumeService(ServiceEnum::HELPER, $request, 'GET', '/business-types');
        if ($response['success'] == true) {
            return $response['data'];
        }
    }

    public function fetchCurrencies()
    {
        $request = new \Illuminate\Http\Request();
        $response = $this->consumeService(ServiceEnum::HELPER, $request, 'GET', '/currencies');
        if ($response['success'] == true) {
            return $response['data'];
        }
    }

    public function fetchCountries()
    {
        $request = new \Illuminate\Http\Request();
        $response = $this->consumeService(ServiceEnum::HELPER, $request, 'GET', '/countries');
        if ($response['success'] == true) {
            return $response['data'];
        }
    }

    public function fetchTimezones()
    {
        $request = new \Illuminate\Http\Request();
        $response = $this->consumeService(ServiceEnum::HELPER, $request, 'GET', '/timezones');
        if ($response['success'] == true) {
            return $response['data'];
        }
    }
    
}
