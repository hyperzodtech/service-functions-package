<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Illuminate\Support\Facades\Http;

trait GoogleDistanceTrait
{
   use SettingsServiceTrait;

   public function calculateGoogleDistance(array $source, array $destination)
   {
      $gmap_api_key = $this->getApiKeyForTenant();

      $distance = null;

      $source = implode(',', $source);
      $destination = implode(',', $destination);

      $resp = Http::get(
         "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins={$source}&destinations={$destination}&departure_time=now&mode=DRIVING&key={$gmap_api_key}"
      );

      if ($resp['status'] == 'OK') {
         $distance = $resp['rows'][0]['elements'][0]['distance']['value'] ?? null;
      }

      return $distance;
   }

   private function getApiKeyForTenant(): string
   {
      $tenant_id = HyperzodServiceFunctions::getTenantId();
      $api_key = cache()->remember("gmap_api_key_tenant_" . $tenant_id, 120, function () use ($tenant_id) {
         return encrypt($this->getApiKeyForTenantAPI($tenant_id));
      });

      return decrypt($api_key);
   }

   private function getApiKeyForTenantAPI(int $tenant_id): string
   {
      $setting = $this->settingsByKeys('tenant', ['api_keys'], null, null, $tenant_id, null, true);
      if (!$setting) {
         throw new \Exception("Api Key setting not found for tenant");
      }
      if (!isset($setting['api_keys'])) {
         throw new \Exception("Api Keys not set for tenant");
      }
      if (!isset($setting['api_keys']['map_keys'])) {
         throw new \Exception("Map api keys not set for tenant");
      }
      if (!isset($setting['api_keys']['map_keys']['gmap']) || is_null($setting['api_keys']['map_keys']['gmap']) || empty($setting['api_keys']['map_keys']['gmap'])) {
         throw new \Exception("Gmap api key not set for tenant");
      }

      return $setting['api_keys']['map_keys']['gmap'];
   }
}
