<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

trait HasLanguageTranslationsTrait
{
    public function getLanguageTranslation($language_translations, $key, $locale = null)
    {
        $locale = $locale ?? (app()->getLocale() ?? 'en');

        $language_translations = collect($language_translations);
        $language_translation = $language_translations->where('key', $key)
            ->whereNotNull('value')
            ->where('locale', $locale)
            ->first();

        if ($language_translation && isset($language_translation['value']) && !empty($language_translation['value'])) {
            return $language_translation['value'];
        }

        $language_translation = $language_translations->where('key', $key)
            ->whereNotNull('value')
            ->first();

        if ($language_translation && isset($language_translation['value']) && !empty($language_translation['value'])) {
            return $language_translation['value'];
        }

        return null;
    }
}
