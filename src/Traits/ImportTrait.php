<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

use finfo;
use Hyperzod\HyperzodServiceFunctions\Enums\ImportEnum;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;

trait ImportTrait
{
   public function getFileNameFromUrl($url)
   {
      return array_slice(explode("/", parse_url($url, PHP_URL_PATH)), -1)[0];
   }

   public function storeImportFile($url, $file_name)
   {
      try {
         $file = file_get_contents($url);
      } catch (\Exception $e) {
         report($e);
         throw new ServiceClientException("File not found");
      }

      //Validate file extension
      $file_info = new finfo(FILEINFO_MIME_TYPE);
      $mime_type = $file_info->buffer($file);
      if ($mime_type !== 'text/csv' && $mime_type !== 'text/plain') {
         throw new ServiceClientException("Only CSV files are allowed.");
      }

      Storage::put($file_name, $file);
   }

   public function deleteImportFile($file_name)
   {
      Storage::delete($file_name);
   }

   public function getImportValidationStatus()
   {
      return Config::get(ImportEnum::VALIDATION_STATUS);
   }

   public function setImportValidationStatus(bool $status)
   {
      return Config::set(ImportEnum::VALIDATION_STATUS, $status);
   }

   public function getImportValidationErrors()
   {
      return Config::get(ImportEnum::VALIDATION_ERRORS) ?? [];
   }

   public function setImportValidationErrors($errors, int $row_number = null)
   {
      $this->setImportValidationStatus(false);
      $validation_errors = $this->getImportValidationErrors();

      if (is_null($row_number)) {
         $validation_errors["others"] = array_merge($validation_errors["others"] ?? [], $errors);
      } else {
         $row_name = "row" . $row_number;
         $validation_errors[$row_name] = array_merge($validation_errors[$row_name] ?? [], $errors);
      }

      Config::set(ImportEnum::VALIDATION_ERRORS, $validation_errors);
   }
}
