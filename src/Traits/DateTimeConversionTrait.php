<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

trait DateTimeConversionTrait
{
   public function setGlobalTimezone()
   {
      $timezone = HyperzodServiceFunctionsFacade::getTenantTimezone();
      date_default_timezone_set($timezone);
      Config::set('app.timezone', $timezone);
   }

   public function convertSchedulingSlotDateTime($scheduling_slot, $to_UTC = true)
   {
      if (isset($scheduling_slot["date"]) && isset($scheduling_slot["time"])) {
         $base_timezone = $to_UTC ? HyperzodServiceFunctionsFacade::getTenantTimezone() : 'UTC';
         $target_timezone = $to_UTC ? 'UTC' : HyperzodServiceFunctionsFacade::getTenantTimezone();

         $time = explode("-", $scheduling_slot["time"]);

         $date_time_string_from = $scheduling_slot["date"] . " " . trim($time[0]);
         $date_time_string_to = $scheduling_slot["date"] . " " . trim($time[1]);

         $carbon_date_time_from = Carbon::parse($date_time_string_from, $base_timezone);
         $carbon_date_time_to = Carbon::parse($date_time_string_to, $base_timezone);

         $carbon_date_time_from->timezone($target_timezone);
         $carbon_date_time_to->timezone($target_timezone);

         $scheduling_slot["date"] = $carbon_date_time_from->format('Y-m-d');
         $scheduling_slot["time"] = $carbon_date_time_from->format('H:i') . " - " . $carbon_date_time_to->format('H:i');
      }
      return $scheduling_slot;
   }

   public function getLocalTimeStamp($timestamp)
   {
      $timezone = HyperzodServiceFunctionsFacade::getTenantTimezone();
      if ($timezone == "UTC") {
         // Log::alert("Service: " . config('app.name'));
         // Log::alert("Inside getLocalTimeStamp()");
         // Log::alert(HyperzodServiceFunctionsFacade::getTenantId());
         // Log::alert("Timezone to convert" . $timestamp);
      }
      $date_time = Carbon::parse($timestamp);
      return $date_time->timezone($timezone)->toDateTimeString();
   }
}
