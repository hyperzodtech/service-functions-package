<?php

namespace Hyperzod\HyperzodServiceFunctions\Traits;

use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;

trait SettingsServiceTrait
{
    use ServiceConsumerTrait;

    public function settingsByKeys(
        string $type, # tenant or merchant
        array $setting_keys = [],
        string $setting_group = null, #
        string $locale = null,
        int $tenant_id = null,
        string $merchant_id = null,
        bool $fallback_enabled = false,
    ) {
        if (!is_null($tenant_id)) {
            $params[TerminologyEnum::TENANT_ID] = intval($tenant_id);
        }
        if (!is_null($merchant_id)) {
            $params[TerminologyEnum::MERCHANT_ID] = $merchant_id;
        }
        if (!is_null($locale)) {
            $params['locale'] = $locale;
        }

        $endpoint = '/';
        if ($type == 'tenant') {
            $endpoint = '/tenant';
        }
        if ($type == 'merchant') {
            $endpoint = '/merchant';
        }

        $params['keys'] = $setting_keys;

        $request = new \Illuminate\Http\Request();
        $request->query->add($params);

        if ($fallback_enabled) {
            $request->headers->set(HttpHeaderKeyEnum::SETTING_FALLBACK_ENABLED, 1);
        }

        $this->requestRetries = 3;
        $this->requestRetriesSleepMilliseconds = 1000;

        $response = $this->consumeService(ServiceEnum::SETTING, $request, 'GET', $endpoint . "/" . $setting_group);

        if (isset($response['success']) && $response['success'] == true) {
            return collect($response['data'])->pluck('setting_value', 'setting_key');
        }

        return false;
    }

    public function updateSetting(
        string $type, # tenant or merchant,
        array $setting_keys = [],
        int $tenant_id = null,
        string $merchant_id = null
    ) {
        if (!is_null($tenant_id)) {
            $params[TerminologyEnum::TENANT_ID] = intval($tenant_id);
        }
        if (!is_null($merchant_id)) {
            $params[TerminologyEnum::MERCHANT_ID] = $merchant_id;
        }

        $endpoint = '/';
        if ($type == 'tenant') {
            $endpoint = '/tenant';
        }
        if ($type == 'merchant') {
            $endpoint = '/merchant';
        }

        $params['settings'] = $setting_keys;

        $request = new \Illuminate\Http\Request();
        $request->query->add($params);

        $response = $this->consumeService(ServiceEnum::SETTING, $request, 'POST', $endpoint);

        if (isset($response['success']) && $response['success'] == true) {
            return collect($response['data'])->pluck('setting_value', 'setting_key');
        }
        return $this->errorResponse("Failed to update settings.", $response);
    }
}
