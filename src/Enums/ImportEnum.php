<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class ImportEnum extends BaseEnum
{
   const VALIDATION_STATUS = 'import_validation_status';
   const VALIDATION_ERRORS = 'import_validation_errors';
}
