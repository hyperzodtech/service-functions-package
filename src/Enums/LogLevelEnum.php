<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class LogLevelEnum
{
   const DEBUG = 'debug';
   const ERROR = 'error';
}
