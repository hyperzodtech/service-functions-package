<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

class SettingMqEnum
{
   const EXCHANGE = 'setting_exchange';

   const SETTING_UPDATED = 'setting.updated';

   const SETTING_UPDATED_TENANT = self::SETTING_UPDATED . ".tenant";

   const SETTING_UPDATED_MERCHANT = self::SETTING_UPDATED . ".merchant";

   const SETTING_UPDATED_MERCHANT_ORDERCONFIG = self::SETTING_UPDATED_MERCHANT . ".orderconfig";

   const SETTING_UPDATED_MERCHANT_DELIVERYRATES = self::SETTING_UPDATED_MERCHANT . ".deliveryrates";

   const SETTING_UPDATED_MERCHANT_PICKUPORDERTYPETEXT = self::SETTING_UPDATED_MERCHANT . ".pickupordertypetext";

   const SETTING_UPDATED_MERCHANT_CUSTOMORDERTYPETEXT = self::SETTING_UPDATED_MERCHANT . ".customordertypetext";

   const SETTING_UPDATED_OPENTIMINGS = self::SETTING_UPDATED . ".opentimings";

   const SETTING_UPDATED_OPENTIMINGS_TENANT = self::SETTING_UPDATED_OPENTIMINGS . ".tenant";

   const SETTING_UPDATED_SITE_SETTINGS = self::SETTING_UPDATED_TENANT . ".sitesettings";

   const SETTING_UPDATED_OPENTIMINGS_MERCHANT = self::SETTING_UPDATED_OPENTIMINGS . ".merchant";
}
