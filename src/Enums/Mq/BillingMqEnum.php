<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

class BillingMqEnum
{
   const EXCHANGE = 'billing_exchange';
   const BILLING_TRANSACTION = 'billing.transaction';
   const BILLING_TRANSACTION_CREATED = self::BILLING_TRANSACTION . ".created";
   const BILLING_TRANSACTION_CREATED_ORDER = self::BILLING_TRANSACTION_CREATED . ".order";
   const BILLING_TENANT_SUBSCRIBED = 'billing.tenant.subscribed';
   const BILLING_TENANT_UNSUBSCRIBED = 'billing.tenant.unsubscribed';
   const BILLING_EXPIRE_TENANT_SAAS_MODULES = 'billing.expire.tenant.saas.modules';
}
