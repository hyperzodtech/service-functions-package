<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

class DeploymentMqEnum
{
   const EXCHANGE = 'deployment_exchange';
   const DEPLOYMENT_STAGE_COMPLETED_EVENT = 'deployment.stage.completed';
}
