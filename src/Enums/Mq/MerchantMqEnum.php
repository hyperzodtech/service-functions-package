<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

class MerchantMqEnum
{
   const EXCHANGE = 'merchant_exchange';

   const MERCHANT_CREATED = 'merchant.created';

   const MERCHANT_DELETED = 'merchant.deleted';

   const MERCHANT_UPDATED = 'merchant.updated';

   const MERCHANT_DELETED_FORCE = self::MERCHANT_DELETED . '.force';

   const MERCHANT_UPDATED_ISOPEN = self::MERCHANT_UPDATED . '.isopen';
}
