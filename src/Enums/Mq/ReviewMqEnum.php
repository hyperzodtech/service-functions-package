<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

class ReviewMqEnum
{
   const EXCHANGE = 'review_exchange';

   const REVIEW_UPDATED = 'review.updated';

   const REVIEW_UPDATED_ORDER = self::REVIEW_UPDATED . ".order";

   const REVIEW_UPDATED_MERCHANT = self::REVIEW_UPDATED . ".merchant";

   const REVIEW_UPDATED_MERCHANT_RATING = self::REVIEW_UPDATED_MERCHANT . ".rating";
}
