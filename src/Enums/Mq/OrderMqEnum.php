<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

use Hyperzod\HyperzodServiceFunctions\Enums\OrderStatusEnum;

class OrderMqEnum
{
   const EXCHANGE = 'order_exchange';

   const ORDER_CREATED = 'order.created';

   const ORDER_UPDATED = 'order.updated';

   const ORDER_COUPON = "order.coupon";

   const ORDER_COUPON_APPLIED = self::ORDER_COUPON . ".applied";

   const ORDER_UPDATED_STATUS = self::ORDER_UPDATED . ".status";

   const ORDER_UPDATED_STATUS_PENDING = self::ORDER_UPDATED_STATUS . "." . OrderStatusEnum::PENDING;

   const ORDER_UPDATED_STATUS_ACCEPTED = self::ORDER_UPDATED_STATUS . "." . OrderStatusEnum::ACCEPTED;

   const ORDER_UPDATED_STATUS_READY = self::ORDER_UPDATED_STATUS . "." . OrderStatusEnum::READY;

   const ORDER_UPDATED_STATUS_COLLECTED = self::ORDER_UPDATED_STATUS . "." . OrderStatusEnum::COLLECTED;

   const ORDER_UPDATED_STATUS_COMPLETED = self::ORDER_UPDATED_STATUS . "." . OrderStatusEnum::COMPLETED;

   const ORDER_UPDATED_STATUS_CANCELLED = self::ORDER_UPDATED_STATUS . "." . OrderStatusEnum::CANCELLED;

   const ORDER_ACTIVE = "order.active";

   const ORDER_UPDATED_DELIVERY_PROVIDER = self::ORDER_UPDATED . ".delivery.provider";
}
