<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

use Hyperzod\HyperzodServiceFunctions\Enums\Auth\UserRoleEnum;

class AuthMqEnum
{
    const EXCHANGE = 'auth_exchange';

    const AUTH_CREATED = 'auth.created'; // Auth User Created Event

    const AUTH_DELETED_USER = 'auth.deleted.user';

    const AUTH_UPDATED_USER = 'auth.updated.user';

    const AUTH_TENANT_ADMIN_OTP_GENERATED = 'auth.' . UserRoleEnum::TENANT_ADMIN . '.otp.generated';

    const TENANT_ADMIN_USER_CREATED = 'auth.' . UserRoleEnum::TENANT_ADMIN . '.created';
    
    const TENANT_USER_CREATED = 'auth.' . UserRoleEnum::TENANT_USER . '.created';

    const AUTH_CREATED_OTP = self::AUTH_CREATED . ".otp";

    const AUTH_CREATED_OTP_USER = self::AUTH_CREATED_OTP . ".user";

    const AUTH_CREATED_FORGOTPASSWORDTOKEN = self::AUTH_CREATED . ".forgotpasswordtoken";

    const AUTH_CREATED_INVITE = self::AUTH_CREATED . ".invite";

    const AUTH_TENANT_ADMIN_EMAIL_UPDATED = 'auth' . UserRoleEnum::TENANT_ADMIN . '.email.updated';

    const AUTH_INVITE_ACCEPTED = 'auth.invite.accepted';
}
