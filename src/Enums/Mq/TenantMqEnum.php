<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

class TenantMqEnum
{
   const EXCHANGE = 'tenant_exchange';

   const TENANT_CREATED = 'tenant.created';

   const TENANT_UPDATED = 'tenant.updated';

   const TENANT_SUSPENDED = 'tenant.suspended';

   const TENANT_DELETED = 'tenant.deleted';

   const TENANT_UPDATED_ISOPEN = self::TENANT_UPDATED . ".isopen";
}
