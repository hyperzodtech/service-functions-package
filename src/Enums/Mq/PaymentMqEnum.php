<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Mq;

class PaymentMqEnum
{
   const EXCHANGE = 'payment_exchange';

   const PAYMENT_PENDING = 'payment.pending';
   const PAYMENT_COMPLETED = 'payment.completed';
   const PAYMENT_FAILED = 'payment.failed';

   const PAYMENT_PENDING_ORDER = self::PAYMENT_PENDING . ".order";
   const PAYMENT_COMPLETED_ORDER = self::PAYMENT_COMPLETED . ".order";
   const PAYMENT_FAILED_ORDER = self::PAYMENT_FAILED . ".order";
}
