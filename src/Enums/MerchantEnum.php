<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class MerchantEnum extends BaseEnum
{
    const DELIVERY_BY_TENANT = 'tenant';
    const DELIVERY_BY_MERCHANT = 'merchant';
}
