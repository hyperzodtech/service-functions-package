<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class CartTypeEnum extends BaseEnum
{
    public const ECOMMERCE = 'ecommerce';
    public const PICK_DROP = 'pick_drop';
}
