<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class HttpHeaderValueEnum
{
    const QUERY_SCOPE_TENANT = 'tenant';
    const QUERY_SCOPE_MERCHANT = 'merchant';
    const QUERY_SCOPE_USER = 'user';

    const CLIENT_MEDIUM_SYSTEM_ADMIN_PANEL = 1;
    const CLIENT_MEDIUM_TENANT_ADMIN_PANEL = 2;
    const CLIENT_MEDIUM_MERCHANT_ADMIN_PANEL = 3;
    const CLIENT_MEDIUM_API_KEY = 4;
    const CLIENT_MEDIUM_MICROSERVICE = 5;
}
