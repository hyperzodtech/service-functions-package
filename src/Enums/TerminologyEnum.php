<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class TerminologyEnum
{
    const TENANT_ID = 'tenant_id';
    const MERCHANT_ID = 'merchant_id';
    const USER_ID = 'user_id';
    const ORDER_ID = 'order_id';
}
