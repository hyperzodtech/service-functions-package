<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class StatusEnum
{
    const INACTIVE = 0;
    const ACTIVE = 1;
}
