<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class ServiceEnum
{
    const ADDRESS = 'address';
    const APPBUILD = 'appbuild';
    const DEPLOYMENT = 'deployment';
    const WIZARD = 'wizard';
    const AUTH = 'auth';
    const CART = 'cart';
    const CATALOG = 'catalog';
    const FORM_BUILDER = 'formbuilder';
    const GROUP = 'group';
    const HELPER = 'helper';
    const MERCHANT = 'merchant';
    const NOTIFICATION = 'notification';
    const OPENSTATUS = 'openstatus';
    const ORDER = 'order';
    const PAGE = 'page';
    const PAGEBUILDER = 'pagebuilder';
    const PAYMENT = 'payment';
    const PLACESEARCH = 'placesearch';
    const PLUGIN = 'plugin';
    const PROMOTIONAL = 'promotional';
    const REVIEW = 'review';
    const SEARCH = 'search';
    const SERVICEABLEAREA = 'serviceablearea';
    const SETTING = 'setting';
    const TENANT = 'tenant';
    const UPLOAD = 'upload';
    const WALLET = 'wallet';
    const WEBHOOK = 'webhook';
    const BILLING = 'billing';
    const API = 'api';
    const SAGA = 'saga';
    const STATS = 'stats';
    const LOG = 'log';
    const URL = 'url';
}
