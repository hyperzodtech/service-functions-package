<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

enum RequestApiGroupEnum: string
{
   case AUTH = 'auth';
   case STORE = 'store';
   case ADMIN = 'admin';
   case MERCHANT = 'merchant';
   case SYSTEM = 'system';
   case PUBLIC = 'public';
   case SERVICE = 'service';
}
