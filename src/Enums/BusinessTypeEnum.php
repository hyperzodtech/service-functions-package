<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class BusinessTypeEnum extends BaseEnum
{
   const STATIC_MERCHANT = 'static_merchant';
   const MULTI_MERCHANT = 'multi_merchant';
   const NEAREST_MERCHANT = 'nearest_merchant';
}
