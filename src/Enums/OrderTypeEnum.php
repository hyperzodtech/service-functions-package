<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class OrderTypeEnum extends BaseEnum
{
    public const DELIVERY = 'delivery';
    public const PICKUP = 'pickup';
    public const PICK_DROP = 'pick_drop';
    public const CUSTOM_1 = 'custom_1';

    // list of all order types with their labels as array of arrays
    public static function list(): array
    {
        return [
            [
                'order_type' => self::DELIVERY,
                'label' => 'Delivery',
                'description' => 'Express',
                'requires_address' => true,
                'is_system' => true,
            ],
            [
                'order_type' => self::PICKUP,
                'label' => 'Pickup',
                'description' => 'Express pickup',
                'requires_address' => false,
                'is_system' => true,
            ],
            [
                'order_type' => self::PICK_DROP,
                'label' => 'Pick & Drop',
                'description' => 'Standard',
                'requires_address' => true,
                'is_system' => true,
            ],
            [
                'order_type' => self::CUSTOM_1,
                'label' => 'Custom',
                'description' => 'Standard',
                'requires_address' => false,
                'is_system' => false,
            ],
        ];
    }

    public static function fetchByOrderType(string $order_type): array
    {
        $order_types = collect(self::list());
        $order_type = $order_types->where('order_type', $order_type)->first();

        return $order_type;
    }
}
