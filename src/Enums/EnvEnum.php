<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class EnvEnum
{
    const LOCAL = 'local';
    const DEV = 'dev';
    const PRODUCTION = 'production';
}
