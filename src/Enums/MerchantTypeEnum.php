<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class MerchantTypeEnum extends BaseEnum
{
    public const ECOMMERCE = 'ecommerce';
    public const PICK_DROP = 'pick_drop';
}
