<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class BusinessCategoryEnum extends BaseEnum
{
   const FOOD_DELIVERY = 'food_delivery';
   const GROCERY_DELIVERY = 'grocery_delivery';
   const BAKERY_DELIVERY = 'bakery_delivery';
   const PET_FOOD_DELIVERY = 'pet_food_delivery';
   const BOUQUET_DELIVERY = 'bouquet_delivery';
   const STATIONARY_DELIVERY = 'stationary_delivery';
   const ACCESSORIES_DELIVERY = 'accessories_delivery';
   const CLOTHING_DELIVERY = 'clothing_delivery';
   const BEVERAGES_DELIVERY = 'beverages_delivery';
}
