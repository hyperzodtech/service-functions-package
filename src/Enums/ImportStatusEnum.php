<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class ImportStatusEnum
{
   const PENDING = "pending";
   const PROCESSING = "processing";
   const COMPLETED = "completed";
   const FAILED = "failed";
}
