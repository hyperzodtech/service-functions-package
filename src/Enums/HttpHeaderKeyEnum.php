<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class HttpHeaderKeyEnum
{
    const QUERY_SCOPE = 'x-query-scopes';  # @string
    const AUTH_USER_ID = 'x-auth-user-id'; # @int
    const AUTH_USER = 'x-auth-user'; # @array @string
    const AUTH_IS_SYSTEM_USER = 'x-auth-is-system-user'; # @bool
    const AUTH_IS_ADMIN_USER = 'x-auth-is-admin-user'; # @bool
    const AUTH_USER_ROLES = 'x-auth-user-roles'; # @array string
    const AUTH_USER_PERMISSIONS = 'x-auth-user-permissions'; # @array string
    const CLIENT_MEDIUM = 'x-client-medium'; # @string  !deprecated
    const TENANT = 'x-tenant'; # @string
    const CLIENT_DEVICE = 'x-client-device'; # @string
    const CLIENT_IP = 'x-client-ip'; # @string
    const APM_TRANSACTION_ID = 'x-apm-transaction-id'; # @string
    const SETTING_FALLBACK_ENABLED = 'x-fallback-enabled'; # @string
    const REQUEST_FROM = 'request-from'; # @string
    const REQUEST_REFERRER = 'request-referrer'; # @string
    const REQUEST_API_GROUP = 'x-request-api-group'; # @string @ApiRouteGroupEnum
    const PAGE_BUILDER_MODE_ENABLED = 'x-page-builder-mode-enabled'; # @bool
    const X_API_KEY = 'X-API-KEY'; # @string
}
