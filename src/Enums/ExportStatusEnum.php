<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class ExportStatusEnum extends BaseEnum
{
   const PROCESSING = "processing";
   const COMPLETED = "completed";
   const FAILED = "failed";
}
