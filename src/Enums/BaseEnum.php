<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class BaseEnum
{
    public function getConstants()
    {
        $reflectionClass = new \ReflectionClass($this);
        return $reflectionClass->getConstants();
    }

    public static function all()
    {
        $reflectionClass = new \ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }

    public static function getConstantValues(): array
    {
        return array_values(self::all());
    }

    public static function getKey($value): string
    {
        $constants = self::all();
        $key = array_search($value, $constants, true);
        if ($key === false) {
            throw new \InvalidArgumentException("Value '$value' is not part of the enum " . static::class);
        }
        return strtolower($key);
    }
}
