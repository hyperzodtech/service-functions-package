<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

class SaasModuleEnum extends BaseEnum
{
   const ADMIN = 'admin';
   const WEB_ORDERING = 'web_ordering';
   const APP_ORDERING = 'app_ordering';
   const APP_MERCHANT = 'app_merchant';
   const APP_DRIVER = 'app_driver';
}
