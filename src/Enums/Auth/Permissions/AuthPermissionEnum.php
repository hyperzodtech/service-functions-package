<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Auth\Permissions;

use Hyperzod\HyperzodServiceFunctions\Enums\BaseEnum;

class AuthPermissionEnum extends BaseEnum
{
   const MANAGE_SYSTEM_ADMIN_USER = 'ManageSystemAdminUser';
   const MANAGE_TENANT_ADMIN = 'ManageTenantAdmin';
   const MANAGE_TENANT_USER = 'ManageTenantUser';
   const MANAGE_MERCHANT_USER = 'ManageMerchantUser';
   const MANAGE_TENANT_AUTH_SETTING = 'ManageTenantAuthSetting';
   const MANAGE_USER = 'ManageUser';
   const LIST_USER = 'ListUser';
}
