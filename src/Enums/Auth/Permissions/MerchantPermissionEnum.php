<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Auth\Permissions;

use Hyperzod\HyperzodServiceFunctions\Enums\BaseEnum;

class MerchantPermissionEnum extends BaseEnum
{
   const ACCESS_MERCHANT = 'AccessMerchant';

   # Dashboard
   const VIEW_MERCHANT_DASHBOARD = 'ViewMerchantDashboard';
   const MANAGE_MERCHANT_DASHBOARD = 'ManageMerchantDashboard';

   # Orders
   const VIEW_MERCHANT_ORDERS = 'ViewMerchantOrders';
   const MANAGE_MERCHANT_ORDERS = 'ManageMerchantOrders';

   # Catalog
   const VIEW_MERCHANT_CATALOG = 'ViewMerchantCatalog';
   const MANAGE_MERCHANT_CATALOG = 'ManageMerchantCatalog';

   const VIEW_MERCHANT_CATALOG_CATEGORY = 'ViewMerchantCatalogCategory';
   const MANAGE_MERCHANT_CATALOG_CATEGORY = 'ManageMerchantCatalogCategory';

   const VIEW_MERCHANT_CATALOG_TAGS = 'ViewMerchantCatalogTags';
   const MANAGE_MERCHANT_CATALOG_TAGS = 'ManageMerchantCatalogTags';

   const VIEW_MERCHANT_CATALOG_LABELS = 'ViewMerchantCatalogLabels';
   const MANAGE_MERCHANT_CATALOG_LABELS = 'ManageMerchantCatalogLabels';

   # Coupons
   const VIEW_MERCHANT_COUPONS = 'ViewMerchantCoupons';
   const MANAGE_MERCHANT_COUPONS = 'ManageMerchantCoupons';

   # Reviews
   const VIEW_MERCHANT_REVIEW = 'ViewMerchantReview';
   const MANAGE_MERCHANT_REVIEW = 'ViewMerchantReview';

   # Reports
   const VIEW_MERCHANT_REPORTS = 'ViewMerchantReports';
   const MANAGE_MERCHANT_REPORTS = 'ManageMerchantReports';
   const MERCHANT_REPORTS_EXPORT = 'MerchantReportsExport';

   # General Settings
   const VIEW_MERCHANT_SETTINGS_GENERAL = 'ViewMerchantSettingsGeneral';
   const MANAGE_MERCHANT_SETTINGS_GENERAL = 'ManageMerchantSettingsGeneral';

   # User Settings
   const VIEW_MERCHANT_SETTINGS_USERS = 'ViewMerchantSettingsUsers';
   const MANAGE_MERCHANT_SETTINGS_USERS = 'ManageMerchantSettingsUsers';

   # Merchant Serviceable Zones
   const VIEW_MERCHANT_SETTINGS_SERVICEABLE_ZONES = 'ViewMerchantSettingsServiceableZones';
   const MANAGE_MERCHANT_SETTINGS_SERVICEABLE_ZONES = 'ManageMerchantSettingsServiceableZones';

   # Merchant Store Timing
   const VIEW_MERCHANT_SETTINGS_STORE_TIMING = 'ViewMerchantSettingsStoreTiming';
   const MANAGE_MERCHANT_SETTINGS_STORE_TIMING = 'ManageMerchantSettingsStoreTiming';

   # Upload
   const VIEW_MERCHANT_SETTINGS_UPLOAD = 'ViewMerchantSettingsUpload';
   const MANAGE_MERCHANT_SETTINGS_UPLOAD = 'ManageMerchantSettingsUpload';

   # Payouts
   const VIEW_MERCHANT_SETTINGS_PAYOUTS = 'ViewMerchantSettingsPayouts';
   const MANAGE_MERCHANT_SETTINGS_PAYOUTS = 'ManageMerchantSettingsPayouts';

   # Apps
   const VIEW_MERCHANT_APPS = 'ViewMerchantApps';
   const MANAGE_MERCHANT_APPS = 'ManageMerchantApps';
}
