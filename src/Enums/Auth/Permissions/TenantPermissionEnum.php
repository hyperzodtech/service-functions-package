<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Auth\Permissions;

use Hyperzod\HyperzodServiceFunctions\Enums\BaseEnum;

class TenantPermissionEnum extends BaseEnum
{
   const ACCESS_TENANT = 'AccessTenant';
   const GLOBAL_ACCESS_TENANT = 'GlobalAccessTenant';
   const ACCESS_TENANT_MERCHANTS = 'AccessTenantMerchants';

   # Dashboard
   const VIEW_TENANT_DASHBOARD = 'ViewTenantDashboard';
   const MANAGE_TENANT_DASHBOARD = 'ManageTenantDashboard';

   # Orders
   const VIEW_TENANT_ORDERS = 'ViewTenantOrders';
   const MANAGE_TENANT_ORDERS = 'ManageTenantOrders';

   # Merchants
   const VIEW_TENANT_MERCHANTS = 'ViewTenantMerchants';
   const MANAGE_TENANT_MERCHANTS = 'ManageTenantMerchants';

   # Category
   const VIEW_TENANT_MERCHANTS_CATEGORY = 'ViewTenantMerchantsCategory';
   const MANAGE_TENANT_MERCHANTS_CATEGORY = 'ManageTenantMerchantsCategory';

   # Product
   const VIEW_TENANT_MERCHANTS_PRODUCT = 'ViewTenantMerchantsProduct';
   const MANAGE_TENANT_MERCHANTS_PRODUCT = 'ManageTenantMerchantsProduct';

   # Customers
   const VIEW_TENANT_CUSTOMERS = 'ViewTenantCustomers';
   const MANAGE_TENANT_CUSTOMERS = 'ManageTenantCustomers';

   # Promotions
   const VIEW_TENANT_PROMOTIONS = 'ViewTenantPromotions';
   const MANAGE_TENANT_PROMOTIONS = 'ManageTenantPromotions';

   const VIEW_TENANT_PROMOTIONS_COUPONS = 'ViewTenantPromotionsCoupons';
   const MANAGE_TENANT_PROMOTIONS_COUPONS = 'ManageTenantPromotionsCoupons';

   const VIEW_TENANT_PROMOTIONS_POPUPS = 'ViewTenantPromotionsPopups';
   const MANAGE_TENANT_PROMOTIONS_POPUPS = 'ManageTenantPromotionsPopups';

   const VIEW_TENANT_PROMOTIONS_CAMPAIGNS = 'ViewTenantPromotionsCampaigns';
   const MANAGE_TENANT_PROMOTIONS_CAMPAIGNS = 'ManageTenantPromotionsCampaigns';

   # Reviews
   const VIEW_TENANT_REVIEWS = 'ViewTenantReviews';
   const MANAGE_TENANT_REVIEWS = 'ManageTenantReviews';

   # Payment Logs
   const VIEW_TENANT_PAYMENT_LOGS = 'ViewTenantPaymentLogs';
   const MANAGE_TENANT_PAYMENT_LOGS = 'ManageTenantPaymentLogs';

   # Reports
   const VIEW_TENANT_REPORTS = 'ViewTenantReports';
   const MANAGE_TENANT_REPORTS = 'ManageTenantReports';
   const TENANT_REPORTS_EXPORT = 'TenantReportsExport';

   # Analytics
   const VIEW_TENANT_ANALYTICS = 'ViewTenantAnalytics';
   const MANAGE_TENANT_ANALYTICS = 'ManageTenantAnalytics';

   # General
   const VIEW_TENANT_SETTINGS_GENERAL = 'ViewTenantSettingsGeneral';
   const MANAGE_TENANT_SETTINGS_GENERAL = 'ManageTenantSettingsGeneral';

   # Groups
   const VIEW_TENANT_SETTINGS_GROUPS = 'ViewTenantSettingsGroups';
   const MANAGE_TENANT_SETTINGS_GROUPS = 'ManageTenantSettingsGroups';

   # Users & Api Keys
   const VIEW_TENANT_SETTINGS_USERS_API_KEYS = 'ViewTenantSettingsUsersApiKeys';
   const MANAGE_TENANT_SETTINGS_USERS_API_KEYS = 'ManageTenantSettingsUsersApiKeys';

   # Serviceable Zones
   const VIEW_TENANT_SETTINGS_SERVICEABLE_ZONES = 'ViewTenantSettingsServiceableZones';
   const MANAGE_TENANT_SETTINGS_SERVICEABLE_ZONES = 'ManageTenantSettingsServiceableZones';

   # Payment
   const VIEW_TENANT_SETTINGS_PAYMENT = 'ViewTenantSettingsPayment';
   const MANAGE_TENANT_SETTINGS_PAYMENT = 'ManageTenantSettingsPayment';

   # Theme
   const MANAGE_TENANT_SETTINGS_THEME = 'ManageTenantSettingsTheme';

   # Language
   const VIEW_TENANT_SETTINGS_LANGUAGE = 'ViewTenantSettingsLanguage';
   const MANAGE_TENANT_SETTINGS_LANGUAGE = 'ManageTenantSettingsLanguage';

   # Order
   const VIEW_TENANT_SETTINGS_ORDER = 'ViewTenantSettingsOrder';
   const MANAGE_TENANT_SETTINGS_ORDER = 'ManageTenantSettingsOrder';

   # Store Timing
   const MANAGE_TENANT_SETTINGS_STORE_TIMING = 'ManageTenantSettingsStoreTiming';

   # Notification
   const VIEW_TENANT_SETTINGS_NOTIFICATION = 'ViewTenantSettingsNotification';
   const MANAGE_TENANT_SETTINGS_NOTIFICATION = 'ManageTenantSettingsNotification';

   # Api Keys
   const VIEW_TENANT_SETTINGS_API_KEYS = 'ViewTenantSettingsApiKeys';
   const MANAGE_TENANT_SETTINGS_API_KEYS = 'ManageTenantSettingsApiKeys';

   # Page
   const VIEW_TENANT_SETTINGS_PAGE = 'ViewTenantSettingsPage';
   const MANAGE_TENANT_SETTINGS_PAGE = 'ManageTenantSettingsPage';

   # Mobile Apps
   const VIEW_TENANT_SETTINGS_MOBILE_APPS = 'ViewTenantSettingsMobileApps';
   const MANAGE_TENANT_SETTINGS_MOBILE_APPS = 'ManageTenantSettingsMobileApps';

   # Domain
   const VIEW_TENANT_SETTINGS_DOMAIN = 'ViewTenantSettingsDomain';
   const MANAGE_TENANT_SETTINGS_DOMAIN = 'ManageTenantSettingsDomain';

   # Webhook
   const VIEW_TENANT_SETTINGS_WEBHOOK = 'ViewTenantSettingsWebhook';
   const MANAGE_TENANT_SETTINGS_WEBHOOK = 'ManageTenantSettingsWebhook';

   # Account
   const VIEW_TENANT_SETTINGS_ACCOUNT = 'ViewTenantSettingsAccount';
   const MANAGE_TENANT_SETTINGS_ACCOUNT = 'ManageTenantSettingsAccount';

   # Maintenance
   const VIEW_TENANT_SETTINGS_MAINTENANCE = 'ViewTenantSettingsMaintenance';
   const MANAGE_TENANT_SETTINGS_MAINTENANCE = 'ManageTenantSettingsMaintenance';

   # Delivery Integrations
   const VIEW_TENANT_SETTINGS_DELIVERY_INTEGRATIONS = 'ViewTenantSettingsDeliveryIntegrations';
   const MANAGE_TENANT_SETTINGS_DELIVERY_INTEGRATIONS = 'ManageTenantSettingsDeliveryIntegrations';

   # Thermal Printer
   const VIEW_TENANT_SETTINGS_THERMAL_PRINTER = 'ViewTenantSettingsThermalPrinter';
   const MANAGE_TENANT_SETTINGS_THERMAL_PRINTER = 'ManageTenantSettingsThermalPrinter';

   # Authentication
   const VIEW_TENANT_SETTINGS_AUTHENTICATION = 'ViewTenantSettingsAuthentication';
   const MANAGE_TENANT_SETTINGS_AUTHENTICATION = 'ManageTenantSettingsAuthentication';

   # Transactions
   const VIEW_TENANT_SETTINGS_TRANSACTIONS = 'ViewTenantSettingsTransactions';
   const MANAGE_TENANT_SETTINGS_TRANSACTIONS = 'ManageTenantSettingsTransactions';

   # Page Builder
   const VIEW_TENANT_SETTINGS_PAGE_BUILDER = 'ViewTenantSettingsPageBuilder';
   const MANAGE_TENANT_SETTINGS_PAGE_BUILDER = 'ManageTenantSettingsPageBuilder';

   # Google Channel
   const VIEW_TENANT_SETTINGS_GOOGLE_CHANNEL = 'ViewTenantSettingsGoogleChannel';
   const MANAGE_TENANT_SETTINGS_GOOGLE_CHANNEL = 'ManageTenantSettingsGoogleChannel';

   # Payouts
   const VIEW_TENANT_SETTINGS_PAYOUTS = 'ViewTenantSettingsPayouts';
   const MANAGE_TENANT_SETTINGS_PAYOUTS = 'ManageTenantSettingsPayouts';

   # Apps
   const VIEW_TENANT_APPS = 'ViewTenantApps';
   const MANAGE_TENANT_APPS = 'ManageTenantApps';

   # Plans
   const VIEW_TENANT_PLANS = 'ViewTenantPlans';
   const MANAGE_TENANT_PLANS = 'ManageTenantPlans';

   # Autozod
   const VIEW_TENANT_AUTOZOD = 'ViewTenantAutozod';
   const MANAGE_TENANT_AUTOZOD = 'ManageTenantAutozod';

   # Help
   const VIEW_TENANT_HELP = 'ViewTenantHelp';
   const MANAGE_TENANT_HELP = 'ManageTenantHelp';
}
