<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Auth\Permissions;

use Hyperzod\HyperzodServiceFunctions\Enums\BaseEnum;

class SystemPermissionEnum extends BaseEnum
{
   const GLOBAL_ACCESS = "GlobalAccess";
}
