<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums\Auth;

use Hyperzod\HyperzodServiceFunctions\Enums\BaseEnum;

class UserRoleEnum extends BaseEnum
{
   const SYSTEM_ADMIN = "S1000";
   const TENANT_ADMIN = "T1001";
   const TENANT_USER = "T1002";
   const MERCHANT_USER = "T1003";
   const USER = "T1004";
}
