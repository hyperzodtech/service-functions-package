<?php

namespace Hyperzod\HyperzodServiceFunctions\Enums;

use Hyperzod\HyperzodServiceFunctions\Traits\SettingsServiceTrait;

class OrderStatusEnum extends BaseEnum
{
    use SettingsServiceTrait;

    const CREATED = 0;
    const PENDING = 1;
    const ACCEPTED = 2;
    const READY = 3;
    const COLLECTED = 4;
    const COMPLETED = 5;
    const CANCELLED = 6;

    public $TENANT_ID;

    public function __construct(int $tenant_id)
    {
        $this->TENANT_ID = $tenant_id;
    }

    public function list($type = 'admin', int $status = null)
    {
        $order_statuses = $this->default($type);

        if (!is_null($status)) {
            $order_status = collect($order_statuses)->where('order_status', $status)->first();
            if (isset($order_status['order_status_label'])) {
                return $order_status['order_status_label'];
            }
            return null;
        }

        return $order_statuses;
    }

    public function default($type)
    {
        $default = null;
        $setting_key = ($type == 'client') ? 'client_order_status' : 'admin_order_status';
        $order_status = $this->settingsByKeys("tenant", [$setting_key], null, null, $this->TENANT_ID, null);
        if ($order_status && isset($order_status[$setting_key]) && !is_null($order_status[$setting_key]) && !empty($order_status[$setting_key])) {
            $default = $order_status[$setting_key];
        }
        if (is_null($default)) $default = $this->mappings();
        return $default;
    }

    public function mappings()
    {
        return [
            [
                'order_status' => self::PENDING,
                'order_status_label' => 'pending',
            ],
            [
                'order_status' => self::ACCEPTED,
                'order_status_label' => 'accepted',
            ],
            [
                'order_status' => self::READY,
                'order_status_label' => 'ready',
            ],
            [
                'order_status' => self::COLLECTED,
                'order_status_label' => 'collected',
            ],
            [
                'order_status' => self::COMPLETED,
                'order_status_label' => 'completed',
            ],
            [
                'order_status' => self::CANCELLED,
                'order_status_label' => 'cancelled',
            ],
        ];
    }

    public function getMerchantStatusOnAdminDelivery()
    {
        return [
            self::PENDING,
            self::ACCEPTED,
            self::READY,
            self::CANCELLED,
            // collected is added when order type is pickup 
        ];
    }

    public static function validate(int $status): bool
    {
        return in_array($status, self::getConstantValues());
    }
}
