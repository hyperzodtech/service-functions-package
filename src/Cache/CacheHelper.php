<?php

namespace Hyperzod\HyperzodServiceFunctions\Cache;

use Illuminate\Support\Facades\Cache;

class CacheHelper
{
	public static function getSetCache($key, \Closure $closure)
	{
		if (!Cache::has($key)) {
			$value = $closure();
			Cache::put($key, $value);
		}
		return Cache::get($key);
	}
}
