<?php

namespace Hyperzod\HyperzodServiceFunctions\Cache;

use Hyperzod\HyperzodServiceFunctions\Exceptions\Cache\TenantCacheException;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Illuminate\Support\Facades\Cache;

class TenantCache
{
    public const KEY_SEPARATOR = ".";

    public const TENANT_CACHE_KEY_PREFIX = "tenant";
    public const MERCHANT_CACHE_KEY_PREFIX = "merchant";

    /**
     * Store tenant data in cache using tenant id as Tag
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @param array $tags Custom Tags
     * @return void
     */
    public static function put(string $key, mixed $value = null, int $ttl = null, array $tags = [])
    {
        $key = self::processCacheKey($key);
        $tags = self::tags($tags);
        Cache::tags($tags)->put($key, $value, $ttl);
    }

    /**
     * Get tenant data from cache using tenant id as Tag
     * @param string $key
     * @param array $tags Custom Tags
     * @return mixed
     */
    public static function get(string $key, array $tags = [])
    {
        $key = self::processCacheKey($key);
        $tags = self::tags($tags);
        return Cache::tags($tags)->get($key);
    }

    /**
     * Check tenant data in cache using tenant id as Tag
     * @param string $key
     * @param array $tags Custom Tags
     * @return bool
     */
    public static function has(string $key, array $tags = [])
    {
        $key = self::processCacheKey($key);
        $tags = self::tags($tags);
        return Cache::tags($tags)->has($key);
    }

    /**
     * Get cache and set if not set already using closure value
     * @param string $key
     * @param \Closure $closure
     * @return mixed
     */
    public static function getSetCache($key, \Closure $closure, int $ttl_seconds = null)
    {
        if (!self::has($key)) {
            $value = $closure();
            self::put($key, $value, $ttl_seconds);
        }
        return self::get($key);
    }

    /**
     * Prepare tags for cache operations by merging tenant id with custom tags
     * @param array $custom_tags Custom Tags
     * @return array
     */
    private static function tags(array $custom_tags = []): array
    {
        $tags = [];
        $tags[] = self::getTenantCacheTag();

        if (self::getMerchantId()) {
            $tags[] = self::getMerchantCacheTag();
        }

        $tags = array_merge($tags, $custom_tags);

        return $tags;
    }

    /**
     * Prepend Tenant cache key prefix to cache key for unqiue cache key
     * @param string $key
     * @return string
     */
    private static function processCacheKey($key): string
    {
        $data[] = self::TENANT_CACHE_KEY_PREFIX;
        $data[] = self::getTenantId();
        $data[] = $key;

        return implode(self::KEY_SEPARATOR, $data);
    }

    /**
     * Flush cache for tenant
     * @return bool
     */
    public static function flush(): bool
    {
        $tags = self::tags();
        return Cache::tags($tags)->flush();
    }

    /**
     * Forget cache for tenant
     * @return bool
     */

    public static function forget(string $key): bool
    {
        $key = self::processCacheKey($key);
        $tags = self::tags();
        return Cache::tags($tags)->forget($key);
    }

    private static function getTenantId()
    {
        $tenant_id = HyperzodServiceFunctions::getTenantId();
        if (!$tenant_id) {
            throw new TenantCacheException("Require Tenant ID to use Tenant Cache");
        }
        return $tenant_id;
    }

    private static function getMerchantId()
    {
        return HyperzodServiceFunctions::getMerchantId();
    }

    public static function getTenantCacheTag()
    {
        return "tenant" . self::KEY_SEPARATOR . self::getTenantId();
    }

    public static function getMerchantCacheTag()
    {
        return "merchant" . self::KEY_SEPARATOR . self::getMerchantId();
    }
}
