<?php

namespace Hyperzod\HyperzodServiceFunctions\Scope;

use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class MerchantScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where(TerminologyEnum::MERCHANT_ID, HyperzodServiceFunctionsFacade::getMerchantId());
    }
}
