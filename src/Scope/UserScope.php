<?php

namespace Hyperzod\HyperzodServiceFunctions\Scope;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;

class UserScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $user = HyperzodServiceFunctionsFacade::getUser();
        $builder->where(TerminologyEnum::USER_ID, $user->id);
    }
}
