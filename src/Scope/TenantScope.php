<?php

namespace Hyperzod\HyperzodServiceFunctions\Scope;

use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;

class TenantScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where(TerminologyEnum::TENANT_ID, HyperzodServiceFunctionsFacade::getTenantId());
    }
}
