<?php

namespace Hyperzod\HyperzodServiceFunctions\Scope;

use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctionsFacade;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Http\Request;

class LocaleScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        if (HyperzodServiceFunctionsFacade::hasGlobal('locale')) {

            $builder->where(
                'locale',
                HyperzodServiceFunctionsFacade::getGlobal('locale')
            );
        }
    }
}