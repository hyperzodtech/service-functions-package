<?php

namespace Hyperzod\HyperzodServiceFunctions\Url\Interfaces;

use Hyperzod\HyperzodServiceFunctions\Dto\Url\SitemapDTOCollection;

interface SitemapInterface
{
   public function handle(): SitemapDTOCollection;
}
