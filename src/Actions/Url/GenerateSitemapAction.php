<?php

namespace Hyperzod\HyperzodServiceFunctions\Actions\Url;

use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Hyperzod\HyperzodServiceFunctions\Url\Interfaces\SitemapInterface;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
class GenerateSitemapAction
{
   use ApiResponseTrait;

   public function __invoke()
   {
      $validated = request()->validate([
         'sitemap_name' => 'required|string',
      ]);

      // list all the php files inside app folder (recursive)
      $files = collect(File::allFiles(app_path()))->filter(function ($file) {
         return $file->getExtension() === 'php';
      })->map(function ($file) {
         return $file->getRealPath();
      });

      // check if file implements the interface
      $files = $files->each(function ($file) {
         include_once($file);
      });

      // get all the classes that implement the interface
      $classes = collect(get_declared_classes())->filter(function ($class) {
         return in_array(SitemapInterface::class, class_implements($class));
      });

      $sitemap = $classes->first(function ($class) use ($validated) {
         return class_basename($class) === Str::studly($validated['sitemap_name'] . 'Sitemap');
      });

      if (!$sitemap) {
         return $this->errorResponse('Sitemap not found');
      }

      $results = (new $sitemap)->handle()->toArray()['collection'];
      $results = collect($results);
      
      // map last modified to carbon sitemap valid timestamp
      $results = $results->map(function ($result) {
         $result['last_modified'] = $result['last_modified']->toAtomString();
         return $result;
      });

      return $this->successResponse(null, $results->toArray());
   }
}
