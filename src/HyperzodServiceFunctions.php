<?php

namespace Hyperzod\HyperzodServiceFunctions;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Validator;
use Hyperzod\HyperzodServiceFunctions\Enums\EnvEnum;
use Hyperzod\HyperzodServiceFunctions\Dto\Auth\UserDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;
use Hyperzod\HyperzodServiceFunctions\Common\FlushModels;
use Hyperzod\HyperzodServiceFunctions\Dto\Tenant\TenantDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderValueEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\RequestApiGroupEnum;
use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;

class HyperzodServiceFunctions
{
    use ServiceConsumerTrait;

    public static function setTenant(TenantDTO $tenant)
    {
        Config::set('tenant', $tenant);
    }

    public static function getTenant(): TenantDTO|bool
    {
        return Config::get('tenant') ?? false;
    }

    public static function setUser(UserDTO $user)
    {
        Config::set('user', $user);
    }

    public static function getUser($exception = true): UserDTO|bool
    {
        if (Config::has('user')) {
            return Config::get('user');
        }

        if ($exception) {
            throw new \Exception("Call setUser before calling getUser");
        }

        return false;
    }

    public static function setTenantId($tenant_id)
    {
        Config::set(TerminologyEnum::TENANT_ID, intval($tenant_id));
    }

    public static function getTenantId(): int|bool
    {
        return Config::get(TerminologyEnum::TENANT_ID) ?? false;
    }

    public static function setMerchantId($id)
    {
        Config::set(TerminologyEnum::MERCHANT_ID, $id);
    }

    public static function getMerchantId(): string|bool
    {
        return Config::get(TerminologyEnum::MERCHANT_ID) ?? false;
    }

    public static function getMerchantIdV2(): string|null
    {
        return Config::get(TerminologyEnum::MERCHANT_ID) ?? null;
    }

    // Client Medium
    public static function setClientMedium(int $value)
    {
        Config::set('client_medium', $value);
    }

    public static function getClientMedium()
    {
        return Config::get('client_medium');
    }

    public static function validateClientMedium(int $client_medium): bool
    {
        $valid_client_mediums = [
            HttpHeaderValueEnum::CLIENT_MEDIUM_SYSTEM_ADMIN_PANEL,
            HttpHeaderValueEnum::CLIENT_MEDIUM_TENANT_ADMIN_PANEL,
            HttpHeaderValueEnum::CLIENT_MEDIUM_MERCHANT_ADMIN_PANEL,
        ];

        return in_array($client_medium, $valid_client_mediums);
    }

    public static function getApmTransactionId(): string
    {
        if (!Config::get('apm_transaction_id')) {
            Config::set('apm_transaction_id', (string) Str::uuid());
        }
        return Config::get('apm_transaction_id');
    }

    public static function getUserTypes($user_type = null, $return_user_type_id = false)
    {
        $data = [
            ["user_type_id" => 1,  "user_type" => "admin"],
            ["user_type_id" => 2,  "user_type" => "tenant"],
            ["user_type_id" => 3,  "user_type" => "merchant"],
            ["user_type_id" => 4,  "user_type" => "customer"],
            ["user_type_id" => 5,  "user_type" => "driver"],
        ];

        if (!is_null($user_type)) {
            $key = array_search($user_type, array_column($data, 'user_type'));
            if ($return_user_type_id) {
                return $data[$key]['user_type_id'];
            } else {
                return $data[$key];
            }
        }

        return $data;
    }

    public function validateTenantId($tenant_id)
    {
        $request = new \Illuminate\Http\Request();
        $request->query->add([
            TerminologyEnum::TENANT_ID => $tenant_id,
        ]);

        return $this->consumeService(ServiceEnum::TENANT, $request, 'GET', '/tenant/view');
    }

    public function getFormattedAmount($amount, $currency_setting = null, $unit = null)
    {
        $unit_suffix = $unit ? " / $unit" : '';

        $amount = number_format(floatval(bcdiv($amount, "1", 2)), 2, '.', ',');

        $amount = str_ends_with($amount, '.00') ? rtrim(rtrim($amount, '0'), '.') : $amount;

        if (is_null($currency_setting)) {
            $currency_setting = $this->getTenantCurrencySetting();
        }

        if (!$currency_setting || empty($currency_setting)) {
            return "$amount" . $unit_suffix;
        }
        $currency_symbol = "";
        $symbol_direction = "left";
        if (isset($currency_setting["symbol_type"]) && !empty($currency_setting["symbol_type"])) {
            if ($currency_setting["symbol_type"] == "symbol_native" && isset($currency_setting["symbol_native"]) && !empty($currency_setting["symbol_native"])) {
                $currency_symbol = $currency_setting["symbol_native"];
            }

            if ($currency_setting["symbol_type"] == "symbol" && isset($currency_setting["symbol"]) && !empty($currency_setting["symbol"])) {
                $currency_symbol = $currency_setting["symbol"];
            }
        }

        if (isset($currency_setting["symbol_direction"]) && !empty($currency_setting["symbol_direction"])) {
            $symbol_direction = $currency_setting["symbol_direction"];
        }

        if ($symbol_direction == "left") {
            return $currency_symbol . $amount . $unit_suffix;
        }

        return $amount . $currency_symbol . $unit_suffix;
    }

    public function getTenantHeader()
    {
        return json_decode(request()->header(HttpHeaderKeyEnum::TENANT), true);
    }

    public function checkIfSystemUser()
    {
        return boolval(request()->header(HttpHeaderKeyEnum::AUTH_IS_SYSTEM_USER));
    }

    public function checkIfAdminUser()
    {
        return boolval(request()->header(HttpHeaderKeyEnum::AUTH_IS_ADMIN_USER));
    }

    public function checkIfPageBuilderModeIsEnabled()
    {
        return boolval(request()->header(HttpHeaderKeyEnum::PAGE_BUILDER_MODE_ENABLED));
    }

    public function getClientDevice()
    {
        return request()->header(HttpHeaderKeyEnum::CLIENT_DEVICE) ?? "NA";
    }

    public function getClientIp()
    {
        return request()->header(HttpHeaderKeyEnum::CLIENT_IP) ?? null;
    }

    public static function getRequestApiGroup(): RequestApiGroupEnum|null
    {
        $request_api_group = request()->header(HttpHeaderKeyEnum::REQUEST_API_GROUP);
        if ($request_api_group) {
            $request_api_group = RequestApiGroupEnum::from($request_api_group);
        }
        return $request_api_group;
    }

    public static function setTenantCurrencySetting($tenant_currency_setting)
    {
        Config::set('tenant_currency_setting', $tenant_currency_setting);
    }

    public static function getTenantCurrencySetting()
    {
        return Config::get('tenant_currency_setting');
    }

    /**
     * Get API Host Domain
     */
    public static function getApiDomain(): string
    {
        return "api." . self::getPublicDomain();
    }

    /**
     * Get Redirect Domain
     */
    public static function getRedirectDomain(): string
    {
        return match (app()->environment()) {
            EnvEnum::PRODUCTION => "gohyperzod.com",
            default => "redirect.hyperzod.dev",
        };
    }

    /**
     * Get Upload Domain
     */
    public static function getUploadDomain(): string
    {
        return match (app()->environment()) {
            EnvEnum::PRODUCTION => "upload.hyperzod.app",
            EnvEnum::DEV => "upload.hyperzod.dev",
            EnvEnum::LOCAL => "upload.hyperzod.test",
        };
    }

    /**
     * Get Public Host Domain
     */
    public static function getPublicDomain(): string
    {
        return match (app()->environment()) {
            EnvEnum::PRODUCTION => "hyperzod.app",
            EnvEnum::DEV => "hyperzod.dev",
            EnvEnum::LOCAL => "hyperzod.test",
        };
    }

    /**
     * Get Micro-Service Host Domain
     */
    public static function getServiceDomain(): string
    {
        return match (app()->environment()) {
            EnvEnum::PRODUCTION => "hyperzod.live",
            EnvEnum::DEV => "hyperzod.dev",
            EnvEnum::LOCAL => "hyperzod.test",
        };
    }

    public static function hyperzodAdminAppNativeDomain(): string
    {
        return match (app()->environment()) {
            EnvEnum::PRODUCTION => "admin.hyperzod.app",
            EnvEnum::DEV => "admin.hyperzod.dev",
            EnvEnum::LOCAL => "admin.hyperzod.test",
        };
    }

    public static function hyperzodTenantAdminAppNativeDomain(string $tenant_slug): string
    {
        return $tenant_slug . "." . self::hyperzodTenantAdminAppNativeDomainTLD();
    }

    public static function hyperzodTenantAdminAppNativeDomainTLD(): string
    {
        return match (app()->environment()) {
            EnvEnum::PRODUCTION => "hyperzod.app",
            EnvEnum::DEV => "hyperzod.dev",
            EnvEnum::LOCAL => "hyperzod.test",
        };
    }

    public static function hyperzodOrderingAppNativeDomainTLD(): string
    {
        return match (app()->environment()) {
            EnvEnum::PRODUCTION => "hyperzod.me",
            EnvEnum::DEV => "hyperzodshop.dev",
            EnvEnum::LOCAL => "hyperzod.test",
        };
    }

    public static function getAdminDomain(): string
    {
        return  self::hyperzodAdminAppNativeDomain(); // domain
    }

    public static function getTenantAdminDomain(int $tenant_id): string
    {
        # Fetch slug and admin domain from tenant svc
        $request = new \Illuminate\Http\Request();
        $request->query->add([
            TerminologyEnum::TENANT_ID => $tenant_id,
        ]);
        try {
            $tenant_svc_response = (new self)->consumeService(ServiceEnum::TENANT, $request, 'GET', '/tenant/view');
            $tenant = new TenantDTO($tenant_svc_response['data']);
            if (!empty($tenant->admin_domain)) {
                return $tenant->admin_domain;
            }
            $domain = self::hyperzodTenantAdminAppNativeDomain($tenant->slug);
            return $domain;
        } catch (\Exception $e) {
            throw new Exception("Error fetching tenant details from tenant service");
        }
    }

    public function setGlobal(String $key, Mixed $value)
    {
        if (Config::has($key)) {
            throw new \Exception("$key is already set.");
        }

        Config::set($key, $value);
    }

    public function getGlobal(String $key)
    {
        if (Config::has($key)) {
            return Config::get($key);
        }
        throw new \Exception("Global value '$key' not defined.");
    }

    public function hasGlobal(String $key)
    {
        return Config::has($key);
    }

    public static function ifTenantQueryScopeIsPresentOnHeader(): bool
    {
        $scopes = request()->header(HttpHeaderKeyEnum::QUERY_SCOPE);
        if (!$scopes) {
            return false;
        }
        $scopes = explode(",", $scopes);
        return in_array(HttpHeaderValueEnum::QUERY_SCOPE_TENANT, $scopes);
    }

    public static function ifUserQueryScopeIsPresentOnHeader(): bool
    {
        $scopes = request()->header(HttpHeaderKeyEnum::QUERY_SCOPE);
        if (!$scopes) {
            return false;
        }
        $scopes = explode(",", $scopes);
        return in_array(HttpHeaderValueEnum::QUERY_SCOPE_USER, $scopes);
    }

    public static function setTenantTimezone($tenant_timezone)
    {
        Config::set('tenant_timezone', $tenant_timezone);
    }

    public static function getTenantTimezone()
    {
        return Config::get('tenant_timezone', 'UTC');
    }

    public function uploadFileViaUrl(string $url)
    {
        $request = new \Illuminate\Http\Request();
        $request->setMethod("POST");
        $payload = [
            "file" =>  $url
        ];
        $request->query->add($payload);

        return $this->consumeService(ServiceEnum::UPLOAD, $request, 'POST', '/upload', true, !(app()->environment() == EnvEnum::LOCAL));
    }

    public function uploadFilesViaMultipleUrl(array $urls)
    {
        $request = new \Illuminate\Http\Request();
        $request->setMethod("POST");
        $payload = [
            "tenant_id" => $this->getTenantId(),
            "links" =>  $urls
        ];
        $request->query->add($payload);

        return $this->consumeService(ServiceEnum::UPLOAD, $request, 'POST', '/upload/links', false, !(app()->environment() == EnvEnum::LOCAL), [
            'skip_referrer' => false,
            'request_timeout' => 30
        ]);
    }

    public function deleteAllMerchantData($params)
    {
        $validated = Validator::make($params, [
            'tenant_id' => 'required|integer',
            'merchant_id' => 'required|string',
        ])->validate();

        $this->setTenantId($validated['tenant_id']);

        $request = new \Illuminate\Http\Request();
        $request->query->add($validated);
        (new FlushModels())->flushMerchant($request);
    }

    public static function ifTenantPanel(): bool
    {
        return request()->header(HttpHeaderKeyEnum::CLIENT_MEDIUM) == HttpHeaderValueEnum::CLIENT_MEDIUM_TENANT_ADMIN_PANEL;
    }

    public static function ifMerchantPanel(): bool
    {
        return request()->header(HttpHeaderKeyEnum::CLIENT_MEDIUM) == HttpHeaderValueEnum::CLIENT_MEDIUM_MERCHANT_ADMIN_PANEL;
    }

    public static function setAuthBearerToken($token)
    {
        Config::set('auth_bearer_token', $token);
    }

    public static function getAuthBearerToken()
    {
        return Config::get('auth_bearer_token');
    }

    public static function setGoogleDistanceMeter(float $distance)
    {
        Config::set('google_distance_meter', $distance);
    }

    public static function getGoogleDistanceMeter()
    {
        return Config::get('google_distance_meter');
    }
}
