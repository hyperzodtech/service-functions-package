<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

class UrlHelper
{
    public static function getMerchantUrl(string $slug, string $merchant_id): string
    {
        $uri = '/m/{slug}/{merchant_id}';

        return str_replace(['{slug}', '{merchant_id}'], [$slug, $merchant_id], $uri);
    }

    public static function getMerchantProductUrl(string $merchant_slug, string $merchant_id, string $product_id): string
    {
        $uri = '/m/{merchant_slug}/{merchant_id}/product/{product_id}';

        return str_replace(
            ['{merchant_slug}', '{merchant_id}', '{product_id}'],
            [$merchant_slug, $merchant_id, $product_id],
            $uri
        );
    }

    public static function getMerchantProductIndexSitemapUrl(string $merchant_id): string
    {
        $uri = '/m/{merchant_id}/products_sitemap.xml';

        return str_replace(['{merchant_id}'], [$merchant_id], $uri);
    }
}
