<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\HSF;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;

class PageBuilderHelper
{
    public static function flushTenantCache(int $tenant_id)
    {
        // Clear Cache
        $url = "https://" . HSF::getApiDomain() . "/public/v1/page-builder/builder/flush/cache/{$tenant_id}";

        @shell_exec("curl --max-time 10 -X POST -H 'Content-Type: application/json' -H '" . HttpHeaderKeyEnum::TENANT . ":{$tenant_id}' '{$url}' > /dev/null 2>&1 &");
    }
}
