<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

use Hyperzod\HyperzodServiceFunctions\Enums\SaasModuleEnum;

class SaasModuleHelper
{
	public static function list(): array
	{
		$modules[] = ['id' => SaasModuleEnum::ADMIN, 'name' => 'Admin'];
		$modules[] = ['id' => SaasModuleEnum::WEB_ORDERING, 'name' => 'Ordering Website'];
		$modules[] = ['id' => SaasModuleEnum::APP_ORDERING, 'name' => 'Ordering App'];
		$modules[] = ['id' => SaasModuleEnum::APP_MERCHANT, 'name' => 'Merchant App'];
		$modules[] = ['id' => SaasModuleEnum::APP_DRIVER, 'name' => 'Driver App'];

		return $modules;
	}

	public static function getName(string $saas_module_id): string
	{
		$nameMap = array_column(self::list(), 'name', 'id');
		return $nameMap[$saas_module_id];
	}
}
