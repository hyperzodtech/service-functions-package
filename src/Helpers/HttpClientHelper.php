<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

class HttpClientHelper
{
    private static $client = null;

    public static function getClient()
    {
        if (self::$client == null) {
            self::$client =(new \Illuminate\Http\Client\PendingRequest())->buildClient();
        }
        return self::$client;
    }
}