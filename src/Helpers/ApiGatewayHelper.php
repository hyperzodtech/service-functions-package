<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\HSF;

class ApiGatewayHelper
{
    public static function flushTenantCache(int $tenant_id)
    {
        $url = "https://" . HSF::getApiDomain() . "/public/v1/flush/cache/tenant/{$tenant_id}";

        @shell_exec("curl --max-time 10 -X POST -H 'Content-Type: application/json' -H '" . HttpHeaderKeyEnum::TENANT . ":{$tenant_id}' '{$url}' > /dev/null 2>&1 &");
    }

    public static function flushMerchantCache(int $tenant_id, string $merchant_id)
    {
        $url = "https://" . HSF::getApiDomain() . "/store/v1/merchant/flush/cache/$merchant_id";

        @shell_exec("curl --max-time 10 -X POST -H 'Content-Type: application/json' -H '" . HttpHeaderKeyEnum::TENANT . ":{$tenant_id}' '{$url}' > /dev/null 2>&1 &");
    }

    public static function flushXTenantValidateCache(string $x_tenant)
    {
        $url = "https://" . HSF::getApiDomain() . "/public/v1/tenant/validate/flush?x-tenant={$x_tenant}";

        @shell_exec("curl --max-time 10 -X GET -H 'Content-Type: application/json' '{$url}' > /dev/null 2>&1 &");
    }
}
