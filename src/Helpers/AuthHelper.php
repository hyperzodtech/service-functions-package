<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

use Illuminate\Http\Request;
use Hyperzod\HyperzodServiceFunctions\Dto\Auth\UserDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\Auth\AuthEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\HttpHeaderKeyEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\TerminologyEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\Auth\ForbiddenResourceException;
use Hyperzod\HyperzodServiceFunctions\Exceptions\Auth\UnauthorizedTokenException;
use Hyperzod\HyperzodServiceFunctions\Exceptions\InvalidServiceResponseException;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceClientException;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Validation\ValidationException;

class AuthHelper
{
    use ServiceConsumerTrait;

    public static function validateBearerToken(string|bool $bearer_token, string|bool $api_key = false): bool
    {
        if ($bearer_token == false && $api_key == false) {
            throw ValidationException::withMessages([
                'message' => 'Auth Bearer token or API key is required to access this api.'
            ]);
        }

        $request = new Request();
        # Set API Key
        if ($api_key) {
            $request->query->add([AuthEnum::APIKEY => $api_key]);
        }
        # Set Bearer Token
        if ($bearer_token) {
            $request->headers->set('Authorization', 'Bearer ' . $bearer_token);
        }
        # Set Client Medium
        if (HyperzodServiceFunctions::getClientMedium()) {
            $request->headers->set(HttpHeaderKeyEnum::CLIENT_MEDIUM, HyperzodServiceFunctions::getClientMedium());
        }
        # Set Tenant ID
        $tenant_id = HyperzodServiceFunctions::getTenantId();
        if ($tenant_id) {
            $request->query->add([TerminologyEnum::TENANT_ID => $tenant_id]);
        }
        # Set Merchant ID
        $merchant_id = HyperzodServiceFunctions::getMerchantId();
        if ($merchant_id !== false && !empty($merchant_id)) {
            $request->query->add([TerminologyEnum::MERCHANT_ID => $merchant_id]);
        }

        $resp = (new self)->consumeService(ServiceEnum::AUTH, $request, 'GET', '/me');

        if (!isset($resp['success'])) {
            throw new InvalidServiceResponseException("Success node not found on validateBearerToken");
        }

        if (!$resp['success']) {
            if ($resp['status_code'] == 403) {
                throw new ForbiddenResourceException($resp['message']);
            }
            throw new UnauthorizedTokenException($resp['message']);
        }

        HyperzodServiceFunctions::setUser(new UserDTO($resp['data']));

        return true;
    }

    public static function ifUserBelongsToTenant(int $user_id, int $tenant_id, bool $throw_exception = false): bool
    {
        $actual_tenant_id = Cache::rememberForever("user_tenant:{$user_id}", function () use ($user_id): int {
            $resp = (new self)->consumeService(
                ServiceEnum::AUTH,
                new Request(),
                'GET',
                "/getUserTenantId/{$user_id}"
            );

            if (isset($resp['success']) && !$resp['success']) {
                throw new ServiceClientException("User {$user_id} does not exists");
            }

            return $resp['data']['tenant_id'];
        });

        $result = ($tenant_id == $actual_tenant_id);

        if ($throw_exception && !$result) {
            throw new ServiceClientException("User {$user_id} does not belong to tenant {$tenant_id}");
        }

        return $result;
    }
}
