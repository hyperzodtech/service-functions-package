<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

use Closure;

class GeneralHelpers
{
    public static function tryOrFalse(Closure $closure)
    {
        try {
            return $closure();
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function removeEmoji($text)
    {
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u'; // Match Emoticons
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u'; // Match Miscellaneous Symbols and Pictographs
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u'; // Match Transport And Map Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u'; // Match Miscellaneous Symbols
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u'; // Match Dingbats

        $clean_text = $text;
        $clean_text = preg_replace($regexEmoticons, '', $clean_text);
        $clean_text = preg_replace($regexSymbols, '', $clean_text);
        $clean_text = preg_replace($regexTransport, '', $clean_text);
        $clean_text = preg_replace($regexMisc, '', $clean_text);
        $clean_text = preg_replace($regexDingbats, '', $clean_text);
        $clean_text = preg_replace('/\s+/', ' ', $clean_text);

        return $clean_text;
    }
}
