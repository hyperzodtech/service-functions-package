<?php

namespace Hyperzod\HyperzodServiceFunctions\Helpers;

use Hyperzod\HyperzodServiceFunctions\Dto\Auth\UserDTO;
use Hyperzod\HyperzodServiceFunctions\Enums\Auth\Permissions\SystemPermissionEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\Auth\Permissions\MerchantPermissionEnum;
use Hyperzod\HyperzodServiceFunctions\Enums\Auth\Permissions\TenantPermissionEnum;

class AuthPermissionHelper
{
	const PERMISSION_DELIMETER = "|";

	public static function ifUserHasGlobalAccessPermission(UserDTO $user): bool
	{
		return in_array(SystemPermissionEnum::GLOBAL_ACCESS, $user->permissions);
	}

	public static function ifUserHasTenantGlobalAccessPermission(UserDTO $user): bool
	{
		return in_array(TenantPermissionEnum::GLOBAL_ACCESS_TENANT, $user->permissions);
	}

	public static function ifUserHasTenantMerchantAccessPermission(UserDTO $user): bool
	{
		return in_array(TenantPermissionEnum::ACCESS_TENANT_MERCHANTS, $user->permissions);
	}

	// check for user permission in array
	public static function ifUserHasPermission(UserDTO $user, array $permissions): bool
	{
		return count(array_intersect($permissions, $user->permissions)) > 0 ? true : false;
	}

	// If user has access to merchant id
	public static function ifUserBelongsToMerchantId(UserDTO $user, string $merchantId): bool
	{
		return in_array($merchantId, $user->merchant_users);
	}
}
