<?php

namespace Hyperzod\HyperzodServiceFunctions\Exceptions;

use Throwable;
use ArgumentCountError;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Hyperzod\HyperzodServiceFunctions\Exceptions\Auth\ForbiddenResourceException;
use Hyperzod\HyperzodServiceFunctions\Exceptions\Auth\UnauthorizedTokenException;

class Handler extends ExceptionHandler
{
    use ApiResponseTrait;

    public function report(Throwable $exception)
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof ThrottleRequestsException) {
            return $this->errorResponse($exception->getMessage(), null, $exception->getStatusCode(), true);
        }

        if ($exception instanceof ModelNotFoundException) {
            $model = class_basename($exception->getModel());
            $data = ['model' => $model];

            if ($request->route()->parameters()) $data['route_params'] = $request->route()->parameters();

            return $this->errorResponse("Record not found", $data, 404);
        }

        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse("HTTP Request not found", [
                'messsage' => $exception->getMessage(),
                'url' => $request->fullUrl(),
            ], $exception->getStatusCode(), true);
        }

        if ($exception instanceof ValidationException) {
            $data = $exception->errors();
            return $this->errorResponse("Validation Failed", $data, 422, true);
        }

        if ($exception instanceof ServiceClientException) {
            return $this->errorResponse($exception->getMessage(), $exception->getContext(), 400, true);
        }

        if ($exception instanceof ForbiddenResourceException) {
            return $this->errorResponse($exception->getMessage(), null, 403, true);
        }

        if ($exception instanceof UnauthorizedTokenException) {
            return $this->errorResponse((!empty($exception->getMessage()) ? $exception->getMessage() : "Unauthroized bearer token."), null, 401, true);
        }

        if ($exception instanceof ArgumentCountError) {
            return $this->errorResponse($exception->getMessage(), null, 500);
        }

        return parent::render($request, $exception);
    }
}
