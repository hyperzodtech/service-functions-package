<?php

namespace Hyperzod\HyperzodServiceFunctions\Exceptions;

use Exception;

class CustomQueryFailedException extends Exception
{
   private array|null $context;

   public function __construct(
      string $message,
      array $context = null,
      int $code = 0,
      Exception $previous = null
   ) {
      parent::__construct($message, $code, $previous);
      $this->context = $context;
   }

   public function getContext(): array|null
   {
      return $this->context;
   }
}
