<?php

namespace Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceDiscovery;

use Exception;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;

class ServiceDiscoveryException extends Exception
{
   use ApiResponseTrait;

   public function render($request)
   {
      return $this->errorResponse($this->getMessage(), $request->all(), 500, true);
   }
}
