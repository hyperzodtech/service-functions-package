<?php

namespace Hyperzod\HyperzodServiceFunctions\Exceptions;

use Exception;
use Hyperzod\HyperzodServiceFunctions\Traits\ApiResponseTrait;

class InvalidServiceResponseException extends Exception
{
   use ApiResponseTrait;

   public function render($request)
   {
      return $this->errorResponse($this->getMessage(), null, 502);
   }
}
