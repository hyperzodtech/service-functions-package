<?php

namespace Hyperzod\HyperzodServiceFunctions\Exceptions\Auth;

use Exception;

class UnauthorizedTokenException extends Exception
{
    //
}
