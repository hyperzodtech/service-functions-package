<?php

namespace Hyperzod\HyperzodServiceFunctions\Exceptions\Auth;

use Exception;

class ForbiddenResourceException extends Exception
{
    //
}
