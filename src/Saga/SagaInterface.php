<?php

namespace Hyperzod\HyperzodServiceFunctions\Saga;

use Alignwebs\SagaClient\SagaBuilder;
use Alignwebs\SagaClient\SagaClient;
use Alignwebs\SagaClient\SagaObject;

interface SagaInterface
{
    public function createSagaClient() : SagaClient;

    public function createSagaBuilder($sagaName, $sagaUid) : SagaBuilder;

    public function createSagaObject($service) : SagaObject;
}
