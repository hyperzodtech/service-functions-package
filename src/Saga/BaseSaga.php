<?php

namespace Hyperzod\HyperzodServiceFunctions\Saga;

use Alignwebs\SagaClient\SagaClient;
use Alignwebs\SagaClient\SagaObject;
use Alignwebs\SagaClient\SagaBuilder;
use Hyperzod\HyperzodServiceFunctions\Enums\ServiceEnum;
use Hyperzod\HyperzodServiceFunctions\Traits\ServiceConsumerTrait;

class BaseSaga implements SagaInterface
{
    use ServiceConsumerTrait;

    public function createSagaClient(): SagaClient
    {
        $host = $this->getServiceUrl(ServiceEnum::SAGA);
        return new SagaClient($host);
    }

    public function createSagaBuilder($sagaName, $sagaUid): SagaBuilder
    {
        return new SagaBuilder($sagaName, $sagaUid);
    }

    public function createSagaObject($service): SagaObject
    {
        return new SagaObject($service);
    }
}
