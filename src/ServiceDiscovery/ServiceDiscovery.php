<?php

namespace Hyperzod\HyperzodServiceFunctions\ServiceDiscovery;

use Hyperzod\HyperzodServiceFunctions\Enums\EnvEnum;
use Hyperzod\HyperzodServiceFunctions\Exceptions\ServiceDiscovery\ServiceDiscoveryException;
use Hyperzod\HyperzodServiceFunctions\Helpers\HttpClientHelper;
use Hyperzod\HyperzodServiceFunctions\HyperzodServiceFunctions;
use Illuminate\Support\Facades\Http;

class ServiceDiscovery
{
    const HTTP_TIMEOUT_SECONDS = 3;
    const HTTP_RETRIES = 3;
    const HTTP_RETRY_SECONDS = 500;

    public static function discoverService($service_tag, $https_enabled = false)
    {
        $sd_host = self::getSDHost();

        $url = $sd_host . "/get";

        $host = cache()->remember("sd:$service_tag", 60 * 60 * 24, function () use ($url, $service_tag): string {
            try {
                $response = Http::setClient(HttpClientHelper::getClient())
                ->retry(self::HTTP_RETRIES, self::HTTP_RETRY_SECONDS)
                ->timeout(self::HTTP_TIMEOUT_SECONDS)
                ->get($url, [
                    'service_name' => $service_tag
                ]);

                if (!$response->ok()) {
                    throw new ServiceDiscoveryException("Service Discovery failed for $service_tag. Response is not ok. Status code: " . $response->status());
                }

                $service = $response->json();

                if (!isset($service['host'])) {
                    throw new ServiceDiscoveryException("Host not set for $service_tag in the service discovery response.");
                }

                return $service['host'];
            } catch (\Exception $e) {
                throw new ServiceDiscoveryException("Service Discovery failed for $service_tag. Message: " . $e->getMessage());
            }
        });

        $protocol = $https_enabled ? "https://" : "http://";

        return $protocol . $host;
    }

    public static function getSDHost()
    {
        $domain = HyperzodServiceFunctions::getServiceDomain();
        // Array of hosts
        $hosts =  [
            EnvEnum::PRODUCTION => "http://sd." . $domain,
            EnvEnum::DEV => "http://sys-sd." . $domain,
            EnvEnum::LOCAL => "http://sys-sd." . $domain,
        ];

        // Find the current dev environment
        $env = app()->environment();

        if (isset($hosts[$env])) {
            return $hosts[$env];
        }

        throw new ServiceDiscoveryException("No SD host found for: $env");
    }
}
